/* eslint-disable no-console */
import { Application } from "express";
import http, { Server } from "http";
import App from "./server";

declare let module: any; // eslint-disable-line @typescript-eslint/no-explicit-any

let app: Application = App;
const server: Server = http.createServer(app);
let currentApp: Application = app;

server.listen(process.env.PORT || 3000, () => {
    console.log("🚀 started");
});

if (module.hot) {
    console.log("✅  Server-side HMR Enabled!");

    module.hot.accept("./server", () => {
        console.log("🔁  HMR Reloading `./server`...");

        try {
            app = App;
            server.removeListener("request", currentApp);
            server.on("request", app);
            currentApp = app;
        } catch (error) {
            console.error(error);
        }
    });
}
