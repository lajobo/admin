import React from "react";
import { useSelector } from "react-redux";
import { StoreNotification } from "../../types/notification";
import { selectNotifications } from "../../store/selectors/notification";
import { Notification } from "./notification";
import { Flex } from "../ui/structure/flex";

export const NotificationManager: React.FC = () => {
    const notifications: StoreNotification[] = useSelector(selectNotifications);

    return notifications.length === 0 ? null : (
        <Flex
            position={"absolute"}
            top={0}
            right={0}
            pt={4}
            pr={4}
            zIndex={100}
            flexDirection={"column"}
            gap={"10px"}
            width={"350px"}
            maxWidth={"70%"}
            pointerEvents={"none"}
        >
            {notifications.map((notification: StoreNotification) => (
                <Notification key={notification.id} {...notification} />
            ))}
        </Flex>
    );
};
