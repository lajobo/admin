import React, { useContext } from "react";
import { useDispatch } from "react-redux";
import { Keyframes, keyframes, ThemeContext } from "styled-components";
import { StoreNotification } from "../../types/notification";
import { Message } from "../ui/message/message";
import { Heading } from "../ui/typography/heading";
import { Dispatch } from "../../types/store";
import { deleteNotification } from "../../store/slices/notification";
import { Text } from "../ui/typography/text";
import { Theme } from "../../types/ui";
import { Flex } from "../ui/structure/flex";
import { useTimer } from "../../hooks/timer";

interface NotificationProps extends StoreNotification { }

export const Notification: React.FC<NotificationProps> = (props: NotificationProps) => {
    const theme: Theme = useContext(ThemeContext);
    const colors: { light, medium, dark } = theme.colors.state[props.type];
    const dispatch: Dispatch = useDispatch();
    const { time, toggle, reset, isActive } = useTimer(
        props.duration,
        0.1,
        () => { dispatch(deleteNotification(props.id)); },
        true
    );

    const animation: Keyframes = keyframes`
        0% { background-position-x: 100% }
        100% { background-position-x: 0% }
    `;

    return (
        <Message
            pointerEvents={"auto"}
            type={props.type}
            size={"xs"}
            onDismiss={() => { dispatch(deleteNotification(props.id)); }}
            overflow={"hidden"}
            background={`linear-gradient(90deg, ${colors.medium}, ${colors.medium} 50%, ${colors.light} 0)`}
            backgroundSize={"200% 100%"}
            backgroundPosition={!isActive && time > 0 ? "100%" : "0%"}
            animationName={isActive ? animation : undefined}
            animationDuration={`${props.duration}s`}
            animationTimingFunction={"linear"}
            onMouseEnter={() => {
                toggle(false);
            }}
            onMouseLeave={() => {
                reset();
                toggle(true);
            }}
        >
            <Flex flexDirection={"column"}>
                <Heading size={"xs"}>
                    {props.heading}
                </Heading>
                {props.content && (
                    <Text color={`state.${props.type}.dark`} size={"xs"}>
                        {props.content}
                    </Text>
                )}
            </Flex>
        </Message>
    );
};
