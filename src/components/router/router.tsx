import React from "react";
import { Route, Routes } from "react-router-dom";
import { AuthUserListPage } from "../../pages/auth/user/list";
import { AuthUserFormPage } from "../../pages/auth/user/form";
import { AuthClientListPage } from "../../pages/auth/client/list";
import { AuthClientFormPage } from "../../pages/auth/client/form";
import { DashboardPage } from "../../pages/dashboard/dashboard";
import { RoutePaths } from "../../types/router";

export const Router: React.FC = () => (
    <Routes>
        <Route path={RoutePaths.Dashboard} element={<DashboardPage />} />

        <Route path={RoutePaths.AuthUserList} element={<AuthUserListPage />} />
        <Route path={RoutePaths.AuthUserForm} element={<AuthUserFormPage />} />
        <Route path={RoutePaths.AuthClientList} element={<AuthClientListPage />} />
        <Route path={RoutePaths.AuthClientForm} element={<AuthClientFormPage />} />
    </Routes>
);
