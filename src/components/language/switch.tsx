import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Box } from "../ui/structure/box";
import { ComponentMenuNavigationLanguageSwitchI18n, I18nLanguages } from "../../types/i18n";
import { selectI18n, selectLanguage } from "../../store/selectors/i18n";
import { GeneralIconProps } from "../../types/component/icon";
import { SvgFlagDe } from "../ui/icon/lib/flag/de";
import { SvgFlagGb } from "../ui/icon/lib/flag/gb";
import { Text } from "../ui/typography/text";
import { Flex } from "../ui/structure/flex";
import { Dispatch } from "../../types/store";
import { setLanguage } from "../../store/slices/i18n";

interface Language {
    language: I18nLanguages;
    label: string;
    icon: React.FC<GeneralIconProps>;
}

interface LanguageSwitchProps {
    darkMode?: boolean;
}

export const LanguageSwitch: React.FC<LanguageSwitchProps> = ({ darkMode }: LanguageSwitchProps) => {
    const dispatch: Dispatch = useDispatch();
    const i18n: ComponentMenuNavigationLanguageSwitchI18n = useSelector(selectI18n).components.menu.navigation.languageSwitch;
    const currentLanguage: I18nLanguages = useSelector(selectLanguage);
    const languages: Language[] = currentLanguage === I18nLanguages.EN
        ? [{ language: I18nLanguages.DE, label: i18n.de, icon: SvgFlagDe }]
        : [{ language: I18nLanguages.EN, label: i18n.en, icon: SvgFlagGb }];

    return (
        <Flex
            as={"ul"}
            flexDirection={"column"}
            alignItems={"center"}
            pb={3}
            opacity={".5"}
        >
            {languages.map((language: Language) => {
                const Icon: React.FC<GeneralIconProps> = language.icon;
                return (
                    <Box
                        key={language.language}
                        as={"li"}
                    >
                        <Box
                            as={"button"}
                            onClick={() => {
                                dispatch(setLanguage(language.language));
                            }}
                            background={"none"}
                            border={"none"}
                            cursor={"pointer"}
                            py={2}
                            px={3}
                        >
                            <Icon width={"15px"} />
                            <Text content={language.label} color={darkMode ? "text" : "white"} size={"s"} ml={2} />
                        </Box>
                    </Box>
                );
            })}
        </Flex>
    );
};

LanguageSwitch.defaultProps = { darkMode: false };
