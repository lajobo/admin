import React, { ReactElement } from "react";
import { AnyObject } from "../../types/generic";
import { cartesianProduct, enrichCartesianProductWithNames } from "../../utils/array";

interface CartesianProductProps {
    props: AnyObject;
    render: (cartesianProps: AnyObject) => ReactElement;
}

export const CartesianProduct: React.FC<CartesianProductProps> = ({
    render,
    ...props
}: CartesianProductProps) => {
    const propCombinations: AnyObject = enrichCartesianProductWithNames(
        cartesianProduct(...Object.values(props.props)),
        Object.keys(props.props)
    ).map((renderProps: AnyObject) => ({
        key: JSON.stringify(renderProps),
        ...renderProps
    }));

    return (
        <>
            {propCombinations.map(render)}
        </>
    );
};
