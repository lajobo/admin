import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { Location } from "history";
import { useSelector } from "react-redux";
import { NavigationMenu } from "./navigation";
import { TopMenu } from "./top";
import { DeviceSize } from "../../types/device";
import { selectDeviceSize } from "../../store/selectors/device";

export const MainMenu: React.FC = () => {
    const location: Location = useLocation();
    const deviceSize: DeviceSize = useSelector(selectDeviceSize);
    const [showNavigation, setShowNavigation] = useState<boolean>(deviceSize === DeviceSize.L);

    useEffect(() => {
        if (deviceSize !== DeviceSize.L) {
            // collapse navigation on route change = on link click
            setShowNavigation(false);
        }
    }, [location?.pathname]);

    useEffect(() => {
        setShowNavigation(deviceSize === DeviceSize.L);
    }, [deviceSize]);

    return (
        <>
            <TopMenu onMenuButtonClick={() => { setShowNavigation(!showNavigation); }} />
            <NavigationMenu visible={showNavigation} />
        </>
    );
};
