import React from "react";
import { useDispatch } from "react-redux";
import { Box } from "../../ui/structure/box";
import { Text } from "../../ui/typography/text";
import { BaseButton } from "../../ui/button/base";
import { Dispatch } from "../../../types/store";
import { logout } from "../../../store/api/auth/logout";

// TODO add data gathering
export const NavigationUserDetails: React.FC = () => {
    const dispatch: Dispatch = useDispatch();

    return (
        <Box
            backgroundColor={"primary"}
            mt={"-5px"}
            px={4}
            py={4}
        >
            <Text
                size={"l"}
                fontWeight={"bold"}
                content={"John Doe"}
                display={"block"}
                color={"white"}
                mb={2}
            />
            <Text
                size={"m"}
                content={"Admin"}
                display={"block"}
                color={"white"}
            />
            <BaseButton
                mt={4}
                cursor={"pointer"}
                onClick={() => {
                    dispatch(logout());
                }}
            >
                <Text
                    size={"m"}
                    content={"Logout"}
                    display={"block"}
                    color={"white"}
                    textDecoration={"underline"}
                />
            </BaseButton>
        </Box>
    );
};
