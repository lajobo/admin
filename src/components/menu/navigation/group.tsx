import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { Location } from "history";
import { Link } from "../../ui/link/link";
import { Box } from "../../ui/structure/box";
import { Text } from "../../ui/typography/text";

interface NavigationItem {
    to: string;
    label: string;
}

const isSameUrl: (
    urlA: string,
    urlB: string
) => boolean = (
    urlA: string,
    urlB: string
) => urlA && urlB && urlA.replace(/^\/|\/$/g, "") === urlB.replace(/^\/|\/$/g, "");

const isActiveItem: (
    pathname: string,
    to: string
) => boolean = (
    pathname: string,
    to: string
) => isSameUrl(pathname, to) || pathname.startsWith(`/${to}`);

const isActive: (
    to: string,
    items: NavigationItem[],
    location: Location
) => boolean = (
    to: string,
    items: NavigationItem[],
    location: Location
) => {
    const toActive: boolean = isSameUrl(location.pathname, to);
    const itemActive: boolean = items && items.reduce<boolean>(
        (
            result: boolean,
            item: NavigationItem
        ) => result || isActiveItem(location.pathname, item.to),
        false
    );

    return toActive || itemActive;
};

interface NavigationGroupProps {
    title: string;
    items?: NavigationItem[];
    to?: string;
}

export const NavigationGroup: React.FC<NavigationGroupProps> = ({
    title,
    items,
    to
}: NavigationGroupProps) => {
    const location: Location = useLocation();
    const [active, setActive] = useState<boolean>(isActive(to, items, location));
    const [open, setOpen] = useState<boolean>(isActive(to, items, location));

    useEffect(() => {
        setActive(isActive(to, items, location));
    }, [location]);

    return (
        <>
            <Box
                as={"li"}
                backgroundColor={"grey.dark"}
                borderBottom={"1px solid"}
                borderBottomColor={"grey.light"}
                cursor={"pointer"}
            >
                <Link
                    to={to || "#"}
                    display={"block"}
                    p={4}
                    onClick={() => {
                        if (!to && items) {
                            setOpen(!open);
                        }
                    }}
                >
                    <Text
                        size={"l"}
                        color={active ? "secondary" : "white"}
                        content={title}
                        textDecoration={"none"}
                    />
                </Link>
            </Box>
            {(open) && items && (
                <>
                    {items.map((item: NavigationItem) => (
                        <Box
                            as={"li"}
                            key={JSON.stringify(item)}
                            backgroundColor={"grey.darker"}
                            borderBottom={"1px solid"}
                            borderBottomColor={"grey.light"}
                        >
                            <Link
                                to={item.to}
                                display={"block"}
                                p={4}
                            >
                                <Text
                                    size={"m"}
                                    color={isActiveItem(location.pathname, item.to) ? "secondary" : "white"}
                                    content={item.label}
                                    textDecoration={"none"}
                                    pl={4}
                                />
                            </Link>
                        </Box>
                    ))}
                </>
            )}
        </>
    );
};
