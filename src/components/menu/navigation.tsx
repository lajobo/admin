import React from "react";
import { useSelector } from "react-redux";
import { Sidebar } from "../ui/sidebar/sidebar";
import { DeviceSize } from "../../types/device";
import { NavigationUserDetails } from "./navigation/userDetails";
import { NavigationGroup } from "./navigation/group";
import { Box } from "../ui/structure/box";
import { selectDeviceSize } from "../../store/selectors/device";
import { ComponentMenuNavigationI18n } from "../../types/i18n";
import { selectI18n } from "../../store/selectors/i18n";
import { LanguageSwitch } from "../language/switch";

interface NavigationMenuProps {
    visible: boolean;
}

export const NavigationMenu: React.FC<NavigationMenuProps> = (props: NavigationMenuProps) => {
    const deviceSize: DeviceSize = useSelector(selectDeviceSize);
    const i18n: ComponentMenuNavigationI18n = useSelector(selectI18n).components.menu.navigation;

    return (
        <Sidebar
            direction={"left"}
            visible={props.visible}
            addDimmer={deviceSize === DeviceSize.S}
            pt={["65px", "95px"]}
            height={["calc(100% - 65px)", "calc(100% - 95px)"]}
            backgroundColor={"grey.darker"}
            flexDirection={"column"}
        >
            <NavigationUserDetails />
            <Box
                as={"ul"}
                m={0}
                p={0}
                borderTop={"1px solid"}
                borderTopColor={"grey.light"}
                flexGrow={1}
            >
                <NavigationGroup title={i18n.dashboard.title} to={"/"} />
                <NavigationGroup
                    title={i18n.auth.title}
                    items={[
                        { to: "auth/user", label: i18n.auth.items.user },
                        { to: "auth/client", label: i18n.auth.items.client }
                    ]}
                />
            </Box>
            <LanguageSwitch />
        </Sidebar>
    );
};
