import React from "react";
import { useSelector } from "react-redux";
import { Icon, Icons } from "../ui/icon/icon";
import { Sidebar } from "../ui/sidebar/sidebar";
import { DeviceSize } from "../../types/device";
import { Box } from "../ui/structure/box";
import { resolveBreakpoint } from "../../utils/component";
import { selectDeviceSize } from "../../store/selectors/device";

interface TopMenuProps {
    onMenuButtonClick: () => void;
}

export const TopMenu: React.FC<TopMenuProps> = (props: TopMenuProps) => {
    const deviceSize: DeviceSize = useSelector(selectDeviceSize);

    return (
        <Sidebar
            direction={"top"}
            fixed
            backgroundColor={"white"}
            size={deviceSize === DeviceSize.S ? "60px" : "90px"}
            zIndex={3}
            borderBottom={"8px solid"}
            borderBottomColor={"primary"}
            pl={4}
            alignItems={"center"}
        >
            {deviceSize !== DeviceSize.L && (
                <Box
                    as={"button"}
                    backgroundColor={"transparent"}
                    border={"none"}
                    onClick={() => {
                        props.onMenuButtonClick();
                    }}
                    mr={4}
                    p={"10px"}
                >
                    <Icon icon={Icons.Menu} color={"grey.dark"} size={"l"} />
                </Box>
            )}
            <Icon icon={Icons.BrandLogo} height={resolveBreakpoint(["80%", "60%"], deviceSize)} />
        </Sidebar>
    );
};
