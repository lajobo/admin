import styled, { StyledComponent } from "styled-components";
import {
    background,
    BackgroundProps,
    border,
    BorderProps,
    color,
    ColorProps,
    display,
    DisplayProps,
    flexbox,
    FlexboxProps,
    fontFamily,
    FontFamilyProps,
    fontSize,
    FontSizeProps,
    fontWeight,
    FontWeightProps,
    height,
    HeightProps,
    layout,
    LayoutProps,
    letterSpacing,
    LetterSpacingProps,
    lineHeight,
    LineHeightProps,
    maxWidth,
    MaxWidthProps,
    position,
    PositionProps,
    shadow,
    ShadowProps,
    space,
    SpaceProps,
    textAlign,
    TextAlignProps,
    width,
    WidthProps
} from "styled-system";
import React from "react";
import { Theme } from "../../../types/ui";
import { ThemeProps } from "../../../utils/interpolation/theme";
import { boxSizing, BoxSizingProps } from "../../../utils/interpolation/boxSizing";
import { cursor, CursorProps } from "../../../utils/interpolation/cursor";
import { after, AfterProps } from "../../../utils/interpolation/after";
import { transform, TransformProps } from "../../../utils/interpolation/transform";
import { RefProps } from "../../../utils/interpolation/ref";
import { transition, TransitionProps } from "../../../utils/interpolation/transition";
import { filter, FilterProps } from "../../../utils/interpolation/filter";
import { backdropFilter, BackdropFilterProps } from "../../../utils/interpolation/backdropFilter";
import { pointerEvents, PointerEventsProps } from "../../../utils/interpolation/pointerEvents";
import { borderCollapse, BorderCollapseProps } from "../../../utils/interpolation/borderCollapse";
import { borderSpacing, BorderSpacingProps } from "../../../utils/interpolation/borderSpacing";
import { style, StyleProps } from "../../../utils/interpolation/style";
import { PolymorphProps } from "../../../utils/interpolation/polymorph";
import { StringColorProps } from "../../../utils/interpolation/color";
import { gap, GapProps } from "../../../utils/interpolation/gap";
import { animation, AnimationProps } from "../../../utils/interpolation/animation";

export type BoxProps =
    React.HTMLAttributes<HTMLDivElement> &
    SpaceProps &
    WidthProps &
    LayoutProps &
    MaxWidthProps &
    FontSizeProps &
    ColorProps &
    TextAlignProps &
    LineHeightProps &
    FontWeightProps &
    LetterSpacingProps &
    FontFamilyProps &
    HeightProps &
    DisplayProps &
    PositionProps &
    BackgroundProps &
    BorderProps &
    FlexboxProps &
    ShadowProps &
    ThemeProps &
    BoxSizingProps &
    CursorProps &
    AfterProps &
    TransformProps &
    RefProps &
    TransitionProps &
    FilterProps &
    BackdropFilterProps &
    PointerEventsProps &
    BorderCollapseProps &
    BorderSpacingProps &
    StyleProps &
    PolymorphProps &
    StringColorProps &
    GapProps &
    AnimationProps;

export const Box: StyledComponent<"div", Theme, BoxProps> = styled.div`
    ${space}
    ${layout}
    ${width}
    ${maxWidth}
    ${fontSize}
    ${color}
    ${textAlign}
    ${lineHeight}
    ${fontWeight}
    ${letterSpacing}
    ${fontFamily}
    ${height}
    ${display}
    ${position}
    ${background}
    ${border}
    ${flexbox}
    ${shadow}
    ${boxSizing}
    ${cursor}
    ${after}
    ${transform}
    ${transition}
    ${filter}
    ${backdropFilter}
    ${pointerEvents}
    ${borderCollapse}
    ${borderSpacing}
    ${gap}
    ${animation}
    ${style}
`;
