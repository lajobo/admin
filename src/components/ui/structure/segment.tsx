import React, { PropsWithChildren, useContext } from "react";
import { ThemeContext } from "styled-components";
import { Flex, FlexProps } from "./flex";
import { Theme } from "../../../types/ui";

interface SegmentProps extends FlexProps {
    stacked?: boolean;
    placeholder?: boolean;
}

export const Segment: React.FC<SegmentProps> = ({
    stacked,
    placeholder,
    children,
    ...props
}: PropsWithChildren<SegmentProps>) => {
    const theme: Theme = useContext(ThemeContext);
    const boxShadow: string = placeholder
        ? `0 2px 25px 0 ${theme.colors.grey.lighter} inset`
        : `0 0 6px 2px ${theme.colors.grey.lighter}`;

    return (
        <Flex
            position={"relative"}
            px={4}
            py={3}
            borderRadius={"default"}
            border={"1px solid"}
            borderColor={"grey.light"}
            color={"body"}
            boxSizing={"border-box"}
            flexDirection={"column"}
            boxShadow={boxShadow}
            background={placeholder ? "grey.pale" : "white"}
            after={stacked && `
                content: '';
                position: absolute;
                bottom: -3px;
                left: 0;
                border-top: 1px solid;
                border-top-color: ${theme.colors.grey.light};
                background: rgba(0,0,0,.03);
                width: 100%;
                height: 6px;
                visibility: visible;
            `}
            {...props}
        >
            {children}
        </Flex>
    );
};

Segment.defaultProps = {
    stacked: false,
    placeholder: false
};
