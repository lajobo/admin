/* eslint-disable object-curly-newline */
import React, { PropsWithChildren } from "react";
import { useSelector } from "react-redux";
import { Flex, FlexProps } from "./flex";
import { Icon, Icons } from "../icon/icon";
import { Heading } from "../typography/heading";
import { AuthStates } from "../../../types/auth";
import { selectAuthState } from "../../../store/selectors/auth";

export enum PageWidth {
    Fullwidth,
    L,
    M,
    S
}

const widths: { [width in PageWidth]: string } = {
    [PageWidth.Fullwidth]: undefined,
    [PageWidth.L]: "64em",
    [PageWidth.M]: "40em",
    [PageWidth.S]: "25em"
};

interface PageProps extends FlexProps {
    width: PageWidth;
    center?: boolean;
    title?: string;
    titleIcon?: Icons;
    innerBackgroundColor?: string;
    innerBorderColor?: string;
}

export const Page: React.FC<PageProps> = ({
    width,
    center,
    title,
    titleIcon,
    children,
    innerBackgroundColor,
    innerBorderColor,
    bg,
    background,
    backgroundColor,
    ...props
}: PropsWithChildren<PageProps>) => {
    const authState: AuthStates = useSelector(selectAuthState);
    const contentStyle: FlexProps = {
        width: "100%",
        maxWidth: [undefined, widths[width]],
        mx: "auto",
        p: 4,
        flexDirection: "column",
        justifyContent: center && "center",
        backgroundColor: innerBackgroundColor,
        boxSizing: "border-box",
        ...props
    };

    return (
        <Flex
            flexDirection={"column"}
            width={authState !== AuthStates.LoggedIn ? "100%" : ["100%", "100%", "calc(100% - 250px)"]}
            height={"100%"}
            bg={bg}
            background={background}
            backgroundColor={backgroundColor}
            boxSizing={"border-box"}
            ml={authState !== AuthStates.LoggedIn ? undefined : [undefined, undefined, "250px"]}
            p={[undefined, 4]}

        >
            {(title || titleIcon) && (
                <Flex
                    {...contentStyle}
                    mb={4}
                    boxSizing={"border-box"}
                    borderRadius={"default"}
                    border={[undefined, "1px solid"]}
                    borderColor={[undefined, innerBorderColor]}
                >
                    <Flex
                        flexDirection={"row"}
                        borderBottom={"2px solid"}
                        borderBottomColor={"grey.pale"}
                        alignItems={"flex-end"}
                    >
                        {titleIcon && (
                            <Icon icon={titleIcon} size={"xl"} pr={2} mb={4} />
                        )}
                        {title && (
                            <Heading
                                size={"xl"}
                                content={title}
                            />
                        )}
                    </Flex>
                </Flex>
            )}
            <Flex
                {...contentStyle}
                flexGrow={1}
                borderRadius={"default"}
                border={[undefined, "1px solid"]}
                borderColor={[undefined, innerBorderColor]}
            >
                {children}
            </Flex>
        </Flex>
    );
};

Page.defaultProps = {
    center: false,
    title: undefined,
    titleIcon: undefined,
    innerBackgroundColor: "white",
    innerBorderColor: "border"
};
