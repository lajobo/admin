import React from "react";
import { Box, BoxProps } from "../structure/box";

type TableBodyProps = BoxProps & React.HTMLAttributes<HTMLTableSectionElement>;

export const TableBody: React.FC<TableBodyProps> = (props: TableBodyProps) => (
    <Box as={"tbody"} {...props} />
);
