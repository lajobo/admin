import React from "react";
import { Box, BoxProps } from "../structure/box";

type TableProps = BoxProps;

export const Table: React.FC<TableProps> = (props: TableProps) => (
    <Box as={"table"} {...props} />
);
