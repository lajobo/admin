import React from "react";
import { Box, BoxProps } from "../structure/box";

type TableDataProps = BoxProps & React.TdHTMLAttributes<HTMLTableDataCellElement>;

export const TableData: React.FC<TableDataProps> = (props: TableDataProps) => (
    <Box as={"td"} {...props} />
);
