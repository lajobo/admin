import React from "react";
import { Box, BoxProps } from "../structure/box";

type TableheadProps = BoxProps & React.HTMLAttributes<HTMLTableSectionElement>;

export const Tablehead: React.FC<TableheadProps> = (props: TableheadProps) => (
    <Box as={"thead"} {...props} />
);
