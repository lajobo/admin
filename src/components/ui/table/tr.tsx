import React from "react";
import { Box, BoxProps } from "../structure/box";

type TableRowProps = BoxProps & React.HTMLAttributes<HTMLTableRowElement>;

export const TableRow: React.FC<TableRowProps> = (props: TableRowProps) => (
    <Box as={"tr"} {...props} />
);
