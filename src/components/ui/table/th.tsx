import React from "react";
import { Box, BoxProps } from "../structure/box";

type TableHeaderProps = BoxProps & React.ThHTMLAttributes<HTMLTableHeaderCellElement>;

export const TableHeader: React.FC<TableHeaderProps> = (props: TableHeaderProps) => (
    <Box as={"th"} {...props} />
);
