import React from "react";
import { Box, BoxProps } from "../structure/box";

type TablefootProps = BoxProps & React.HTMLAttributes<HTMLTableSectionElement>;

export const Tablefoot: React.FC<TablefootProps> = (props: TablefootProps) => (
    <Box as={"tfoot"} {...props} />
);
