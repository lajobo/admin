import React, { useContext, useRef } from "react";
import styled, { StyledComponent, ThemeContext } from "styled-components";
import {
    background,
    BackgroundProps,
    border,
    BorderProps,
    color,
    ColorProps,
    flex,
    flexbox,
    FlexboxProps,
    FlexProps,
    grid,
    GridProps,
    layout,
    LayoutProps,
    position,
    PositionProps,
    space,
    SpaceProps,
    typography,
    TypographyProps
} from "styled-system";
import { Flex, FlexProps as FlexComponentProps } from "../structure/flex";
import { Text } from "../typography/text";
import { ThemeProps } from "../../../utils/interpolation/theme";
import { Theme } from "../../../types/ui";
import { Icon, Icons } from "../icon/icon";
import { useEventListener } from "../../../hooks/eventListener";

type StyledRadioProps =
    LayoutProps &
    GridProps &
    SpaceProps &
    PositionProps &
    ColorProps &
    BackgroundProps &
    FlexProps &
    FlexboxProps &
    TypographyProps &
    BorderProps &
    ThemeProps;

const StyledRadio: StyledComponent<"input", Theme, StyledRadioProps> = styled.input`
    ${space}
    ${layout}
    ${grid}
    ${position}
    ${color}
    ${background}
    ${flex}
    ${flexbox}
    ${typography}
    ${border}
`;

export interface RadioOption {
    value: string;
    label: string;
}

interface RadioButtonProps extends React.InputHTMLAttributes<HTMLInputElement> {
    label: string;
    box: FlexComponentProps;
    loading?: boolean;
    hasErrors?: boolean;
    hasWarnings?: boolean;
    onFocusOut?: () => void;
}

export const RadioButton: React.FC<RadioButtonProps> = ({
    label,
    box,
    disabled,
    loading,
    hasErrors,
    hasWarnings,
    onFocusOut,
    ...props
}: RadioButtonProps) => {
    const theme: Theme = useContext(ThemeContext);
    const radioRef: React.RefObject<HTMLInputElement> = useRef<HTMLInputElement>();

    useEventListener("focusout", () => {
        if (onFocusOut) {
            onFocusOut();
        }
    }, radioRef?.current);

    return (
        <Flex
            flexDirection={"row"}
            styleProps={`
                & input:not([disabled]):focus + div {
                    border: ${theme.borders.focus};
                    box-shadow: ${theme.shadows.focus};
                }
            `}
            {...box}
        >
            <StyledRadio
                ref={radioRef}
                type={"radio"}
                width={0}
                height={0}
                disabled={disabled}
                {...props}
            />
            <Flex
                width={"18px"}
                height={"18px"}
                backgroundColor={hasErrors ? "state.error.light" : (hasWarnings ? "state.warning.light" : "grey.white")}
                border={"1px solid"}
                borderColor={hasErrors ? "state.error.dark" : (hasWarnings ? "state.warning.dark" : "grey.medium")}
                borderRadius={"50%"}
                boxShadow={disabled || loading ? "disabled" : undefined}
                cursor={disabled || loading ? undefined : "pointer"}
                justifyContent={"center"}
                alignItems={"center"}
                onClick={() => {
                    if (radioRef && radioRef.current) {
                        radioRef.current.click();
                        radioRef.current.focus();
                    }
                }}
            >
                {props.checked && (
                    <Icon icon={Icons.Dot} size={"xs"} height={"8px"} />
                )}
            </Flex>
            <Text
                as={"label"}
                size={"s"}
                content={label}
                pl={3}
                cursor={disabled || loading ? undefined : "pointer"}
                color={
                    disabled || loading ? "grey.light" : (
                        hasErrors ? "state.error.dark" : (
                            hasWarnings ? "state.warning.dark" : undefined
                        )
                    )
                }
                htmlFor={props.id}
            />
        </Flex>
    );
};

RadioButton.defaultProps = {
    loading: false,
    hasErrors: false,
    hasWarnings: false
};
