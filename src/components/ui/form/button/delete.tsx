import React, { useContext, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { AnyAction, AsyncThunk, ThunkDispatch } from "@reduxjs/toolkit";
import { NavigateFunction, useNavigate } from "react-router";
import { CommonFormButtonDeleteModalI18n, CommonFormI18n } from "../../../../types/i18n";
import { selectI18n } from "../../../../store/selectors/i18n";
import { Button, ButtonProps } from "../button";
import { Modal } from "../../modal/modal";
import { replaceVariables } from "../../../../utils/i18n";
import { Text } from "../../typography/text";
import { FormDataState } from "../../../../types/reducers/formData";
import { State } from "../../../../types/store";
import { FormContextStructure } from "../../../../types/contexts/form";
import { FormContext } from "../../../../contexts/form";

interface ButtonDeleteProps extends ButtonProps {
    modalI18n?: Partial<CommonFormButtonDeleteModalI18n>;
    deleteFunction: AsyncThunk<any, any, {}>; // eslint-disable-line @typescript-eslint/no-explicit-any
    deleteFunctionPropModifier: (data: FormDataState) => object;
}

export const ButtonDelete: React.FC<ButtonDeleteProps> = ({
    modalI18n,
    deleteFunction,
    deleteFunctionPropModifier,
    ...props
}: ButtonDeleteProps) => {
    const i18n: CommonFormI18n = { ...useSelector(selectI18n).common.form };
    const combinedModalI18n: CommonFormButtonDeleteModalI18n = {
        ...i18n.button.deleteModal,
        ...modalI18n
    };
    const dispatch: ThunkDispatch<State, {}, AnyAction> = useDispatch();
    const formContext: FormContextStructure = useContext(FormContext);
    const [showModal, setShowModal] = useState<boolean>(false);
    const navigate: NavigateFunction = useNavigate();

    return (
        <>
            <Button
                type={"button"}
                content={i18n.button.delete}
                onClick={() => {
                    setShowModal(true);
                }}
                color={"danger"}
                {...props}
            />
            {showModal && (
                <Modal
                    title={replaceVariables(combinedModalI18n.title, formContext.formData)}
                    actions={[
                        {
                            text: combinedModalI18n.deleteButton,
                            color: "danger",
                            onClick: () => {
                                dispatch(deleteFunction(deleteFunctionPropModifier(formContext.formData)));
                                setShowModal(false);
                                navigate(-1);
                            }
                        },
                        {
                            text: combinedModalI18n.cancelButton,
                            color: "grey",
                            onClick: () => {
                                setShowModal(false);
                            }
                        }
                    ]}
                >
                    <Text content={replaceVariables(combinedModalI18n.message, formContext.formData)} contentAsHTML />
                </Modal>
            )}
        </>
    );
};
