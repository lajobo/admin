import React from "react";
import { useSelector } from "react-redux";
import { CommonFormI18n } from "../../../../types/i18n";
import { selectI18n } from "../../../../store/selectors/i18n";
import { Button, ButtonProps } from "../button";

interface ButtonSubmitProps extends ButtonProps {
    initial?: boolean;
}

export const ButtonSubmit: React.FC<ButtonSubmitProps> = ({
    initial,
    ...props
}: ButtonSubmitProps) => {
    const i18n: CommonFormI18n = useSelector(selectI18n).common.form;

    return (
        <Button content={initial ? i18n.button.create : i18n.button.update} {...props} />
    );
};

ButtonSubmit.defaultProps = { initial: false };
