import React from "react";
import { useSelector } from "react-redux";
import { NavigateFunction, useNavigate } from "react-router";
import { CommonFormI18n } from "../../../../types/i18n";
import { selectI18n } from "../../../../store/selectors/i18n";
import { Button, ButtonProps } from "../button";

interface ButtonCancelProps extends ButtonProps { }

export const ButtonCancel: React.FC<ButtonCancelProps> = (props: ButtonCancelProps) => {
    const i18n: CommonFormI18n = useSelector(selectI18n).common.form;
    const navigate: NavigateFunction = useNavigate();

    return (
        <Button
            type={"button"}
            content={i18n.button.cancel}
            onClick={() => {
                navigate(-1);
            }}
            color={"grey"}
            {...props}
        />
    );
};
