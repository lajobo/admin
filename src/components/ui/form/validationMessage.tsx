import React from "react";
import { ValidationResult } from "../../../types/contexts/validation";
import { Text } from "../typography/text";
import { Flex, FlexProps } from "../structure/flex";

interface ValidationMessageProps extends ValidationResult, FlexProps { }

export const ValidationMessage: React.FC<ValidationMessageProps> = ({
    valid,
    errors,
    warnings,
    ...props
}: ValidationMessageProps) => {
    const hasErrors: boolean = (errors || []).length > 0;
    const hasWarnings: boolean = (warnings || []).length > 0;
    const validationMessages: string[] = hasErrors
        ? [...new Set(errors)]
        : (
            hasWarnings
                ? [...new Set(warnings)]
                : undefined
        );

    return valid || !validationMessages ? null : (
        <Flex flexDirection={"column"} {...props}>
            {validationMessages.map((validationMessage: string) => (
                <Text
                    key={validationMessage}
                    content={validationMessage}
                    size={"xs"}
                    color={`state.${hasErrors ? "error" : "warning"}.dark`}
                />
            ))}
        </Flex>
    );
};
