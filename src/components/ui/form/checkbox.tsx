import styled, { StyledComponent, ThemeContext } from "styled-components";
import {
    background,
    BackgroundProps,
    border,
    BorderProps,
    color,
    ColorProps,
    flex,
    flexbox,
    FlexboxProps,
    FlexProps,
    grid,
    GridProps,
    layout,
    LayoutProps,
    position,
    PositionProps,
    space,
    SpaceProps,
    typography,
    TypographyProps
} from "styled-system";
import React, { PropsWithChildren, useContext, useEffect, useRef } from "react";
import { Theme } from "../../../types/ui";
import { ThemeProps } from "../../../utils/interpolation/theme";
import { FormContextData, useFormContext } from "../../../hooks/formContext";
import { Flex } from "../structure/flex";
import { BoxProps } from "../structure/box";
import { Text } from "../typography/text";
import { Icon, Icons } from "../icon/icon";
import { log } from "../../../utils/logger";
import { LogLevels } from "../../../types/logger";
import { useValidationContextProvider, ValidationContextProviderData } from "../../../hooks/validationContextProvider";
import { getFormElementStates } from "../../../utils/form";
import { ValidationContext } from "../../../contexts/validation";
import { ValidationMessage } from "./validationMessage";
import { useEventListener } from "../../../hooks/eventListener";

type StyledCheckboxProps =
    LayoutProps &
    GridProps &
    SpaceProps &
    PositionProps &
    ColorProps &
    BackgroundProps &
    FlexProps &
    FlexboxProps &
    TypographyProps &
    BorderProps &
    ThemeProps;

const StyledCheckbox: StyledComponent<"input", Theme, StyledCheckboxProps> = styled.input`
    ${space}
    ${layout}
    ${grid}
    ${position}
    ${color}
    ${background}
    ${flex}
    ${flexbox}
    ${typography}
    ${border}
`;

interface CheckboxProps extends React.InputHTMLAttributes<HTMLInputElement> {
    name: string;
    label?: string;
    compact?: boolean;
    box?: BoxProps;
    loading?: boolean;
}

export const Checkbox: React.FC<CheckboxProps> = ({
    label,
    compact,
    box,
    loading,
    children,
    ...props
}: PropsWithChildren<CheckboxProps>) => {
    const validationContextData: ValidationContextProviderData = useValidationContextProvider();
    const theme: Theme = useContext(ThemeContext);
    const formContextData: FormContextData = useFormContext(
        props.name,
        props.id,
        props.checked,
        props.defaultChecked,
        props.onChange,
        validationContextData.validate,
        validationContextData.setFormErrors,
        false
    );
    const ref: React.RefObject<HTMLInputElement> = useRef<HTMLInputElement>();
    const [isDisabled, isLoading, hasErrors, hasWarnings] = getFormElementStates(
        props.disabled,
        loading || formContextData.loading,
        validationContextData.validationResult
    );

    useEventListener("focusout", () => {
        validationContextData.validate(formContextData.value);
    }, ref?.current);

    useEffect(() => {
        // uncontrolled state not supported due to the styling of the checkbox
        if (props.defaultChecked) {
            log(
                LogLevels.Error,
                "A Checkbox component does not support the uncontrolled state. The prop \"defaultChecked\" is " +
                "therefore forbidden. Consider using the form components formData and onSubmit prop instead."
            );
        }
    }, []);

    return (
        <Flex flexDirection={"column"}>
            <Flex
                flexDirection={"row"}
                alignItems={"center"}
                styleProps={`
                    & input:focus + div {
                        border: ${theme.borders.focus};
                        box-shadow: ${theme.shadows.focus};
                    }
                `}
                {...box}
            >
                <StyledCheckbox
                    ref={ref}
                    type={"checkbox"}
                    width={"0"}
                    height={"0"}
                    checked={Boolean(formContextData.value)}
                    {...props}
                    onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                        formContextData.onChange(event, !formContextData.value);
                        validationContextData.validate(!formContextData.value);
                    }}
                    id={formContextData.id}
                    disabled={isDisabled || isLoading}
                />
                <Flex
                    width={"20px"}
                    height={"20px"}
                    backgroundColor={hasErrors ? "state.error.light" : (hasWarnings ? "state.warning.light" : "white")}
                    border={"1px solid"}
                    borderColor={hasErrors ? "state.error.dark" : (hasWarnings ? "state.warning.dark" : "grey.medium")}
                    borderRadius={"default"}
                    boxShadow={isDisabled || isLoading ? "disabled" : undefined}
                    cursor={isDisabled || isLoading ? undefined : "pointer"}
                    justifyContent={"center"}
                    alignItems={"center"}
                    onClick={() => {
                        if (ref && ref.current) {
                            ref.current.click();
                            ref.current.focus();
                        }
                    }}
                >
                    {(formContextData.value || props.checked) && (
                        <Icon icon={Icons.Cross} size={"xs"} />
                    )}
                </Flex>
                {label && (
                    <Text
                        as={"label"}
                        size={"s"}
                        content={label}
                        pl={3}
                        cursor={isDisabled || isLoading ? undefined : "pointer"}
                        color={
                            isDisabled || isLoading ? "grey.light" : (
                                hasErrors ? "state.error.dark" : (
                                    hasWarnings ? "state.warning.dark" : "body"
                                )
                            )
                        }
                        htmlFor={formContextData.id}
                    />
                )}
                <ValidationContext.Provider value={validationContextData.context}>
                    {children}
                </ValidationContext.Provider>
            </Flex>
            <ValidationMessage {...validationContextData.validationResult} mt={2} />
        </Flex>
    );
};

Checkbox.defaultProps = {
    label: undefined,
    compact: false,
    box: undefined,
    loading: false
};
