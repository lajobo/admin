import styled, { StyledComponent } from "styled-components";
import {
    background,
    BackgroundProps,
    border,
    BorderProps,
    color,
    ColorProps,
    flex,
    flexbox,
    FlexboxProps,
    FlexProps,
    grid,
    GridProps,
    layout,
    LayoutProps,
    position,
    PositionProps,
    space,
    SpaceProps,
    typography,
    TypographyProps
} from "styled-system";
import React, { PropsWithChildren, useRef } from "react";
import { Theme } from "../../../types/ui";
import { boxSizing, BoxSizingProps } from "../../../utils/interpolation/boxSizing";
import { cursor, CursorProps } from "../../../utils/interpolation/cursor";
import { ThemeProps } from "../../../utils/interpolation/theme";
import { placeholderColor, PlaceholderColorProps } from "../../../utils/interpolation/placeholderColor";
import { FormContextData, useFormContext } from "../../../hooks/formContext";
import { resize, ResizeProps } from "../../../utils/interpolation/resize";
import { useValidationContextProvider, ValidationContextProviderData } from "../../../hooks/validationContextProvider";
import { getFormElementStates } from "../../../utils/form";
import { useEventListener } from "../../../hooks/eventListener";
import { ValidationContext } from "../../../contexts/validation";
import { ValidationMessage } from "./validationMessage";
import { Flex } from "../structure/flex";

type StyledTextareaProps =
    React.TextareaHTMLAttributes<HTMLTextAreaElement> &
    LayoutProps &
    GridProps &
    SpaceProps &
    PositionProps &
    ColorProps &
    BackgroundProps &
    FlexProps &
    FlexboxProps &
    TypographyProps &
    BorderProps &
    ThemeProps &
    BoxSizingProps &
    CursorProps &
    PlaceholderColorProps &
    ResizeProps;

const StyledTextarea: StyledComponent<"textarea", Theme, StyledTextareaProps> = styled.textarea`
    ${(ssProps: StyledTextareaProps) => `
        &:focus {
            outline: 0;
            border: ${ssProps.theme.borders.focus};
            box-shadow: ${ssProps.theme.shadows.focus};
        }
    `}
    ${(ssProps: StyledTextareaProps) => `
        &:disabled {
            box-shadow: ${ssProps.theme.shadows.disabled};
            color: ${ssProps.theme.colors.grey.light};
        }
    `}
    ${space}
    ${layout}
    ${grid}
    ${position}
    ${color}
    ${background}
    ${flex}
    ${flexbox}
    ${typography}
    ${border}
    ${boxSizing}
    ${cursor}
    ${placeholderColor}
    ${resize}
`;

interface TextareaProps extends StyledTextareaProps {
    loading?: boolean;
    value?: string;
    defaultValue?: string;
}

export const Textarea: React.FC<TextareaProps> = ({
    loading,
    children,
    ...props
}: PropsWithChildren<TextareaProps>) => {
    const validationContextData: ValidationContextProviderData = useValidationContextProvider();
    const formContextData: FormContextData = useFormContext(
        props.name,
        props.id,
        props.value,
        props.defaultValue,
        props.onChange,
        validationContextData.validate,
        validationContextData.setFormErrors
    );
    const ref: React.RefObject<HTMLTextAreaElement> = useRef<HTMLTextAreaElement>();
    const [isDisabled, isLoading, hasErrors, hasWarnings] = getFormElementStates(
        props.disabled,
        loading || formContextData.loading,
        validationContextData.validationResult
    );

    useEventListener("focusout", () => {
        validationContextData.validate(formContextData.value);
    }, ref?.current);

    return (
        <Flex flexDirection={"column"}>
            <StyledTextarea
                ref={ref}
                fontSize={1}
                lineHeight={1}
                px={4}
                py={3}
                borderRadius={"default"}
                border={"1px solid"}
                borderColor={hasErrors ? "state.error.dark" : (hasWarnings ? "state.warning.dark" : "grey.light")}
                placeholderColor={hasErrors ? "state.error.dark" : (hasWarnings ? "state.warning.dark" : "grey.light")}
                backgroundColor={hasErrors ? "state.error.light" : (hasWarnings ? "state.warning.light" : undefined)}
                color={"body"}
                display={"inline-block"}
                width={"100%"}
                maxWidth={"100%"}
                boxSizing={"border-box"}
                rows={3}
                resize={"vertical"}
                minHeight={"70px"}
                value={String(formContextData.value)}
                {...props}
                onChange={(event: React.ChangeEvent<HTMLTextAreaElement>) => {
                    formContextData.onChange(event, event.target.value);
                }}
                id={formContextData.id}
                disabled={isDisabled || isLoading}
            />
            <ValidationContext.Provider value={validationContextData.context}>
                {children}
            </ValidationContext.Provider>
            <ValidationMessage {...validationContextData.validationResult} mt={2} />
        </Flex>
    );
};
