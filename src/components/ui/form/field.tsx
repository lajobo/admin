import styled, { StyledComponent } from "styled-components";
import React, { PropsWithChildren, useContext, useState } from "react";
import { Box, BoxProps } from "../structure/box";
import { Theme } from "../../../types/ui";
import { FieldContextStructure } from "../../../types/contexts/field";
import { FieldContext } from "../../../contexts/field";
import { Label } from "./label";
import { FormContextStructure } from "../../../types/contexts/form";
import { FormContext } from "../../../contexts/form";
import { Text } from "../typography/text";

type StyledFieldProps = BoxProps & { };

const StyledField: StyledComponent<"div", Theme, StyledFieldProps> = styled(Box)``;

interface FieldProps extends StyledFieldProps {
    label?: string;
    width?: number;
}

export const Field: React.FC<FieldProps> = ({
    label,
    width,
    children,
    ...props
}: PropsWithChildren<FieldProps>) => {
    const formContext: FormContextStructure = useContext(FormContext);
    const [fieldId, setFieldId] = useState<string>();
    const [fieldRequired, setFieldRequired] = useState<boolean>(false);

    const combinedLabel: string = label || (formContext?.active && formContext?.i18n && formContext?.i18n[fieldId]);

    const context: FieldContextStructure = {
        active: true,
        fieldId,
        setFieldId: (id: string) => { setFieldId(id); },
        setFieldRequired: (required: boolean) => { setFieldRequired(required); }
    };

    return (
        <StyledField
            display={"flex"}
            flexDirection={"column"}
            flexGrow={width}
            width={"100%"}
            mb={"auto"}
            {...props}
        >
            <FieldContext.Provider value={context}>
                {combinedLabel && (
                    <Label htmlFor={fieldId}>
                        {combinedLabel}
                        {fieldRequired && (
                            <sup><Text color={"error"} size={"xs"} content={"*"} /></sup>
                        )}
                    </Label>
                )}
                {children}
            </FieldContext.Provider>
        </StyledField>
    );
};

Field.defaultProps = {
    label: undefined,
    width: 1
};
