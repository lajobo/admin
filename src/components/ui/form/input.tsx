import styled, { StyledComponent } from "styled-components";
import {
    background,
    BackgroundProps,
    border,
    BorderProps,
    color,
    ColorProps,
    flex,
    flexbox,
    FlexboxProps,
    FlexProps,
    grid,
    GridProps,
    layout,
    LayoutProps,
    position,
    PositionProps,
    space,
    SpaceProps,
    typography,
    TypographyProps
} from "styled-system";
import React, { PropsWithChildren, useRef } from "react";
import { Theme } from "../../../types/ui";
import { ThemeProps } from "../../../utils/interpolation/theme";
import { boxSizing, BoxSizingProps } from "../../../utils/interpolation/boxSizing";
import { cursor, CursorProps } from "../../../utils/interpolation/cursor";
import { placeholderColor, PlaceholderColorProps } from "../../../utils/interpolation/placeholderColor";
import { FormContextData, useFormContext } from "../../../hooks/formContext";
import { Icon, Icons } from "../icon/icon";
import { Flex } from "../structure/flex";
import { useEventListener } from "../../../hooks/eventListener";
import { useValidationContextProvider, ValidationContextProviderData } from "../../../hooks/validationContextProvider";
import { ValidationContext } from "../../../contexts/validation";
import { ValidationMessage } from "./validationMessage";
import { getFormElementStates } from "../../../utils/form";

type StyledInputProps =
    React.InputHTMLAttributes<HTMLInputElement> &
    LayoutProps &
    GridProps &
    SpaceProps &
    PositionProps &
    ColorProps &
    BackgroundProps &
    FlexProps &
    FlexboxProps &
    TypographyProps &
    BorderProps &
    ThemeProps &
    BoxSizingProps &
    CursorProps &
    PlaceholderColorProps;

const StyledInput: StyledComponent<"input", Theme, StyledInputProps> = styled.input`
    ${(ssProps: StyledInputProps) => `
    &:focus {
        outline: 0;
        border: ${ssProps.theme.borders.focus};
        box-shadow: ${ssProps.theme.shadows.focus};
    }
    `}
    ${(ssProps: StyledInputProps) => `
    &:disabled {
        box-shadow: ${ssProps.theme.shadows.disabled};
        color: ${ssProps.theme.colors.grey.light};
    }
    `}
    ${space}
    ${layout}
    ${grid}
    ${position}
    ${color}
    ${background}
    ${flex}
    ${flexbox}
    ${typography}
    ${border}
    ${boxSizing}
    ${cursor}
    ${placeholderColor}
`;

interface InputProps extends StyledInputProps {
    name: string;
    type?: "color" | "date" | "datetime-local" | "month" | "number" | "password" | "range" | "search" | "text" | "time" | "week";
    icon?: Icons;
    iconPosition?: "left" | "right";
    loading?: boolean;
    value?: string;
    defaultValue?: string;
}

export const Input: React.FC<InputProps> = ({
    icon,
    iconPosition,
    loading,
    children,
    ...props
}: PropsWithChildren<InputProps>) => {
    const validationContextData: ValidationContextProviderData = useValidationContextProvider();
    const formContextData: FormContextData = useFormContext(
        props.name,
        props.id,
        props.value,
        props.defaultValue,
        props.onChange,
        validationContextData.validate,
        validationContextData.setFormErrors
    );
    const ref: React.RefObject<HTMLInputElement> = useRef();
    const [isDisabled, isLoading, hasErrors, hasWarnings] = getFormElementStates(
        props.disabled,
        loading || formContextData.loading,
        validationContextData.validationResult
    );

    useEventListener("focusout", () => {
        validationContextData.validate(formContextData.value);
    }, ref?.current);

    return (
        <Flex flexDirection={"column"}>
            <Flex position={"relative"}>
                {icon && iconPosition === "left" && (
                    <Icon
                        icon={icon}
                        position={"absolute"}
                        top={"50%"}
                        icontransform={"translateY(-50%)"}
                        fill={hasErrors ? "state.error.dark" : (hasWarnings ? "state.warning.dark" : undefined)}
                        left={"9px"}
                    />
                )}
                <StyledInput
                    ref={ref}
                    fontSize={1}
                    lineHeight={1}
                    pl={icon && iconPosition === "left" ? "36px" : 4}
                    pr={icon && iconPosition === "right" ? "36px" : 4}
                    py={3}
                    borderRadius={"default"}
                    border={"1px solid"}
                    borderColor={hasErrors ? "state.error.dark" : (hasWarnings ? "state.warning.dark" : "grey.light")}
                    backgroundColor={hasErrors ? "state.error.light" : (hasWarnings ? "state.warning.light" : undefined)}
                    placeholderColor={hasErrors ? "state.error.dark" : (hasWarnings ? "state.warning.dark" : "grey.light")}
                    color={"body"}
                    display={"inline-block"}
                    width={"100%"}
                    boxSizing={"border-box"}
                    value={String(formContextData.value)}
                    placeholder={formContextData.placeholder}
                    {...props}
                    onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                        formContextData.onChange(event, event.target.value);

                        // only validate on change if the input had errors or warnings before to sign when they are
                        // fixed. but dont validate on each change this that would lead to too much flickering
                        if (hasErrors || hasWarnings) {
                            validationContextData.validate(event.target.value);
                        }
                    }}
                    id={formContextData.id}
                    disabled={isDisabled || isLoading}
                />
                {icon && iconPosition === "right" && (
                    <Icon
                        icon={icon}
                        position={"absolute"}
                        top={"50%"}
                        icontransform={"translateY(-50%)"}
                        fill={hasErrors ? "state.error.dark" : (hasWarnings ? "state.warning.dark" : undefined)}
                        right={"9px"}
                    />
                )}
                <ValidationContext.Provider value={validationContextData.context}>
                    {children}
                </ValidationContext.Provider>
            </Flex>
            <ValidationMessage {...validationContextData.validationResult} mt={2} />
        </Flex>
    );
};

Input.defaultProps = {
    type: "text",
    icon: undefined,
    iconPosition: "left",
    loading: false
};
