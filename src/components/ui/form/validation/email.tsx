import React from "react";
import { useSelector } from "react-redux";
import { useValidationContext } from "../../../../hooks/validationContext";
import { FormDataValue } from "../../../../types/reducers/formData";
import { selectI18n } from "../../../../store/selectors/i18n";
import { ComponentUiFormValidationEmailI18n } from "../../../../types/i18n";

interface ValidationEmailProps {
    i18n?: Partial<ComponentUiFormValidationEmailI18n>;
}

export const ValidationEmail: React.FC<ValidationEmailProps> = (props: ValidationEmailProps) => {
    const i18n: ComponentUiFormValidationEmailI18n = {
        ...useSelector(selectI18n).components.ui.form.validation.email,
        ...props.i18n
    };
    useValidationContext((value: FormDataValue) => {
        const regExp: RegExp = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

        // this validation should succeed on an empty string to avoid double errors. if the field is required it should
        // receive a ValidationRequired to ensure that a value will be entered at all
        return String(value).length === 0 || regExp.test(String(value))
            ? { valid: true }
            : {
                valid: false,
                errors: [i18n.format]
            };
    });

    return null;
};
