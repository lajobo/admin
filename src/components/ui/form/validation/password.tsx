import React from "react";
import { useSelector } from "react-redux";
import { useValidationContext } from "../../../../hooks/validationContext";
import { FormDataValue } from "../../../../types/reducers/formData";
import { selectI18n } from "../../../../store/selectors/i18n";
import { ComponentUiFormValidationPasswordI18n } from "../../../../types/i18n";

interface ValidationPasswordProps {
    i18n?: Partial<ComponentUiFormValidationPasswordI18n>;
}

export const ValidationPassword: React.FC<ValidationPasswordProps> = (props: ValidationPasswordProps) => {
    const i18n: ComponentUiFormValidationPasswordI18n = {
        ...useSelector(selectI18n).components.ui.form.validation.password,
        ...props.i18n
    };
    useValidationContext((value: FormDataValue) => {
        const regExp: RegExp = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;

        // this validation should succeed on an empty string to avoid double errors. if the field is required it should
        // receive a ValidationRequired to ensure that a value will be entered at all
        return String(value).length === 0 || regExp.test(String(value))
            ? { valid: true }
            : {
                valid: false,
                errors: [i18n.format]
            };
    });

    return null;
};
