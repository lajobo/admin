import React, { useContext, useEffect } from "react";
import { useSelector } from "react-redux";
import { useValidationContext } from "../../../../hooks/validationContext";
import { FormDataValue } from "../../../../types/reducers/formData";
import { selectI18n } from "../../../../store/selectors/i18n";
import { ComponentUiFormValidationRequiredI18n } from "../../../../types/i18n";
import { FieldContextStructure } from "../../../../types/contexts/field";
import { FieldContext } from "../../../../contexts/field";

interface ValidationRequiredProps {
    i18n?: Partial<ComponentUiFormValidationRequiredI18n>;
}

export const ValidationRequired: React.FC<ValidationRequiredProps> = (props: ValidationRequiredProps) => {
    const fieldContext: FieldContextStructure = useContext<FieldContextStructure>(FieldContext);
    const i18n: ComponentUiFormValidationRequiredI18n = {
        ...useSelector(selectI18n).components.ui.form.validation.required,
        ...props.i18n
    };

    useValidationContext((value: FormDataValue) => (value
        ? { valid: true }
        : {
            valid: false,
            errors: [i18n.required]
        }));

    useEffect(() => {
        fieldContext.setFieldRequired(true);
    }, []);

    return null;
};
