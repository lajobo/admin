import styled, { StyledComponent } from "styled-components";
import {
    background,
    BackgroundProps,
    border,
    BorderProps,
    color,
    ColorProps,
    flex,
    flexbox,
    FlexboxProps,
    FlexProps,
    grid,
    GridProps,
    layout,
    LayoutProps,
    position,
    PositionProps,
    space,
    SpaceProps,
    typography,
    TypographyProps
} from "styled-system";
import React, { PropsWithChildren, useRef } from "react";
import { Theme } from "../../../types/ui";
import { ThemeProps } from "../../../utils/interpolation/theme";
import { boxSizing, BoxSizingProps } from "../../../utils/interpolation/boxSizing";
import { cursor, CursorProps } from "../../../utils/interpolation/cursor";
import { FormContextData, useFormContext } from "../../../hooks/formContext";
import { Flex } from "../structure/flex";
import { useValidationContextProvider, ValidationContextProviderData } from "../../../hooks/validationContextProvider";
import { useEventListener } from "../../../hooks/eventListener";
import { ValidationMessage } from "./validationMessage";
import { ValidationContext } from "../../../contexts/validation";
import { getFormElementStates } from "../../../utils/form";
import { PlaceholderColorProps } from "../../../utils/interpolation/placeholderColor";

type StyledSelectProps =
    React.SelectHTMLAttributes<HTMLSelectElement> &
    LayoutProps &
    GridProps &
    SpaceProps &
    PositionProps &
    ColorProps &
    BackgroundProps &
    FlexProps &
    FlexboxProps &
    TypographyProps &
    BorderProps &
    ThemeProps &
    BoxSizingProps &
    CursorProps &
    PlaceholderColorProps;

const StyledSelect: StyledComponent<"select", Theme, StyledSelectProps> = styled.select`
    ${(ssProps: StyledSelectProps) => `
        &:focus {
            outline: 0;
            border: ${ssProps.theme.borders.focus};
            box-shadow: ${ssProps.theme.shadows.focus};
        }
    `}
    ${(ssProps: StyledSelectProps) => `
    &:disabled {
        box-shadow: ${ssProps.theme.shadows.disabled};
        color: ${ssProps.theme.colors.grey.light};
    }
    `}
    
    ${space}
    ${layout}
    ${grid}
    ${position}
    ${color}
    ${background}
    ${flex}
    ${flexbox}
    ${typography}
    ${border}
    ${boxSizing}
    ${cursor}
`;

export interface SelectOption {
    value: string;
    label: string;
}

interface SelectProps extends StyledSelectProps {
    name: string;
    options: SelectOption[];
    addEmptyOption?: boolean;
    loading?: boolean;
    value?: string | number;
    defaultValue?: string | number;
}

export const Select: React.FC<SelectProps> = ({
    options,
    addEmptyOption,
    loading,
    children,
    ...props
}: PropsWithChildren<SelectProps>) => {
    const validationContextData: ValidationContextProviderData = useValidationContextProvider();
    const formContextData: FormContextData = useFormContext(
        props.name,
        props.id,
        props.value,
        props.defaultValue,
        props.onChange,
        validationContextData.validate,
        validationContextData.setFormErrors
    );
    const ref: React.RefObject<HTMLSelectElement> = useRef();

    // add an additional empty option if activated in the props
    const addAdditionalEmptyOption: boolean = addEmptyOption && !options.find((option: SelectOption) => !option.value);
    // adds an additional empty option which will be disabled if not activated in the props but the current value
    // is empty and no empty option was given in the props, since the first option would be selected by default otherwise
    const addAdditionalDisabledEmptyOption: boolean =
        !addAdditionalEmptyOption &&
        ["", undefined].includes(String(formContextData.value || props.value || props.defaultValue)) &&
        options.find((option: SelectOption) => option.value === "") === undefined;

    const [isDisabled, isLoading, hasErrors, hasWarnings] = getFormElementStates(
        props.disabled,
        loading || formContextData.loading,
        validationContextData.validationResult
    );

    useEventListener("focusout", () => {
        validationContextData.validate(formContextData.value);
    }, ref?.current);

    return (
        <Flex flexDirection={"column"}>
            <Flex position={"relative"}>
                <StyledSelect
                    ref={ref}
                    fontSize={1}
                    lineHeight={1}
                    px={4}
                    py={3}
                    borderRadius={"default"}
                    border={"1px solid"}
                    borderColor={hasErrors ? "state.error.dark" : (hasWarnings ? "state.warning.dark" : "grey.light")}
                    backgroundColor={
                        isDisabled ? "background" : (
                            hasErrors ? "state.error.light" : (
                                hasWarnings ? "state.warning.light" : "white"
                            )
                        )
                    }
                    placeholderColor={hasErrors ? "state.error.dark" : (hasWarnings ? "state.warning.dark" : "grey.light")}
                    color={"body"}
                    display={"inline-block"}
                    width={"100%"}
                    boxSizing={"border-box"}
                    value={String(formContextData.value)}
                    position={"relative"}
                    {...props}
                    onChange={(event: React.ChangeEvent<HTMLSelectElement>) => {
                        formContextData.onChange(event, event.target.value);
                        validationContextData.validate(event.target.value);
                    }}
                    id={formContextData.id}
                    disabled={isDisabled || isLoading}
                >
                    {(addAdditionalEmptyOption || addAdditionalDisabledEmptyOption) && (
                        <option disabled={addAdditionalDisabledEmptyOption} key={""} value={""} label={" "} />
                    )}
                    {options.map((option: SelectOption) => (
                        <option key={option.value} {...option} />
                    ))}
                </StyledSelect>
                <ValidationContext.Provider value={validationContextData.context}>
                    {children}
                </ValidationContext.Provider>
            </Flex>
            <ValidationMessage {...validationContextData.validationResult} mt={2} />
        </Flex>
    );
};

Select.defaultProps = {
    addEmptyOption: true,
    loading: false
};
