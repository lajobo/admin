/* eslint-disable */
import React, { PropsWithChildren, useContext, useEffect, useRef, useState } from "react";
import { Button as DefaultButton, ButtonProps as DefaultBottomProps } from "../button/button";
import { Spinner } from "../loader/spinner";
import { useWindowSize, WindowSize } from "../../../hooks/windowSize";
import { FormContextStructure } from "../../../types/contexts/form";
import { FormContext } from "../../../contexts/form";

export interface ButtonProps extends DefaultBottomProps {
    loading?: boolean;
    content?: string;
}

export const Button: React.FC<ButtonProps> = ({
    loading,
    content,
    children,
    ...props
}: PropsWithChildren<ButtonProps>) => {
    const childrenRef: React.RefObject<HTMLDivElement> = useRef();
    const [loadingSpinnerOffset, setLoadingSpinnerOffset] = useState<number>();
    const windowSize: WindowSize = useWindowSize();
    const formContext: FormContextStructure = useContext<FormContextStructure>(FormContext);

    useEffect(() => {
        if (childrenRef?.current) {
            // get children container width to place spinner correctly since the spinner should not change the
            // children position
            setLoadingSpinnerOffset(childrenRef.current.getBoundingClientRect().width);
        }
    }, [childrenRef, windowSize?.width]);

    return (
        <DefaultButton
            position={"relative"}
            {...props}
            disabled={props.disabled || loading || formContext.loading}
            mb={3}
        >
            {(loading || formContext.loading) && (
                <Spinner
                    size={"s"}
                    position={"absolute"}
                    left={"50%"}
                    top={"50%"}
                    transform={`translate(calc(-100% - ${(loadingSpinnerOffset / 2) + 10}px), -50%)`}
                    discreet
                    color={"body"}
                />
            )}
            <div ref={childrenRef}>
                {content || children}
            </div>
        </DefaultButton>
    );
};

Button.defaultProps = {
    color: "primary",
    appearance: "contained",
    loading: false,
    content: undefined
};
