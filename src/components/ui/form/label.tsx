import React, { PropsWithChildren } from "react";
import { Text, TextProps } from "../typography/text";

interface LabelProps extends TextProps {
    text?: string;
}

export const Label: React.FC<LabelProps> = ({
    text,
    children,
    ...props
}: PropsWithChildren<LabelProps>) => (!text && !children ? null : (
    <Text
        as={"label"}
        cursor={props.htmlFor ? "pointer" : undefined}
        size={"xs"}
        content={text}
        fontWeight={"bold"}
        mb={3}
        {...props}
    >
        {children}
    </Text>
));
