import React, { PropsWithChildren, useEffect, useReducer, useState } from "react";
import styled, { StyledComponent } from "styled-components";
import { Box, BoxProps } from "../structure/box";
import { Theme } from "../../../types/ui";
import { FormContextStructure, FormI18n } from "../../../types/contexts/form";
import { FormContext } from "../../../contexts/form";
import { FormDataReducer, formDataReducer, setValue } from "../../../reducers/formData";
import { FormDataState, FormDataValue, FormError } from "../../../types/reducers/formData";
import { log } from "../../../utils/logger";
import { LogLevels } from "../../../types/logger";
import { FormValidation } from "../../../types/contexts/validation";
import { ValidationMessage } from "./validationMessage";

type StyledFormProps = React.FormHTMLAttributes<HTMLFormElement> & BoxProps;

const StyledForm: StyledComponent<"form", Theme, StyledFormProps> = styled(Box)``;

interface FormProps extends StyledFormProps {
    i18n?: FormI18n;
    formData?: FormDataState;
    defaultFormData?: FormDataState;
    onFormDataChange?: (name: string, value: FormDataValue) => void;
    onFormSubmit?: (formData: FormDataState<any>, valid: boolean) => void; // eslint-disable-line @typescript-eslint/no-explicit-any
    loading?: boolean;
    errors?: FormError[];
    warnings?: string[];
}

export const Form: React.FC<FormProps> = ({
    i18n,
    formData,
    defaultFormData,
    onFormDataChange,
    onFormSubmit,
    loading,
    errors,
    warnings,
    children,
    ...props
}: PropsWithChildren<FormProps>) => {
    const [controlled, setControlled] = useState<boolean>(!!formData);
    const [formDataState, dispatch] = useReducer<FormDataReducer>(formDataReducer, defaultFormData || {});
    const [validations, setValidations] = useState<FormValidation[]>([]);
    const [formErrors, setFormErrors] = useState<FormError[]>([]);

    const addValidation: (validation: FormValidation) => () => void = (newValidation: FormValidation) => {
        setValidations((storedValidations: FormValidation[]) => [...storedValidations, newValidation]);

        return () => {
            setValidations((
                storedValidations: FormValidation[]
            ) => storedValidations.filter((storedValidation: FormValidation) => storedValidation !== newValidation));
        };
    };

    const context: FormContextStructure = {
        active: true,
        formData: controlled ? formData : formDataState,
        setValue: (name: string, value: FormDataValue) => {
            // forward change event to reducer only if component is uncontrolled
            if (!controlled) {
                dispatch(setValue(name, value));
            }

            // forward change event to props
            if (onFormDataChange) {
                onFormDataChange(name, value);
            }

            // remove formErrors
            setFormErrors((currentFormErrors: FormError[]) => currentFormErrors
                .filter((formError: FormError) => formError.field !== name));
        },
        loading,
        addValidation,
        i18n,
        errors: formErrors
    };

    useEffect(() => {
        setFormErrors(errors);
    }, [errors]);

    useEffect(() => {
        if (formData && defaultFormData) {
            log(
                LogLevels.Error,
                "Form elements must be either controlled or uncontrolled (specify either the formData prop, or the " +
                "defaultFormData prop, but not both). Decide between using a controlled or uncontrolled form " +
                "element and remove one of these props. More info: https://reactjs.org/link/controlled-components"
            );
        }
    }, []);

    useEffect(() => {
        if (!!formData !== controlled) {
            setControlled(!!formData);

            log(
                LogLevels.Warn,
                "A component is changing an uncontrolled form to be controlled. This is likely caused by the formData " +
                "changing from undefined to a defined value, which should not happen. Decide between using a controlled " +
                "or uncontrolled form element for the lifetime of the component. More info: " +
                "https://reactjs.org/link/controlled-components"
            );
        }
    }, [formData]);

    return (
        <StyledForm
            as={"form"}
            onSubmit={(event: React.FormEvent<HTMLFormElement>) => {
                event.preventDefault();

                const formValid: boolean = validations
                    // its important to call validation() before `&& valid` since validation() needs to be called in
                    // any way even if the form is already invalid to trigger and show all validations
                    .reduce<boolean>((valid: boolean, validation: FormValidation) => validation() && valid, true);

                onFormSubmit(context.formData, formValid);
            }}
            {...props}
        >
            <FormContext.Provider value={context}>
                {children}
            </FormContext.Provider>
            <ValidationMessage
                valid={(!errors || errors.length === 0) && (!warnings || warnings.length === 0)}
                errors={(errors || []).filter((error: FormError) => !error.field).map((error: FormError) => error.error)}
                warnings={warnings}
                mt={2}
            />
        </StyledForm>
    );
};

Form.defaultProps = { errors: [] };
