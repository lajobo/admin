import React, { PropsWithChildren, useEffect } from "react";
import { FormContextData, useFormContext } from "../../../hooks/formContext";
import { BoxProps } from "../structure/box";
import { Flex } from "../structure/flex";
import { RadioButton, RadioOption } from "./radioButton";
import { log } from "../../../utils/logger";
import { LogLevels } from "../../../types/logger";
import { useValidationContextProvider, ValidationContextProviderData } from "../../../hooks/validationContextProvider";
import { getFormElementStates } from "../../../utils/form";
import { ValidationContext } from "../../../contexts/validation";
import { ValidationMessage } from "./validationMessage";

interface RadioGroupProps extends React.InputHTMLAttributes<HTMLInputElement> {
    name: string;
    value?: string;
    defaultValue?: string;
    options: RadioOption[];
    nullable?: boolean;
    box?: BoxProps;
    loading?: boolean;
}

export const RadioGroup: React.FC<RadioGroupProps> = ({
    options,
    value,
    defaultValue,
    nullable,
    box,
    loading,
    children,
    ...props
}: PropsWithChildren<RadioGroupProps>) => {
    const validationContextData: ValidationContextProviderData = useValidationContextProvider();
    const formContextData: FormContextData = useFormContext(
        props.name,
        props.id,
        value,
        defaultValue,
        props.onChange,
        validationContextData.validate,
        validationContextData.setFormErrors
    );
    const [isDisabled, isLoading, hasErrors, hasWarnings] = getFormElementStates(
        props.disabled,
        loading || formContextData.loading,
        validationContextData.validationResult
    );

    useEffect(() => {
        // nullable not supported for un/controlled components due to native event handling not triggering on active
        // radio click
        if (!formContextData.active && nullable) {
            log(
                LogLevels.Error,
                "A RadioGroup component can only be nullable if rendered within a FormContext like in a Form " +
                "component. Consider passing nullable={false} or using the form components formData and onSubmit " +
                "prop instead."
            );
        }

        // uncontrolled state not supported due to the styling of the radio and passing of events
        if (defaultValue) {
            log(
                LogLevels.Error,
                "A RadioGroup component does not support the uncontrolled state. The prop \"defaultValue\" is " +
                "therefore forbidden. Consider using the form components formData and onSubmit prop instead."
            );
        }
    }, []);

    return (
        <Flex flexDirection={"column"}>
            <Flex
                flexDirection={"column"}
                position={"relative"}
                {...box}
            >
                {options.map((option: RadioOption, index: number) => (
                    <RadioButton
                        key={JSON.stringify(option)}
                        box={index < (options.length - 1) ? { mb: 3 } : undefined}
                        {...option}
                        id={`${formContextData.id}#${option.value}`}
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                            formContextData.onChange(event, option.value);
                            validationContextData.validate(option.value);
                        }}
                        onClick={(event: React.MouseEvent<HTMLInputElement>) => {
                            if (nullable && formContextData.value === option.value) {
                                formContextData.onChange(event, "");
                                validationContextData.validate("");
                            }
                        }}
                        onFocusOut={() => {
                            validationContextData.validate(formContextData.value);
                        }}
                        {...props}
                        checked={(formContextData.value || value) === option.value}
                        disabled={isDisabled}
                        loading={isLoading}
                        hasErrors={hasErrors}
                        hasWarnings={hasWarnings}
                    />
                ))}
                <ValidationContext.Provider value={validationContextData.context}>
                    {children}
                </ValidationContext.Provider>
            </Flex>
            <ValidationMessage {...validationContextData.validationResult} mt={2} />
        </Flex>
    );
};

RadioGroup.defaultProps = {
    value: undefined,
    defaultValue: undefined,
    nullable: true,
    box: undefined
};
