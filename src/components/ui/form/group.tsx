import styled, { StyledComponent } from "styled-components";
import React, { PropsWithChildren } from "react";
import { Box, BoxProps } from "../structure/box";
import { Theme } from "../../../types/ui";

type StyledGroupProps = BoxProps & {
    direction?: "row" | "column";
};

const StyledGroup: StyledComponent<"div", Theme, StyledGroupProps> = styled(Box)`
    ${(props: StyledGroupProps) => `
        flex-direction: ${props.direction};
        & > label {
            ${props.direction === "column" ? "margin-bottom: -8px" : "align-self: center"};
        }
    `}
`;

interface GroupProps extends StyledGroupProps {
    label?: string;
    width?: number;
    gap?: string;
}

export const Group: React.FC<GroupProps> = ({
    label,
    direction,
    width,
    children,
    ...props
}: PropsWithChildren<GroupProps>) => (
    <StyledGroup
        as={"fieldset"}
        display={"flex"}
        direction={direction}
        alignItems={direction === "row" && "center"}
        boxSizing={"border-box"}
        border={"none"}
        flexGrow={width}
        flexBasis={0}
        p={0}
        m={0}
        mb={4}
        styleProps={`
        gap: ${props.gap};
        
        > * {
            flex-basis: 0;
            flex-grow: 1;
            justify-content: flex-end;
            margin-bottom: 0;
        }
        `}
        {...props}
    >
        {children}
    </StyledGroup>
);

Group.defaultProps = {
    direction: "row",
    width: 1,
    gap: "16px"
};
