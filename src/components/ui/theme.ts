import { Theme } from "../../types/ui";

export const defaultTheme: Theme = {
    space: [0, 2, 4, 8, 16, 32, 64],
    fontSizes: [12, 14, 16, 20, 24, 32],
    lineHeights: [1, 1.25, 1.75, 2.5],
    colors: {
        body: "#403f41",
        headings: "#222",
        inverted: "#fff",
        background: "#fff",
        primary: "#388b3d",
        primaryDiscreet: "rgba(56,139,61,0.2)",
        secondary: "#a9ce84",
        secondaryDiscreet: "rgba(169,206,132,0.2)",
        error: "#d83434",
        warning: "#f0af39",
        border: "rgba(34,36,38,.25)",
        grey: {
            pale: "#f0f0f0",
            lighter: "#d8d8d8",
            light: "#bbb",
            medium: "#888",
            dark: "#403f41",
            darker: "#1d201a"
        },
        backdrop: { medium: "rgba(216,216,216,0.75)" },
        state: {
            info: {
                light: "#f8ffff",
                medium: "#a9d5de",
                dark: "#276f86"
            },
            success: {
                light: "#fcfff5",
                medium: "#a3c293",
                dark: "#2c662d"
            },
            warning: {
                light: "#fffaf3",
                medium: "#c9ba9b",
                dark: "#573a08"
            },
            error: {
                light: "#fff6f6",
                medium: "#e0b4b4",
                dark: "#9f3a38"
            }
        }
    },
    radii: { default: "4px" },
    shadows: {
        focus: "1px 1px 0 1px rgba(56,139,61,0.6)",
        disabled: "0 2px 25px 0 #d8d8d8 inset",
        sidebar: "0 0 10px 5px #bbb"
    },
    borders: { focus: "1px solid #388b3d" },
    fonts: {
        body: "system-ui, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Roboto, \"Helvetica Neue\", sans-serif",
        heading: "inherit",
        monospace: "Menlo, monospace"
    },
    breakpoints: ["481px", "769px"]
};
