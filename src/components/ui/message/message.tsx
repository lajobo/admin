/* eslint-disable object-curly-newline */
import React, { PropsWithChildren, useContext } from "react";
import styled, { StyledComponent, ThemeContext } from "styled-components";
import { Box, BoxProps } from "../structure/box";
import { Theme } from "../../../types/ui";
import { resolveColor } from "../../../utils/component";
import { Text, TextProps } from "../typography/text";
import { Heading } from "../typography/heading";
import { Icon, Icons, sizes as iconSizes, Sizes as IconSizes } from "../icon/icon";
import { Flex } from "../structure/flex";
import { BaseButtonProps } from "../button/base";

type StyledMessageProps = BoxProps & {
    theme?: Theme;
    highlightColor?: string;
};

const StyledMessage: StyledComponent<"div", Theme, StyledMessageProps> = styled(Box)`
    box-sizing: border-box;

    ${(messageProps: StyledMessageProps) => `
h1, h2, h3, h4, h5, strong { color: ${resolveColor(messageProps.theme.colors, messageProps.highlightColor)}; }
    `}
`;

type Types = "info" | "success" | "warning" | "error";

const types: (theme: Theme, type: Types) => StyledMessageProps = (theme: Theme, type: Types) => ({
    backgroundColor: `state.${type}.light`,
    boxShadow: `0 0 0 1px ${resolveColor(theme.colors, `state.${type}.light`)} inset, 0 0 0 0 transparent`,
    borderColor: `state.${type}.dark`,
    color: `state.${type}.dark`,
    highlightColor: `state.${type}.dark`
});

type Sizes = "xs" | "s" | "m" | "l" | "xl";

const sizes: { [size in Sizes]: StyledMessageProps } = {
    xs: {
        fontSize: 0,
        lineHeight: 1,
        px: 4,
        py: 2
    },
    s: {
        fontSize: 1,
        lineHeight: 2,
        px: 4,
        py: 2
    },
    m: {
        fontSize: 2,
        lineHeight: 2,
        px: 4,
        py: 2
    },
    l: {
        fontSize: 3,
        lineHeight: 2,
        px: 4,
        py: 2
    },
    xl: {
        fontSize: 4,
        lineHeight: 2,
        px: 4,
        py: 2
    }
};

type Compacts = "true" | "false";

const compacts: { [compact in Compacts]: BaseButtonProps } = {
    true: {
        display: "inline-flex"
    },
    false: {
        display: "flex;",
        width: "100%"
    }
};

export interface MessageProps extends StyledMessageProps {
    onDismiss?: () => void;
    type?: Types;
    size?: Sizes;
    compact?: boolean;
    heading?: string;
    content?: string;
    icon?: Icons;
    center?: boolean;
}

export const Message: React.FC<MessageProps> = ({
    onDismiss,
    type,
    size,
    compact,
    heading,
    content,
    icon,
    center,
    children,
    ...props
}: PropsWithChildren<MessageProps>) => {
    const theme: Theme = useContext(ThemeContext);
    const iconSizeKeys: string[] = Object.keys(iconSizes);
    const iconSize: IconSizes = (iconSizeKeys[iconSizeKeys.indexOf(size) + 2] || iconSizeKeys.pop()) as IconSizes;

    return (
        <StyledMessage
            border={"1px solid"}
            borderRadius={"default"}
            flexDirection={"row"}
            position={"relative"}
            justifyContent={center && "center"}
            {...types(theme, type)}
            {...sizes[size]}
            {...(onDismiss ? { pr: 5 } : {})}
            {...compacts[String(compact)]}
            {...props}
        >
            {children || (
                <>
                    {icon !== undefined && (
                        <Flex
                            justifyContent={"center"}
                            alignItems={"center"}
                            mr={sizes[size]?.px || 2}
                        >
                            <Icon size={iconSize} icon={icon} fill={types(theme, type).highlightColor} />
                        </Flex>
                    )}
                    <Flex flexDirection={"column"}>
                        {heading && (<Heading size={size}>{heading}</Heading>)}
                        {content && (<Text size={size} {...types(theme, type) as TextProps}>{content}</Text>)}
                    </Flex>
                </>
            )}
            {onDismiss && (
                <Icon
                    icon={Icons.Cross}
                    size={"xs"}
                    fill={types(theme, type).highlightColor}
                    position={"absolute"}
                    top={"8px"}
                    right={"8px"}
                    cursor={"pointer"}
                    onClick={() => {
                        onDismiss();
                    }}
                />
            )}
        </StyledMessage>
    );
};

Message.defaultProps = {
    onDismiss: undefined,
    type: "info",
    size: "m",
    compact: false,
    heading: undefined,
    content: undefined,
    center: false
};
