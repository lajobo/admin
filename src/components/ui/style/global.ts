import { createGlobalStyle, DefaultTheme, GlobalStyleComponent, ThemeProps } from "styled-components";
import { Theme } from "../../../types/ui";

export const GlobalStyle: GlobalStyleComponent<{}, DefaultTheme> = createGlobalStyle`
    ${(props: ThemeProps<Theme>) => `
html,
body,
#root {
    height: 100%;
}

html {
    overflow-y: scroll;
}

body {
    margin: 0;
    padding: 0;
    font-family: ${props.theme.fonts.body};
    color: ${props.theme.colors.grey.light};
}

a {
    text-decoration: none;
}

bold {
    font-weight: bold;
}

ul, ol, li {
    list-style: none;
    padding: 0;
    margin: 0;
}
    `}
`;
