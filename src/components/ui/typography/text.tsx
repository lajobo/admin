import styled, { StyledComponent } from "styled-components";
import {
    border,
    BorderProps,
    color as colorFn,
    ColorProps,
    display,
    DisplayProps,
    fontFamily,
    FontFamilyProps,
    fontSize,
    FontSizeProps,
    fontWeight,
    FontWeightProps,
    height,
    HeightProps,
    layout,
    LayoutProps,
    letterSpacing,
    LetterSpacingProps,
    lineHeight,
    LineHeightProps,
    maxWidth,
    MaxWidthProps,
    space,
    SpaceProps,
    textAlign,
    TextAlignProps,
    typography,
    TypographyProps,
    width,
    WidthProps
} from "styled-system";
import React, { PropsWithChildren } from "react";
import { cursor, CursorProps } from "../../../utils/interpolation/cursor";
import { Theme } from "../../../types/ui";
import { PolymorphProps } from "../../../utils/interpolation/polymorph";
import { HtmlForProps } from "../../../utils/interpolation/htmlFor";
import { textDecoration, TextDecorationProps } from "../../../utils/interpolation/textDecoration";

export type StyledTextProps =
    SpaceProps &
    WidthProps &
    LayoutProps &
    MaxWidthProps &
    FontSizeProps &
    ColorProps &
    TextAlignProps &
    LineHeightProps &
    FontWeightProps &
    LetterSpacingProps &
    FontFamilyProps &
    HeightProps &
    DisplayProps &
    BorderProps &
    TypographyProps &
    CursorProps &
    PolymorphProps &
    HtmlForProps &
    TextDecorationProps;

const StyledText: StyledComponent<"span", Theme, StyledTextProps> = styled.span<StyledTextProps>`
    ${space}
    ${layout}
    ${width}
    ${maxWidth}
    ${fontSize}
    ${colorFn}
    ${textAlign}
    ${lineHeight}
    ${fontWeight}
    ${letterSpacing}
    ${fontFamily}
    ${height}
    ${display}
    ${border}
    ${typography}
    ${cursor}
    ${textDecoration}
`;

type Sizes = "xs" | "s" | "m" | "l" | "xl";

const sizes: { [size in Sizes]: { fontSize: number, lineHeight: number } } = {
    xs: {
        fontSize: 0,
        lineHeight: 1
    },
    s: {
        fontSize: 1,
        lineHeight: 1
    },
    m: {
        fontSize: 2,
        lineHeight: 1
    },
    l: {
        fontSize: 3,
        lineHeight: 1
    },
    xl: {
        fontSize: 4,
        lineHeight: 1
    }
};

export interface TextProps extends StyledTextProps {
    size?: Sizes;
    content?: string;
    contentAsHTML?: boolean;
}

export const Text: React.FC<TextProps> = ({
    size,
    content,
    contentAsHTML,
    children,
    ...props
}: PropsWithChildren<TextProps>) => (
    <StyledText
        fontFamily={"body"}
        {...sizes[size]}
        {...props}
        dangerouslySetInnerHTML={content && contentAsHTML ? { __html: content } : undefined}
    >
        {children || (contentAsHTML ? undefined : content)}
    </StyledText>
);

Text.defaultProps = {
    color: "body",
    size: "m",
    content: undefined,
    contentAsHTML: false
};
