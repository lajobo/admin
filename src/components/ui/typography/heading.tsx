/* eslint-disable object-curly-newline */
import styled from "styled-components";
import React, { PropsWithChildren } from "react";
import {
    border,
    BorderProps,
    color,
    ColorProps,
    display,
    DisplayProps,
    fontFamily,
    FontFamilyProps,
    fontSize,
    FontSizeProps,
    fontWeight,
    FontWeightProps,
    height,
    HeightProps,
    layout,
    LayoutProps,
    letterSpacing,
    LetterSpacingProps,
    lineHeight,
    LineHeightProps,
    maxWidth,
    MaxWidthProps,
    space,
    SpaceProps,
    textAlign,
    TextAlignProps,
    width,
    WidthProps
} from "styled-system";
import { Text } from "./text";
import { cursor, CursorProps } from "../../../utils/interpolation/cursor";
import { PolymorphProps } from "../../../utils/interpolation/polymorph";

type StyledHeadingProps =
    SpaceProps &
    WidthProps &
    LayoutProps &
    MaxWidthProps &
    FontSizeProps &
    ColorProps &
    TextAlignProps &
    LineHeightProps &
    FontWeightProps &
    LetterSpacingProps &
    FontFamilyProps &
    HeightProps &
    DisplayProps &
    BorderProps &
    CursorProps &
    PolymorphProps;

const StyledHeading: React.FC<StyledHeadingProps> = styled(Text)`
    ${space}
    ${layout}
    ${width}
    ${maxWidth}
    ${fontSize}
    ${color}
    ${textAlign}
    ${lineHeight}
    ${fontWeight}
    ${letterSpacing}
    ${fontFamily}
    ${height}
    ${display}
    ${border}
    ${cursor}
    margin-block-start: 0;
    margin-block-end: 0;
    margin-inline-start: 0;
    margin-inline-end: 0;

    &:first-child {
        margin-top: 0;
    }
`;

type Sizes = "xs" | "s" | "m" | "l" | "xl";

const sizes: { [size in Sizes]: StyledHeadingProps } = {
    xs: {
        as: "h5",
        fontSize: 1,
        lineHeight: 2,
        mt: 1,
        mb: 0
    },
    s: {
        as: "h4",
        fontSize: 2,
        lineHeight: 2,
        mt: 2,
        mb: 1
    },
    m: {
        as: "h3",
        fontSize: 3,
        lineHeight: 2,
        mt: 3,
        mb: 2
    },
    l: {
        as: "h2",
        fontSize: 4,
        lineHeight: 2,
        mt: 4,
        mb: 3
    },
    xl: {
        as: "h1",
        fontSize: 5,
        lineHeight: 2,
        mt: 5,
        mb: 4
    }
};

type Compacts = "true" | "false";

const compacts: { [compact in Compacts]: StyledHeadingProps } = {
    true: {
        display: "inline-flex"
    },
    false: {
        display: "flex;",
        width: "100%"
    }
};

type Dividings = "true" | "false";

const dividings: { [dividing in Dividings]: StyledHeadingProps } = {
    true: {
        borderBottom: "1px solid",
        borderBottomColor: "grey.medium"
    },
    false: { }
};

interface HeadingProps extends StyledHeadingProps {
    size?: Sizes;
    compact?: boolean;
    dividing?: boolean;
    content?: string;
}

export const Heading: React.FC<HeadingProps> = ({
    size,
    compact,
    dividing,
    content,
    children,
    ...props
}: PropsWithChildren<HeadingProps>) => (
    <StyledHeading
        color={"headings"}
        fontWeight={"bold"}
        fontFamily={"heading"}
        {...sizes[size]}
        {...compacts[String(compact)]}
        {...dividings[String(dividing)]}
        {...props}
    >
        {children || content}
    </StyledHeading>
);

Heading.defaultProps = {
    content: undefined,
    size: "m",
    compact: false,
    dividing: false
};
