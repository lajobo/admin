/* eslint-disable object-curly-newline */
import React from "react";
import { SvgAdd } from "./lib/add";
import { GeneralIconProps } from "../../../types/component/icon";
import { SvgCircle } from "./lib/circle";
import { SvgMenu } from "./lib/menu";
import { SvgTrash } from "./lib/trash";
import { SvgUser } from "./lib/user";
import { SvgCross } from "./lib/cross";
import { SvgLock } from "./lib/lock";
import { SvgDot } from "./lib/dot";
import { SvgBrandLogo } from "./lib/brandLogo";
import { SvgBrandIcon } from "./lib/brandIcon";
import { SvgCheckboxFilled } from "./lib/checkboxFilled";
import { SvgCheckboxEmpty } from "./lib/checkboxEmpty";
import { SvgFolder } from "./lib/folder";
import { SvgTrashOpen } from "./lib/trashOpen";
import { SvgFolderOpen } from "./lib/folderOpen";
import { SvgFlagDe } from "./lib/flag/de";
import { SvgFlagGb } from "./lib/flag/gb";

export enum Icons {
    Add = "add",
    BrandIcon = "brandIcon",
    BrandLogo = "brandLogo",
    CheckboxEmpty = "checkboxEmpty",
    CheckboxFilled = "checkboxFilled",
    Circle = "circle",
    Cross = "cross",
    Dot = "dot",
    FlagDe = "flagDE",
    FlagGb = "flagGB",
    Folder = "folder",
    FolderOpen = "folderOpen",
    Lock = "lock",
    Menu = "menu",
    Trash = "trash",
    TrashOpen = "trashOpen",
    User = "user"
}

const IconMap: Map<Icons, React.FC<GeneralIconProps>> = new Map<Icons, React.FC<GeneralIconProps>>([
    [Icons.Add, SvgAdd],
    [Icons.BrandIcon, SvgBrandIcon],
    [Icons.BrandLogo, SvgBrandLogo],
    [Icons.CheckboxEmpty, SvgCheckboxEmpty],
    [Icons.CheckboxFilled, SvgCheckboxFilled],
    [Icons.Circle, SvgCircle],
    [Icons.Cross, SvgCross],
    [Icons.Dot, SvgDot],
    [Icons.FlagDe, SvgFlagDe],
    [Icons.FlagGb, SvgFlagGb],
    [Icons.Folder, SvgFolder],
    [Icons.FolderOpen, SvgFolderOpen],
    [Icons.Lock, SvgLock],
    [Icons.Menu, SvgMenu],
    [Icons.Trash, SvgTrash],
    [Icons.TrashOpen, SvgTrashOpen],
    [Icons.User, SvgUser]
]);

export type Sizes = "xs" | "s" | "m" | "l" | "xl";

export const sizes: { [size in Sizes]: GeneralIconProps } = {
    xs: {
        height: "12px"
    },
    s: {
        height: "16px"
    },
    m: {
        height: "20px"
    },
    l: {
        height: "24px"
    },
    xl: {
        height: "29px"
    }
};

export interface IconProps extends GeneralIconProps {
    icon: Icons;
    size?: Sizes;
    bordered?: boolean;
}

export const Icon: React.FC<IconProps> = ({
    icon,
    size,
    bordered,
    ...props
}: IconProps) => {
    const IconComponent: React.FC<GeneralIconProps> = IconMap.get(icon);
    return (
        <IconComponent
            {...sizes[size]}
            {...(bordered ? { border: "1px solid", bordercolor: "grey.dark" } : {})}
            {...props}
        />
    );
};

Icon.defaultProps = {
    size: "m"
};
