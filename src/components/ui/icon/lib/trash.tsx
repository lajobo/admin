/* eslint-disable max-len */
import * as React from "react";
import styled from "styled-components";
import { GeneralIconProps, styledIconDefinitions } from "../../../../types/component/icon";

export const SvgTrash: React.FC<GeneralIconProps> = styled((props: GeneralIconProps) => (
    <svg
        xmlns={"http://www.w3.org/2000/svg"}
        viewBox={"0 0 41.336 41.336"}
        {...props}
    >
        <path d={"M34.7,9.2h-7.3V5.5c0-0.7-0.6-1.3-1.3-1.3H15.3c-0.7,0-1.3,0.6-1.3,1.3v3.7H6.6c-1,0-1.8,0.8-1.8,1.8s0.8,1.8,1.8,1.8h1.8 v26.1c0,1.2,1,2.2,2.2,2.2h20c1.2,0,2.2-1,2.2-2.2V12.8h1.8c1,0,1.8-0.8,1.8-1.8S35.7,9.2,34.7,9.2z M14.8,36.1 c0,0.7-0.6,1.3-1.3,1.3s-1.3-0.6-1.3-1.3V17.3c0-0.7,0.6-1.3,1.3-1.3s1.3,0.6,1.3,1.3V36.1z M22,36.1c0,0.7-0.6,1.3-1.3,1.3 c-0.7,0-1.3-0.6-1.3-1.3V17.3c0-0.7,0.6-1.3,1.3-1.3c0.7,0,1.3,0.6,1.3,1.3V36.1z M24.7,9.2h-8.1V6.9h8.1V9.2z M29.2,36.1 c0,0.7-0.6,1.3-1.3,1.3c-0.7,0-1.3-0.6-1.3-1.3V17.3c0-0.7,0.6-1.3,1.3-1.3c0.7,0,1.3,0.6,1.3,1.3V36.1z"} />
    </svg>
))`${styledIconDefinitions}`;
