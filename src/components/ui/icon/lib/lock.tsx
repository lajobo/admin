/* eslint-disable max-len */
import * as React from "react";
import styled from "styled-components";
import { GeneralIconProps, styledIconDefinitions } from "../../../../types/component/icon";

export const SvgLock: React.FC<GeneralIconProps> = styled((props: GeneralIconProps) => (
    <svg
        xmlns={"http://www.w3.org/2000/svg"}
        viewBox={"0 0 470 470"}
        {...props}
    >
        <path
            id={"path4518"}
            d={"M 162.5,134.5 C 162.5,94.523 195.023,62 235,62 c 39.977,0 72.5,32.523 72.5,72.5 V 192 h 30 V 134.5 C 337.5,77.981 291.519,32 235,32 178.481,32 132.5,77.981 132.5,134.5 V 192 h 30 z"}
        />
        <rect x={"77.5"} y={"190"} width={"315"} height={"280"} />
    </svg>
))`${styledIconDefinitions}`;
