/* eslint-disable max-len */
import * as React from "react";
import styled from "styled-components";
import { GeneralIconProps, styledIconDefinitions } from "../../../../types/component/icon";

export const SvgFolderOpen: React.FC<GeneralIconProps> = styled((props: GeneralIconProps) => (
    <svg
        xmlns={"http://www.w3.org/2000/svg"}
        viewBox={"0 0 24 24"}
        {...props}
    >
        <path d={"M20.3,5.8H12L9.9,3.7H3.7c-1.1,0-2.1,1-2.1,2.1v12.4c0,1.1,1,2.1,2.1,2.1h16.6c1.1,0,2.1-1,2.1-2.1V7.9 C22.4,6.7,21.4,5.8,20.3,5.8z"} />
        <path style={{ fill: "#fff" }} d={"M21.5,7.3h-8.3l-2.1,0H4.9c-1.1,0-2.1,1-2.1,2.1l-1.6,8.3c0,1.1,1,2.1,2.1,2.1h16.6c1.1,0,2.1-1,2.1-2.1 l1.6-8.3C23.6,8.2,22.6,7.3,21.5,7.3z"} />
        <path d={"M21.9,7.9h-8.3l-2.1,0H5.3c-1.1,0-2.1,1-2.1,2.1l-1.6,8.3c0,1.1,1,2.1,2.1,2.1h16.6c1.1,0,2.1-1,2.1-2.1L24,9.9 C24,8.8,23,7.9,21.9,7.9z"} />
    </svg>
))`${styledIconDefinitions}`;
