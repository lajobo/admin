/* eslint-disable max-len */
import * as React from "react";
import styled from "styled-components";
import { GeneralIconProps, styledIconDefinitions } from "../../../../types/component/icon";

export const SvgDot: React.FC<GeneralIconProps> = styled((props: GeneralIconProps) => (
    <svg
        xmlns={"http://www.w3.org/2000/svg"}
        viewBox={"0 0 2 2"}
        {...props}
    >
        <circle cx={"1"} cy={"1"} r={"1"} />
    </svg>
))`${styledIconDefinitions}`;
