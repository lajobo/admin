/* eslint-disable max-len */
import * as React from "react";
import styled from "styled-components";
import { GeneralIconProps, styledIconDefinitions } from "../../../../types/component/icon";

export const SvgFolder: React.FC<GeneralIconProps> = styled((props: GeneralIconProps) => (
    <svg
        xmlns={"http://www.w3.org/2000/svg"}
        viewBox={"0 0 24 24"}
        {...props}
    >
        <path d={"M20.3,5.8H12L9.9,3.7H3.7c-1.1,0-2.1,1-2.1,2.1v12.4c0,1.1,1,2.1,2.1,2.1h16.6c1.1,0,2.1-1,2.1-2.1V7.9 C22.4,6.7,21.4,5.8,20.3,5.8z"} />
    </svg>
))`${styledIconDefinitions}`;
