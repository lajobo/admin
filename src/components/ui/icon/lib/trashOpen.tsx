/* eslint-disable max-len */
import * as React from "react";
import styled from "styled-components";
import { GeneralIconProps, styledIconDefinitions } from "../../../../types/component/icon";

export const SvgTrashOpen: React.FC<GeneralIconProps> = styled((props: GeneralIconProps) => (
    <svg
        xmlns={"http://www.w3.org/2000/svg"}
        viewBox={"0 0 41.336 41.336"}
        {...props}
    >
        <path d={"M8.4,12.8v26.1c0,1.2,1,2.2,2.2,2.2h20c1.2,0,2.2-1,2.2-2.2V12.8h1.8 M14.8,36.1c0,0.7-0.6,1.3-1.3,1.3s-1.3-0.6-1.3-1.3 V17.3c0-0.7,0.6-1.3,1.3-1.3s1.3,0.6,1.3,1.3V36.1z M22,36.1c0,0.7-0.6,1.3-1.3,1.3c-0.7,0-1.3-0.6-1.3-1.3V17.3 c0-0.7,0.6-1.3,1.3-1.3c0.7,0,1.3,0.6,1.3,1.3V36.1z M29.2,36.1c0,0.7-0.6,1.3-1.3,1.3c-0.7,0-1.3-0.6-1.3-1.3V17.3 c0-0.7,0.6-1.3,1.3-1.3c0.7,0,1.3,0.6,1.3,1.3V36.1z"} />
        <path d={"M31.4,13.7l1.7,0.5c1,0.3,1.9-0.3,2.2-1.3c0.3-1-0.3-1.9-1.3-2.2L27,8.9l1-3.6c0.2-0.7-0.2-1.5-0.9-1.6L16.6,0.8 c-0.7-0.2-1.5,0.2-1.6,0.9l-1,3.6L6.9,3.5C6,3.2,5,3.8,4.7,4.8S5,6.7,6,6.9l1.7,0.5 M24.4,8.2l-7.8-2.1l0.6-2.3L25,5.9L24.4,8.2z"} />
    </svg>
))`${styledIconDefinitions}`;
