/* eslint-disable max-len */
import * as React from "react";
import styled from "styled-components";
import { GeneralIconProps, styledIconDefinitions } from "../../../../types/component/icon";

export const SvgCheckboxEmpty: React.FC<GeneralIconProps> = styled((props: GeneralIconProps) => (
    <svg
        xmlns={"http://www.w3.org/2000/svg"}
        viewBox={"0 0 28.35 28.35"}
        {...props}
    >
        <path
            style={{ fill: "none", strokeWidth: 1 }}
            d={"M22.65,27.28H5.7c-2.56,0-4.64-2.08-4.64-4.64V5.7c0-2.56,2.08-4.64,4.64-4.64h16.95 c2.56,0,4.64,2.08,4.64,4.64v16.95C27.28,25.21,25.21,27.28,22.65,27.28z"}
        />
    </svg>
))`${styledIconDefinitions}`;

SvgCheckboxEmpty.defaultProps = { stroke: "#000" };
