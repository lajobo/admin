/* eslint-disable max-len */
import * as React from "react";
import styled from "styled-components";
import { GeneralIconProps, styledIconDefinitions } from "../../../../types/component/icon";

export const SvgCheckboxFilled: React.FC<GeneralIconProps> = styled((props: GeneralIconProps) => (
    <svg
        xmlns={"http://www.w3.org/2000/svg"}
        viewBox={"0 0 28.35 28.35"}
        {...props}
    >
        <path
            style={{ fill: "none", strokeWidth: 1 }}
            d={"M22.65,27.28H5.7c-2.56,0-4.64-2.08-4.64-4.64V5.7c0-2.56,2.08-4.64,4.64-4.64h16.95 c2.56,0,4.64,2.08,4.64,4.64v16.95C27.28,25.21,25.21,27.28,22.65,27.28z"}
        />
        <line style={{ strokeWidth: 2 }} x1={"22.52"} y1={"5.5"} x2={"11.7"} y2={"22.85"} />
        <line style={{ strokeWidth: 2 }} x1={"5.83"} y1={"14.97"} x2={"11.7"} y2={"22.85"} />
    </svg>
))`${styledIconDefinitions}`;

SvgCheckboxFilled.defaultProps = { stroke: "#000" };
