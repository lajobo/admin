import React, { PropsWithChildren } from "react";
import { Transition, TransitionStatus } from "react-transition-group";
import { ENTERED, ENTERING, EXITED } from "react-transition-group/Transition";
import { useSelector } from "react-redux";
import { Flex, FlexProps } from "../structure/flex";
import { resolveObjectKey } from "../../../utils/object";
import { Box } from "../structure/box";
import { resolveBreakpoint } from "../../../utils/component";
import { DeviceSize } from "../../../types/device";
import { selectDeviceSize } from "../../../store/selectors/device";

type Directions = "top" | "right" | "bottom" | "left";
type Sizes = "s" | "m" | "l" | string | number;
type VariableSides = "height" | "width";

const sizes: { [size in Sizes]: string | string[] } = {
    s: ["150px"],
    m: ["80%", "250px"],
    l: ["90%", "80%"]
};

const variableSides: { [direction in Directions]: VariableSides } = {
    top: "height",
    right: "width",
    bottom: "height",
    left: "width"
};

const getPositionStyle: (
    direction: Directions,
    size: Sizes,
    state: TransitionStatus,
    fixed: boolean
) => FlexProps = (
    direction: Directions,
    size: Sizes,
    state: TransitionStatus,
    fixed: boolean
) => {
    const pxSize: string | string[] = sizes[size];

    return {
        ...(fixed
            ? { position: "fixed" }
            : {
                position: "fixed",
                ...(direction === "bottom" ? { bottom: 0 } : { top: 0 }),
                ...(direction === "right" ? { right: 0 } : { left: 0 })
            }),
        ...{
            width: "100%",
            height: "100%",
            [variableSides[direction]]: pxSize
        }
    };
};

interface StyledSidebarProps extends FlexProps {
    backgroundColor: string | string[];
    duration: number;
    fixed: boolean;
    direction: Directions;
    size: Sizes;
    variableSide: VariableSides;
    transitionStatus: TransitionStatus;
    zIndex: number;
}

export const StyledSidebar: React.FC<StyledSidebarProps> = ({
    backgroundColor,
    duration,
    fixed,
    direction,
    size,
    variableSide,
    transitionStatus,
    zIndex,
    children,
    ...props
}: PropsWithChildren<StyledSidebarProps>) => (
    <Flex
        zIndex={zIndex}
        backgroundColor={backgroundColor}
        transition={`transform ${duration}ms ease-in-out`}
        {...getPositionStyle(direction, size, transitionStatus, fixed)}
        {...props}
    >
        {children}
    </Flex>
);

interface SidebarProps extends FlexProps {
    visible?: boolean;
    fixed?: boolean;
    direction: Directions,
    size?: Sizes;
    backgroundColor?: string | string[];
    zIndex?: number;
    addDimmer?: boolean;
}

export const Sidebar: React.FC<SidebarProps> = ({
    visible,
    fixed,
    direction,
    size,
    backgroundColor,
    zIndex,
    addDimmer,
    children,
    ...props
}: PropsWithChildren<SidebarProps>) => {
    const deviceSize: DeviceSize = useSelector(selectDeviceSize);

    const pxSize: string | number = resolveBreakpoint(resolveObjectKey(sizes, String(size), size), deviceSize);
    const variableSide: VariableSides = variableSides[direction];
    const duration: number = 750;

    const sidebarProps: StyledSidebarProps = {
        backgroundColor,
        variableSide,
        duration,
        fixed,
        size,
        direction,
        transitionStatus: ENTERED,
        zIndex,
        [variableSide]: pxSize,
        ...props
    };

    const transformDirection: string = ["top", "bottom"].includes(direction) ? "Y" : "X";
    const transformSign: string = ["top", "left"].includes(direction) ? "-" : "";

    return fixed
        ? (
            <StyledSidebar {...sidebarProps}>
                {children}
            </StyledSidebar>
        ) : (
            <Transition
                in={visible}
                timeout={duration}
            >
                {(transitionStatus: TransitionStatus) => (
                    <>
                        <StyledSidebar
                            overflow={"hidden"}
                            {...sidebarProps}
                            transitionStatus={transitionStatus}
                            transform={
                                `translate${transformDirection}(${transformSign}${[ENTERING, ENTERED].includes(transitionStatus) ? 0 : "100%"})`
                            }
                            {...(transitionStatus === EXITED ? { width: 0, height: 0 } : {})}
                        >
                            {children}
                        </StyledSidebar>
                        {addDimmer && (
                            <Transition
                                in={[ENTERING, ENTERED].includes(transitionStatus)}
                                timeout={duration}
                            >
                                {(dimmerTransitionStatus: TransitionStatus) => (
                                    <Box
                                        position={"fixed"}
                                        left={0}
                                        top={0}
                                        width={"100%"}
                                        height={"100%"}
                                        zIndex={1}
                                        backgroundColor={"grey.lighter"}
                                        opacity={[ENTERING, ENTERED].includes(dimmerTransitionStatus) ? "75%" : 0}
                                        pointerEvents={[ENTERED].includes(dimmerTransitionStatus) ? "auto" : "none"}
                                        transition={`opacity ${duration}ms ease-in-out`}
                                    />
                                )}
                            </Transition>
                        )}
                    </>
                )}
            </Transition>
        );
};

Sidebar.defaultProps = {
    visible: false,
    fixed: false,
    size: "m",
    backgroundColor: "white",
    zIndex: 2,
    addDimmer: false
};
