/* eslint-disable object-curly-newline */
import styled, { StyledComponent, ThemeContext } from "styled-components";
import React, { useContext } from "react";
import {
    background,
    BackgroundProps,
    display,
    DisplayProps,
    flexbox,
    FlexboxProps,
    height,
    HeightProps,
    layout,
    LayoutProps,
    maxWidth,
    MaxWidthProps,
    position,
    PositionProps,
    space,
    SpaceProps,
    width,
    WidthProps
} from "styled-system";
import { Theme } from "../../../types/ui";
import { ThemeProps } from "../../../utils/interpolation/theme";
import { BoxSizingProps } from "../../../utils/interpolation/boxSizing";
import { CursorProps } from "../../../utils/interpolation/cursor";
import { stroke, StrokeProps } from "../../../utils/interpolation/stroke";
import { Flex, FlexProps } from "../structure/flex";
import { fill, FillProps } from "../../../utils/interpolation/fill";
import { resolveColor } from "../../../utils/component";
import { BaseButtonProps } from "../button/base";

type StyledSpinnerProps =
    React.HTMLAttributes<HTMLOrSVGElement> &
    SpaceProps &
    WidthProps &
    LayoutProps &
    MaxWidthProps &
    HeightProps &
    DisplayProps &
    PositionProps &
    BackgroundProps &
    FlexboxProps &
    ThemeProps &
    BoxSizingProps &
    CursorProps &
    StrokeProps &
    FillProps;

const StyledSpinner: StyledComponent<"svg", Theme, StyledSpinnerProps> = styled.svg`
    animation: rotate 1s linear infinite;
    
    & circle {
        fill: none;
        stroke-linecap: round;
        animation: dash 1.5s ease-in-out infinite;
    }
    
    @keyframes rotate {
        100% {
            transform: rotate(360deg);
        }
    }
    
    @keyframes dash {
        0% {
            stroke-dasharray: 1, 150;
            stroke-dashoffset: 0;
        }
        50% {
            stroke-dasharray: 90, 150;
            stroke-dashoffset: -35;
        }
        100% {
            stroke-dasharray: 90, 150;
            stroke-dashoffset: -124;
        }
    }
    
    ${space}
    ${layout}
    ${width}
    ${maxWidth}
    ${height}
    ${display}
    ${position}
    ${background}
    ${flexbox}
    ${stroke}
    ${fill}
`;

type Sizes = "xs" | "s" | "m" | "l" | "xl";

const sizes: { [size in Sizes]: BaseButtonProps } = {
    xs: {
        width: "20px"
    },
    s: {
        width: "30px"
    },
    m: {
        width: "40px"
    },
    l: {
        width: "50px"
    },
    xl: {
        width: "60px"
    }
};

interface SpinnerProps extends FlexProps {
    size?: Sizes;
    discreet?: boolean;
    color?: string;
    center?: boolean;
}

export const Spinner: React.FC<SpinnerProps> = ({
    size,
    discreet,
    color,
    center,
    ...props
}: SpinnerProps) => {
    const theme: Theme = useContext(ThemeContext);

    return (
        <Flex
            className={"spinner-flex"}
            justifyContent={center ? "center" : undefined}
            {...props}
        >
            <StyledSpinner
                viewBox={"0 0 50 50"}
                {...sizes[size]}
                fill={"none"}
            >
                <circle
                    cx={"25"}
                    cy={"25"}
                    r={"20"}
                    stroke={color
                        ? resolveColor(theme.colors, color)
                        : (discreet ? theme.colors.primaryDiscreet : theme.colors.primary)}
                    strokeWidth={"3"}
                />
            </StyledSpinner>
        </Flex>
    );
};

Spinner.defaultProps = {
    size: "m",
    discreet: false,
    center: false
};
