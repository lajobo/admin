import styled, { StyledComponent } from "styled-components";
import React from "react";
import { Theme } from "../../../types/ui";
import { Box, BoxProps } from "../structure/box";
import { resolveObjectKey } from "../../../utils/object";
import { resolveColor } from "../../../utils/component";

export type BaseButtonProps =
    React.ButtonHTMLAttributes<HTMLButtonElement> &
    BoxProps &
    { focusOutlineColor?: string };

export const BaseButton: StyledComponent<"button", Theme, BaseButtonProps> = styled(Box)`
    ${(buttonProps: BaseButtonProps) => (buttonProps.color
        ? `* { stroke: ${resolveObjectKey(buttonProps.theme.colors, buttonProps.color) || buttonProps.color} }`
        : undefined)}
    ${(buttonProps: BaseButtonProps) => (buttonProps.color
        ? `* { fill: ${resolveObjectKey(buttonProps.theme.colors, buttonProps.color) || buttonProps.color} }`
        : undefined)}
    ${(buttonProps: BaseButtonProps) => (buttonProps.focusOutlineColor
        ? `&:focus { outline: 3px solid ${resolveColor(buttonProps.theme.colors, buttonProps.focusOutlineColor)}; }`
        : undefined)}
`;
