/* eslint-disable object-curly-newline */
import React from "react";
import { Icon, Icons } from "../icon/icon";
import { BaseButton, BaseButtonProps } from "./base";

const baseStyle: BaseButtonProps = {
    alignItems: "center",
    justifyContent: "center",
    my: 1,
    mx: 1,
    borderRadius: "default",
    border: "1px solid transparent"
};

export type Colors = "primary" | "secondary" | "grey" | "white" | "dark" | "danger";
export type Appearances = "contained" | "outlined" | "text";

const colorsAndAppearances: { [color in Colors]: { [appearance in Appearances]: BaseButtonProps } } = {
    primary: {
        contained: {
            bg: "primary",
            color: "inverted",
            focusOutlineColor: "grey.dark"
        },
        outlined: {
            bg: "background",
            color: "primary",
            borderColor: "primary",
            focusOutlineColor: "primary"
        },
        text: {
            color: "primary",
            bg: "transparent",
            focusOutlineColor: "primary"
        }
    },
    secondary: {
        contained: {
            bg: "secondary",
            color: "body",
            focusOutlineColor: "body"
        },
        outlined: {
            bg: "background",
            color: "secondary",
            borderColor: "secondary",
            focusOutlineColor: "secondary"
        },
        text: {
            color: "secondary",
            bg: "transparent",
            focusOutlineColor: "secondary"
        }
    },
    grey: {
        contained: {
            bg: "grey.medium",
            color: "background",
            focusOutlineColor: "grey.dark"
        },
        outlined: {
            bg: "background",
            color: "grey.medium",
            borderColor: "grey.medium",
            focusOutlineColor: "grey.dark"
        },
        text: {
            color: "grey.medium",
            bg: "transparent",
            focusOutlineColor: "grey.medium"
        }
    },
    white: {
        contained: {
            bg: "white",
            color: "body",
            focusOutlineColor: "body"
        },
        outlined: {
            bg: "background",
            color: "white",
            borderColor: "white",
            focusOutlineColor: "white"
        },
        text: {
            color: "white",
            bg: "transparent",
            focusOutlineColor: "white"
        }
    },
    dark: {
        contained: {
            bg: "grey.dark",
            color: "white",
            focusOutlineColor: "body"
        },
        outlined: {
            bg: "background",
            color: "grey.dark",
            borderColor: "grey.dark",
            focusOutlineColor: "grey.dark"
        },
        text: {
            color: "grey.dark",
            bg: "transparent",
            focusOutlineColor: "transparent"
        }
    },
    danger: {
        contained: {
            bg: "state.error.dark",
            color: "state.error.light",
            focusOutlineColor: "body"
        },
        outlined: {
            bg: "background",
            color: "state.error.dark",
            borderColor: "state.error.dark",
            focusOutlineColor: "body"
        },
        text: {
            color: "state.error.dark",
            bg: "transparent",
            focusOutlineColor: "transparent"
        }
    }
};

type Sizes = "s" | "m" | "l" | "xl";

const sizes: { [size in Sizes]: BaseButtonProps } = {
    s: {
        fontSize: 1,
        lineHeight: 1,
        px: 3,
        py: 2
    },
    m: {
        fontSize: 2,
        lineHeight: 1,
        px: 4,
        py: 3
    },
    l: {
        fontSize: 3,
        lineHeight: 1,
        px: 5,
        py: 4
    },
    xl: {
        fontSize: 4,
        lineHeight: 1,
        px: 6,
        py: 5
    }
};

type Compacts = "true" | "false";

const compacts: { [compact in Compacts]: BaseButtonProps } = {
    true: {
        display: "inline-flex"
    },
    false: {
        display: "flex;",
        width: "100%",
        maxWidth: [undefined, "250px"],
        mx: "auto"
    }
};

type Fullwidths = "true" | "false";

const fullwidths: { [fullwidth in Fullwidths]: BaseButtonProps } = {
    true: {
        maxWidth: "100%"
    },
    false: {}
};

export interface ButtonProps extends BaseButtonProps {
    color?: Colors;
    appearance?: Appearances;
    size?: Sizes;
    compact?: boolean;
    fullwidth?: boolean;
    icon?: Icons;
}

export const Button: React.FC<ButtonProps> = ({
    color,
    appearance,
    size,
    compact,
    fullwidth,
    icon,
    children,
    ...props
}: ButtonProps) => (
    <BaseButton
        as={"button"}
        position={"relative"}
        {...baseStyle}
        {...colorsAndAppearances[color][appearance]}
        {...sizes[size]}
        {...compacts[String(compact)]}
        {...fullwidths[String(fullwidth)]}
        {...(props.disabled
            ? {
                backgroundColor: "white",
                boxShadow: "disabled",
                color: "grey.light",
                borderColor: "grey.light"
            }
            : {}
        )}
        cursor={props.disabled ? undefined : "pointer"}
        {...props}
    >
        {children}
        {!children && icon && (
            <Icon icon={icon} size={size} />
        )}
    </BaseButton>
);

Button.defaultProps = {
    color: "primary",
    appearance: "contained",
    size: "m",
    compact: false,
    fullwidth: false,
    icon: undefined
};
