import React, { ForwardRefExoticComponent, RefAttributes } from "react";
import { Link as ReactRouterLink, LinkProps as ReactRouterLinkProps } from "react-router-dom";
import styled, { StyledComponent } from "styled-components";
import {
    border,
    BorderProps,
    color,
    ColorProps,
    display,
    DisplayProps,
    fontFamily,
    FontFamilyProps,
    fontSize,
    FontSizeProps,
    fontWeight,
    FontWeightProps,
    height,
    HeightProps,
    layout,
    LayoutProps,
    letterSpacing,
    LetterSpacingProps,
    lineHeight,
    LineHeightProps,
    maxWidth,
    MaxWidthProps,
    space,
    SpaceProps,
    textAlign,
    TextAlignProps,
    width,
    WidthProps
} from "styled-system";
import { cursor, CursorProps } from "../../../utils/interpolation/cursor";
import { textDecoration, TextDecorationProps } from "../../../utils/interpolation/textDecoration";

export type LinkProps =
    ReactRouterLinkProps &
    React.RefAttributes<HTMLAnchorElement> &
    SpaceProps &
    WidthProps &
    LayoutProps &
    MaxWidthProps &
    FontSizeProps &
    ColorProps &
    TextAlignProps &
    LineHeightProps &
    FontWeightProps &
    LetterSpacingProps &
    FontFamilyProps &
    HeightProps &
    DisplayProps &
    BorderProps &
    CursorProps &
    TextDecorationProps;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const Link: StyledComponent<ForwardRefExoticComponent<LinkProps & RefAttributes<HTMLAnchorElement>>, any, any> =
    styled(ReactRouterLink)<LinkProps>`
    ${space}
    ${layout}
    ${width}
    ${maxWidth}
    ${fontSize}
    ${color}
    ${textAlign}
    ${lineHeight}
    ${fontWeight}
    ${letterSpacing}
    ${fontFamily}
    ${height}
    ${display}
    ${border}
    ${cursor}
    ${textDecoration}
`;
