import React from "react";
import styled, { StyledComponent } from "styled-components";
import { Box, BoxProps } from "../structure/box";
import { Theme } from "../../../types/ui";

type StyledImageProps = React.HTMLAttributes<HTMLImageElement> & BoxProps;

const StyledImage: StyledComponent<"img", Theme, StyledImageProps> = styled(Box)``;

export interface ImageProps extends StyledImageProps {
    center?: boolean;
}

export const Image: React.FC<ImageProps> = (props: ImageProps) => (
    <StyledImage
        as={"img"}
        mx={props.center && "auto"}
        {...props}
    />
);

Image.defaultProps = { center: false };
