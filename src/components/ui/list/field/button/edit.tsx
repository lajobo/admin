import React, { useContext } from "react";
import { Icons } from "../../../icon/icon";
import { ListButtonField } from "../button";
import { DataListRowContextStructure } from "../../../../../types/contexts/list";
import { ListRowContext } from "../../../../../contexts/listRow";

export const ListEditButtonField: React.FC = () => {
    const listRowContext: DataListRowContextStructure = useContext(ListRowContext) as DataListRowContextStructure;

    return (
        <ListButtonField
            icon={Icons.Folder}
            iconHover={Icons.FolderOpen}
            link={String(listRowContext.data.id)}
        />
    );
};
