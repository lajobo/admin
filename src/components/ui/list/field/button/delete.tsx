import React, { useContext, useState } from "react";
import { AnyAction, AsyncThunk, ThunkDispatch } from "@reduxjs/toolkit";
import { useDispatch, useSelector } from "react-redux";
import { Icons } from "../../../icon/icon";
import { ListButtonField } from "../button";
import { DataListRowContextStructure } from "../../../../../types/contexts/list";
import { ListRowContext } from "../../../../../contexts/listRow";
import { Text } from "../../../typography/text";
import { Modal } from "../../../modal/modal";
import { ComponentUiListFieldButtonDeleteI18n } from "../../../../../types/i18n";
import { selectI18n } from "../../../../../store/selectors/i18n";
import { replaceVariables } from "../../../../../utils/i18n";
import { FormDataState } from "../../../../../types/reducers/formData";
import { State } from "../../../../../types/store";

interface ListDeleteButtonFieldProps {
    modalI18n?: Partial<ComponentUiListFieldButtonDeleteI18n>;
    deleteFunction: AsyncThunk<any, any, {}>; // eslint-disable-line @typescript-eslint/no-explicit-any
    deleteFunctionPropModifier: (data: FormDataState) => object;
}

export const ListDeleteButtonField: React.FC<ListDeleteButtonFieldProps> = ({
    modalI18n,
    deleteFunction,
    deleteFunctionPropModifier
}: ListDeleteButtonFieldProps) => {
    const listRowContext: DataListRowContextStructure = useContext(ListRowContext) as DataListRowContextStructure;
    const dispatch: ThunkDispatch<State, {}, AnyAction> = useDispatch();
    const [showModal, setShowModal] = useState<boolean>(false);
    const i18n: ComponentUiListFieldButtonDeleteI18n = {
        ...useSelector(selectI18n).components.ui.list.field.button.delete,
        ...modalI18n
    };

    const onClick: () => void = () => {
        setShowModal(true);
    };

    return (
        <>
            <ListButtonField
                icon={Icons.Trash}
                iconHover={Icons.TrashOpen}
                onClick={onClick}
            />
            {showModal && (
                <Modal
                    title={replaceVariables(i18n.title, listRowContext.data)}
                    actions={[
                        {
                            text: i18n.deleteButton,
                            color: "danger",
                            onClick: () => {
                                dispatch(deleteFunction(deleteFunctionPropModifier(listRowContext.data)));
                                setShowModal(false);
                            }
                        },
                        {
                            text: i18n.cancelButton,
                            color: "grey",
                            onClick: () => {
                                setShowModal(false);
                            }
                        }
                    ]}
                >
                    <Text content={replaceVariables(i18n.message, listRowContext.data)} contentAsHTML />
                </Modal>
            )}
        </>
    );
};
