import React, { useState } from "react";
import { Icon, Icons } from "../../icon/icon";
import { Link } from "../../link/link";

interface ListButtonFieldProps {
    icon: Icons;
    iconHover?: Icons;
    onClick?: () => void;
    link?: string;
}

export const ListButtonField: React.FC<ListButtonFieldProps> = ({
    icon,
    iconHover,
    onClick,
    link
}: ListButtonFieldProps) => {
    const [isHovered, setIsHovered] = useState<boolean>(false);

    const iconElement: React.ReactElement = (
        <Icon
            icon={isHovered ? iconHover || icon : icon}
            fill={"grey.medium"}
            onClick={onClick}
            onMouseEnter={() => { setIsHovered(true); }}
            onMouseLeave={() => { setIsHovered(false); }}
            width={"24px"}
            height={"24px"}
            cursor={"pointer"}
            p={2}
        />
    );

    return !link ? iconElement : (
        <Link to={link}>
            {iconElement}
        </Link>
    );
};
