import React, { useContext } from "react";
import { DataListRowContextStructure } from "../../../../types/contexts/list";
import { ListRowContext } from "../../../../contexts/listRow";
import { FormDataValue } from "../../../../types/reducers/formData";
import { SvgCheckboxFilled } from "../../icon/lib/checkboxFilled";
import { SvgCheckboxEmpty } from "../../icon/lib/checkboxEmpty";
import { GeneralIconProps } from "../../../../types/component/icon";

interface ListDateFieldProps {
    name: string;
    convertToBoolean?: (value: FormDataValue) => boolean;
    alternativeRender?: (value: boolean) => React.ReactElement;
}

export const ListCheckboxField: React.FC<ListDateFieldProps> = ({
    name,
    convertToBoolean,
    alternativeRender
}: ListDateFieldProps) => {
    const listRowContext: DataListRowContextStructure = useContext(ListRowContext) as DataListRowContextStructure;
    const value: boolean = convertToBoolean
        ? convertToBoolean(listRowContext.data[name])
        : Boolean(listRowContext.data[name]);
    const Icon: React.FC<GeneralIconProps> = value ? SvgCheckboxFilled : SvgCheckboxEmpty;

    return alternativeRender
        ? alternativeRender(value)
        : (<Icon width={"16px"} height={"16px"} stroke={"body"} />);
};

ListCheckboxField.defaultProps = {
    convertToBoolean: undefined,
    alternativeRender: undefined
};
