import React, { PropsWithChildren } from "react";
import { Text } from "../../typography/text";

interface ListConcatFieldProps {
    separator?: string;
}

export const ListConcatField: React.FC<ListConcatFieldProps> = ({
    separator,
    children
}: PropsWithChildren<ListConcatFieldProps>) => (
    <>
        {React.Children.map(children, (child: React.ReactElement, index: number) => (
            <>
                {child}
                {separator && index < (React.Children.count(children) - 1) && (
                    <Text content={separator} contentAsHTML />
                )}
            </>
        ))}
    </>
);
