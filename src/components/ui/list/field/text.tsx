import React, { useContext } from "react";
import { TypographyProps } from "styled-system";
import { DataListRowContextStructure } from "../../../../types/contexts/list";
import { ListRowContext } from "../../../../contexts/listRow";
import { Text } from "../../typography/text";

interface ListTextFieldProps extends TypographyProps {
    name: string;
}

export const ListTextField: React.FC<ListTextFieldProps> = ({
    name,
    ...props
}: ListTextFieldProps) => {
    const listRowContext: DataListRowContextStructure = useContext(ListRowContext) as DataListRowContextStructure;

    return (
        <Text content={String(listRowContext.data[name])} {...props} />
    );
};
