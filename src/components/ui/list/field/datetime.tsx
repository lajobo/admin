import React, { useContext } from "react";
import { useSelector } from "react-redux";
import { format, parseISO } from "date-fns";
import { DataListRowContextStructure } from "../../../../types/contexts/list";
import { ListRowContext } from "../../../../contexts/listRow";
import { Text } from "../../typography/text";
import { selectI18n } from "../../../../store/selectors/i18n";

interface ListDateTimeFieldProps {
    name: string;
}

export const ListDateTimeField: React.FC<ListDateTimeFieldProps> = ({ name }: ListDateTimeFieldProps) => {
    const listRowContext: DataListRowContextStructure = useContext(ListRowContext) as DataListRowContextStructure;
    const { dateTimeFormat } = useSelector(selectI18n).common.date;

    return (
        <Text content={format(parseISO(String(listRowContext.data[name])), dateTimeFormat)} />
    );
};
