import React, { PropsWithChildren, useContext } from "react";
import { HeaderListRowContextStructure, ListRowContextStructure, ListRowTypes } from "../../../types/contexts/list";
import { ListRowContext } from "../../../contexts/listRow";
import { TableHeader } from "../table/th";
import { TableData } from "../table/td";
import { Text } from "../typography/text";
import { BoxProps } from "../structure/box";

interface ListColumnProps {
    name: string;
    shrinkToContent?: boolean;
    compact?: boolean;
    center?: boolean;
}

export const ListColumn: React.FC<ListColumnProps> = ({
    name,
    shrinkToContent,
    compact,
    center,
    children
}: PropsWithChildren<ListColumnProps>) => {
    const listRowContext: ListRowContextStructure = useContext(ListRowContext);
    const isHeader: boolean = listRowContext?.type === ListRowTypes.Header;
    const TableCell: React.FC<BoxProps> = isHeader ? TableHeader : TableData;

    return (
        <TableCell
            p={compact ? 3 : 4}
            backgroundColor={isHeader ? "grey.lighter" : undefined}
            borderBottom={"1px solid"}
            borderBottomColor={"border"}
            borderRight={"1px solid"}
            borderRightColor={"border"}
            width={shrinkToContent ? "0" : undefined}
            textAlign={center ? "center" : undefined}
            minWidth={"32px"}
        >
            {isHeader && (<Text content={(listRowContext as HeaderListRowContextStructure).i18n[name]} />)}
            {!isHeader && children}
        </TableCell>
    );
};

ListColumn.defaultProps = {
    shrinkToContent: false,
    compact: false,
    center: false
};
