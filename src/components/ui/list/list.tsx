import React, { PropsWithChildren, useContext } from "react";
import { ThemeContext } from "styled-components";
import { useSelector } from "react-redux";
import { Table } from "../table/table";
import { TableRow } from "../table/tr";
import { FormDataState } from "../../../types/reducers/formData";
import { ListI18n, ListRowTypes } from "../../../types/contexts/list";
import { ListRowContext } from "../../../contexts/listRow";
import { Tablehead } from "../table/thead";
import { TableBody } from "../table/tbody";
import { DataFetchingState } from "../../../types/state";
import { TableData } from "../table/td";
import { Spinner } from "../loader/spinner";
import { Text } from "../typography/text";
import { ComponentUiListI18n } from "../../../types/i18n";
import { selectI18n } from "../../../store/selectors/i18n";
import { ListController } from "./controller";

interface TableProps {
    i18n: ListI18n;
    dataFetchingState: DataFetchingState;
    data: FormDataState[];
    hideHeader?: boolean;
}

export const List: React.FC<TableProps> = ({
    i18n,
    dataFetchingState,
    data,
    hideHeader,
    children
}: PropsWithChildren<TableProps>) => {
    const { colors } = useContext(ThemeContext);
    const staticI18n: ComponentUiListI18n = useSelector(selectI18n).components.ui.list.list;

    return (
        <>
            <ListController />
            <Table
                border={"1px solid"}
                borderColor={"border"}
                borderSpacing={"0"}
                borderRadius={"default"}
            >
                {!hideHeader && (
                    <Tablehead
                        styleProps={`
                        th:first-of-type {
                            border-radius: 4px 0 0 0;
                        }
                        th:last-of-type {
                            border-radius: 0 4px 0 0;
                        border-right: none;
                        }
                    `}
                    >
                        <TableRow>
                            <ListRowContext.Provider value={{ type: ListRowTypes.Header, i18n }}>
                                {children}
                            </ListRowContext.Provider>
                        </TableRow>
                    </Tablehead>
                )}
                <TableBody
                    styleProps={`
                    tr:nth-child(2n+2) {
                        background-color: ${colors.grey.pale};
                        
                        &:hover {
                            background-color: ${colors.grey.lighter};
                        }
                    }
    
                    tr > td:last-of-type {
                        border-right: none;
                    }
                    
                    tr:last-of-type {
                        > td {
                            border-bottom: none;
                        }
                        > td:first-of-type {
                            border-radius: 0 0 0 4px;
                        }
                        > td:last-of-type {
                            border-radius: 0 0 4px 0;
                        }
                    }
                `}
                >
                    {dataFetchingState === DataFetchingState.Pending && (
                        <TableRow>
                            <TableData
                                px={4}
                                height={"51px"}
                                textAlign={"center"}
                                colSpan={React.Children.count(children)}
                            >
                                <Spinner size={"s"} center />
                            </TableData>
                        </TableRow>
                    )}
                    {dataFetchingState === DataFetchingState.Fulfilled && data.length === 0 && (
                        <TableRow>
                            <TableData
                                px={4}
                                height={"51px"}
                                textAlign={"center"}
                                colSpan={React.Children.count(children)}
                            >
                                <Text content={staticI18n.noData} fontStyle={"italic"} color={"grey.medium"} />
                            </TableData>
                        </TableRow>
                    )}
                    {
                        dataFetchingState === DataFetchingState.Fulfilled &&
                        data.length > 0 &&
                        data.map((dataset: FormDataState) => (
                            <ListRowContext.Provider
                                key={JSON.stringify(dataset)}
                                value={{ type: ListRowTypes.Data, data: dataset }}
                            >
                                <TableRow
                                    styleProps={`
                                &:hover {
                                    background-color: ${colors.grey.lighter};
                                }
                            `}
                                >
                                    {children}
                                </TableRow>
                            </ListRowContext.Provider>
                        ))
                    }
                    {dataFetchingState === DataFetchingState.Rejected && (
                        <TableRow>
                            <TableData
                                px={4}
                                height={"51px"}
                                textAlign={"center"}
                                colSpan={React.Children.count(children)}
                                backgroundColor={"state.error.light"}
                            >
                                <Text content={staticI18n.dataFetchingRejected} color={"state.error.dark"} />
                            </TableData>
                        </TableRow>
                    )}
                </TableBody>
            </Table>
        </>
    );
};

List.defaultProps = { hideHeader: false };
