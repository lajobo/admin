import React from "react";
import { useSelector } from "react-redux";
import { Flex } from "../structure/flex";
import { Button } from "../button/button";
import { Icon, Icons } from "../icon/icon";
import { Text } from "../typography/text";
import { Link } from "../link/link";
import { ComponentUiListControllerI18n } from "../../../types/i18n";
import { selectI18n } from "../../../store/selectors/i18n";

export const ListController: React.FC = () => {
    const i18n: ComponentUiListControllerI18n = useSelector(selectI18n).components.ui.list.controller;

    return (
        <Flex mb={5} justifyContent={"flex-end"}>
            <Link to={"create"}>
                <Button maxWidth={"initial"}>
                    <Icon icon={Icons.Add} fill={"inverted"} size={"xs"} mr={2} />
                    <Text content={i18n.create} color={"inverted"} size={"m"} />
                </Button>
            </Link>
        </Flex>
    );
};
