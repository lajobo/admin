import React, { PropsWithChildren } from "react";
import { Box } from "../structure/box";
import { Heading } from "../typography/heading";
import { Button, Colors } from "../button/button";

interface Action {
    text: string;
    color?: Colors;
    onClick: () => void;
}

interface BasicModalProps {
    title?: string;
    actions?: Action[];
}

export const Modal: React.FC<BasicModalProps> = ({
    title,
    actions,
    children
}: PropsWithChildren<BasicModalProps>) => (
    <Box
        position={"fixed"}
        display={"flex"}
        top={0}
        left={0}
        width={"100%"}
        height={"100%"}
        backgroundColor={"backdrop.medium"}
        zIndex={4}
        alignItems={"center"}
        justifyContent={"center"}
    >
        <Box
            width={["95%", "80%"]}
            backgroundColor={"white"}
            display={"flex"}
            borderRadius={"default"}
            flexDirection={"column"}
            border={"1px solid"}
            borderColor={"border"}
        >
            {title && (
                <Box px={4} py={3} borderBottom={"1px solid"} borderBottomColor={"border"}>
                    <Heading content={title} size={"l"} />
                </Box>
            )}
            {children && (
                <Box p={4} color={"body"}>
                    {children}
                </Box>
            )}
            {actions && (
                <Box
                    px={4}
                    py={3}
                    borderTop={"1px solid"}
                    borderTopColor={"border"}
                    display={"flex"}
                    flexDirection={"row"}
                    justifyContent={"flex-end"}
                >
                    {actions.map((action: Action) => (
                        <Button
                            key={JSON.stringify(action)}
                            color={action.color}
                            compact
                            onClick={action.onClick}
                        >
                            {action.text}
                        </Button>
                    ))}
                </Box>
            )}
        </Box>
    </Box>
);
