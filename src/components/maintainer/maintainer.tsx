import React from "react";
import { useDeviceSizeMaintainer } from "../../hooks/deviceSizeMaintainer";

export const Maintainer: React.FC = () => {
    useDeviceSizeMaintainer();
    return null;
};
