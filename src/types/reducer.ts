// Not to be confused with the ActionContainer in the state types. This one is for react reducers
export interface ActionContainer <T>{
    action: T;
    payload: any; // eslint-disable-line @typescript-eslint/no-explicit-any
}
