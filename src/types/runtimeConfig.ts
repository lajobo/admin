import { Envs } from "./env";
import { LogLevels } from "./logger";

export interface RuntimeConfig {
    NODE_ENV: Envs.Dev | Envs.Production;
    API_URL: string;
    AUTH_PUBLIC_KEY: string;
    LOG_LEVEL: LogLevels;
}
