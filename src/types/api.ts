export enum ApiRequestMethods {
    POST = "POST",
    GET = "GET",
    DELETE = "DELETE",
    PUT = "PUT"
}

export interface ApiRequestConfig {
    method?: ApiRequestMethods;
    headers?: {
        Authorization?: string;
        "Content-Type"?: string;
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        [key: string]: any;
    };
    body?: string;
}

export interface ApiResponse <T = unknown> {
    data: T;
    status: number;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    error?: any;
}

export interface FormBodyData {
    [key: string]: string;
}
