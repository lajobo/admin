export interface DeviceStoreState {
    userAgent: string;
    size: DeviceSize;
}

export enum DeviceSize {
    S = "s",
    M = "m",
    L = "l"
}
