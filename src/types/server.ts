import http from "http";
import { RequestWithCookies } from "./routes";
import { AnyObject } from "./generic";

export type ApiMiddlewareResponseManipulator = (
    responseBuffer: Buffer,
    proxyRes: http.IncomingMessage,
    req: http.IncomingMessage & RequestWithCookies,
    res: http.ServerResponse & { cookie: (name: string, value: string, options: AnyObject) => void }
) => string | Buffer | undefined;

export type AsyncApiMiddlewareResponseManipulator = (
    responseBuffer: Buffer,
    proxyRes: http.IncomingMessage,
    req: http.IncomingMessage & RequestWithCookies,
    res: http.ServerResponse & { cookie: (name: string, value: string, options: AnyObject) => void }
) => Promise<string | Buffer | undefined>;
