export enum RoutePaths {
    Dashboard = "/",
    AuthUserList = "/auth/user",
    AuthUserForm = "/auth/user/:userId",
    AuthClientList = "/auth/client",
    AuthClientForm = "/auth/client/:clientId"
}
