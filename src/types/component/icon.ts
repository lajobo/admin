import * as React from "react";
import {
    background,
    BackgroundProps,
    border,
    BorderProps,
    color,
    ColorProps,
    display,
    DisplayProps,
    flexbox,
    FlexboxProps,
    height,
    HeightProps,
    layout,
    LayoutProps,
    maxWidth,
    MaxWidthProps,
    position,
    PositionProps,
    space,
    SpaceProps,
    width,
    WidthProps
} from "styled-system";
import { Interpolation, InterpolationFunction } from "styled-components";
import { fill, FillProps } from "../../utils/interpolation/fill";
import { ThemeProps } from "../../utils/interpolation/theme";
import { stroke, StrokeProps } from "../../utils/interpolation/stroke";
import { iconTransform, IconTransformProps } from "../../utils/interpolation/iconTransform";

export type GeneralIconProps =
    React.SVGProps<SVGSVGElement> &
    SpaceProps &
    ColorProps &
    LayoutProps &
    FlexboxProps &
    BackgroundProps &
    PositionProps &
    WidthProps &
    HeightProps &
    MaxWidthProps &
    DisplayProps &
    BorderProps &
    ThemeProps &
    StrokeProps &
    FillProps &
    IconTransformProps;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const styledIconDefinitions: (props: GeneralIconProps) => Interpolation<any>[] = (props: GeneralIconProps) => {
    const styledDefinitions: InterpolationFunction<GeneralIconProps>[] = [
        space,
        color,
        layout,
        flexbox,
        background,
        position,
        width,
        height,
        maxWidth,
        display,
        border,
        fill,
        stroke,
        iconTransform
    ];

    return styledDefinitions.map((definition: InterpolationFunction<GeneralIconProps>) => definition(props));
};
