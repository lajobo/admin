import * as core from "express";
import { Response as NodeResponse } from "node-fetch";
import { ApiRequestConfig, ApiRequestMethods } from "../api";
import { RequestWithCookies } from "../routes";

export interface TokenRequestWhitelistEntry {
    pattern: RegExp;
    method: ApiRequestMethods;
}

export type ApiRequestMiddleware = (
    req: RequestWithCookies,
    res: core.Response,
    requestConfig: ApiRequestConfig,
    next: () => void
) => void;

export type ApiResponseMiddleware = (
    req: RequestWithCookies,
    res: core.Response,
    requestResponse: NodeResponse,
    next: () => void
) => void;
