import { AnyAction as AnyReduxAction, SerializedError } from "@reduxjs/toolkit";

export enum DataFetchingState {
    Idle,
    Pending,
    Fulfilled,
    Rejected
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type AnyAction<T = any, M = unknown> = AnyReduxAction & {
    payload: T;
    meta?: {
        arg: M;
        requestId: string;
        requestStatus: string;
    };
};

export type RejectedAction<M = unknown> = {
    error: SerializedError;
    meta?: {
        arg: M;
        requestId: string;
        requestStatus: string;
    };
};
