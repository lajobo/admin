import { DataFetchingState } from "./state";

export interface UserStoreState {
    state: DataFetchingState;
    users: Users;
}

export type Users = User[];

export interface User {
    id: string;
    username: string;
    createdAt: string;
    active: boolean;
    state?: DataFetchingState;
}
