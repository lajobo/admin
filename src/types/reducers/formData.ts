export type FormDataValue = string | number | boolean;

export type FormDataState<T = { [key: string]: FormDataValue }> = T;

export enum FormDataActions {
    SetValue
}

export interface FormError {
    field?: string;
    error: string;
}
