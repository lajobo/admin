import { FormDataState } from "../reducers/formData";

export enum ListRowTypes {
    Header,
    Data
}

export interface ListI18n {
    [key: string]: string;
}

export interface HeaderListRowContextStructure {
    type: ListRowTypes.Header;
    i18n: ListI18n;
}

export interface DataListRowContextStructure {
    type: ListRowTypes.Data;
    data: FormDataState;
}

export type ListRowContextStructure = HeaderListRowContextStructure | DataListRowContextStructure;
