import { FormDataState, FormDataValue, FormError } from "../reducers/formData";
import { FormValidation } from "./validation";

export interface FormI18n {
    [key: string]: string;
}

export interface FormContextStructure {
    active: boolean;
    formData: FormDataState;
    setValue: (name: string, value: FormDataValue) => void;
    loading: boolean;
    addValidation: (validation: FormValidation) => () => void;
    i18n: FormI18n;
    errors: FormError[];
}
