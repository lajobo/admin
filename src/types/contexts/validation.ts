import { FormDataValue } from "../reducers/formData";

export interface ValidationResult {
    valid: boolean;
    errors?: string[];
    warnings?: string[];
}

export type Validation = (value: FormDataValue) => ValidationResult;

export type FieldValidation = (value: FormDataValue) => ValidationResult;

export type FormValidation = () => boolean;

export interface ValidationContextStructure {
    active: boolean;
    addValidation: (validation: Validation) => () => void;
}
