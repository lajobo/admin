export interface FieldContextStructure {
    active: boolean;
    fieldId: string;
    setFieldId: (id: string) => void;
    setFieldRequired: (required: boolean) => void;
}
