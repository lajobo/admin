import { DefaultTheme } from "styled-components";

export interface Theme extends DefaultTheme {
    space: [number, number, number, number, number, number, number];
    fontSizes: [number, number, number, number, number, number];
    lineHeights: [number, number, number, number];
    colors: {
        body: string;
        headings: string;
        inverted: string;
        background: string;
        primary: string;
        primaryDiscreet: string;
        secondary: string;
        secondaryDiscreet: string;
        error: string;
        warning: string;
        border: string;
        grey: {
            pale: string;
            lighter: string;
            light: string;
            medium: string;
            dark: string;
            darker: string;
        };
        backdrop: {
            medium: string;
        }
        state: {
            info: {
                light: string;
                medium: string;
                dark: string;
            };
            success: {
                light: string;
                medium: string;
                dark: string;
            };
            warning: {
                light: string;
                medium: string;
                dark: string;
            };
            error: {
                light: string;
                medium: string;
                dark: string;
            };
        }
    };
    radii: {
        default: string;
    };
    shadows: {
        focus: string;
        disabled: string;
        sidebar: string;
    };
    borders: {
        focus: string;
    };
    fonts: {
        body: string;
        heading: string;
        monospace: string;
    }
    breakpoints: [string, string];
}
