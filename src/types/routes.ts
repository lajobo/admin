import * as core from "express";
import Cookies from "universal-cookie";

export interface RequestWithCookies extends core.Request {
    universalCookies: Cookies;
}

export type Route = (req: RequestWithCookies, res: core.Response) => void;
