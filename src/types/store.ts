import { ThunkDispatch } from "@reduxjs/toolkit";
import { I18nStoreState } from "./i18n";
import { DeviceStoreState } from "./device";
import { AuthStoreState } from "./auth";
import { UserStoreState } from "./user";
import { AnyAction } from "./state";
import { NotificationStoreState } from "./notification";

export interface State {
    auth: AuthStoreState;
    device: DeviceStoreState;
    i18n: I18nStoreState;
    notification: NotificationStoreState;
    user: UserStoreState;
}

// Unfortunately there are no useful types exported for the thunk api
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type GetThunkAPI<T = any> = {
    dispatch: ThunkDispatch<State, {}, AnyAction<T>>
    getState: () => State;
    requestId: string;
} & any; // eslint-disable-line @typescript-eslint/no-explicit-any

export type Dispatch = ThunkDispatch<State, { }, AnyAction>;
