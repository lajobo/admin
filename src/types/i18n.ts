import { Notification } from "./notification";

export interface I18nStoreState {
    language: I18nLanguages;
    data: I18n;
}

export enum I18nLanguages {
    EN = "en",
    DE = "de"
}

export interface I18n {
    common: {
        date: CommonDateI18n,
        form: CommonFormI18n
    }
    app: AppI18n;
    components: {
        menu: {
            navigation: ComponentMenuNavigationI18n;
        },
        ui: {
            form: {
                validation: {
                    email: ComponentUiFormValidationEmailI18n;
                    password: ComponentUiFormValidationPasswordI18n;
                    required: ComponentUiFormValidationRequiredI18n;
                    error: ComponentUiFormValidationErrorI18n
                }
            },
            list: {
                controller: ComponentUiListControllerI18n;
                field: {
                    button: {
                        delete: ComponentUiListFieldButtonDeleteI18n;
                    }
                },
                list: ComponentUiListI18n;
            }
        }
    },
    pages: {
        auth: {
            user: {
                form: PageAuthUserFormI18n;
                list: PageAuthUserListI18n;
            };
        }
        login: PagesLoginI18n;
    },
    store: {
        auth: {
            logout: StoreAuthLogoutI18n;
        }
        user: {
            delete: StoreUserDeleteI18n;
        }
    }
}

export interface CommonDateI18n {
    dateFormat: string;
    timeFormat: string;
    timeFormatWithSeconds: string;
    dateTimeFormat: string;
}

export interface CommonFormButtonDeleteModalI18n {
    title: string;
    message: string;
    deleteButton: string;
    cancelButton: string;
}

export interface CommonFormI18n {
    button: {
        create: string;
        update: string;
        delete: string;
        deleteModal: CommonFormButtonDeleteModalI18n;
        cancel: string;
    }
}

export interface AppI18n {
    example: string;
    en: string;
    de: string;
}

export interface ComponentMenuNavigationI18n {
    dashboard: {
        title: string;
    };
    auth: {
        title: string;
        items: {
            user: string;
            client: string;
        }
    };
    languageSwitch: ComponentMenuNavigationLanguageSwitchI18n;
}

export interface ComponentMenuNavigationLanguageSwitchI18n {
    de: string;
    en: string;
}

export interface ComponentUiFormValidationEmailI18n {
    format: string;
}

export interface ComponentUiFormValidationPasswordI18n {
    format: string;
}

export interface ComponentUiFormValidationRequiredI18n {
    required: string;
}

export interface ComponentUiFormValidationErrorI18n {
    required: string;
    unique: string;
}

export interface ComponentUiListControllerI18n {
    create: string;
}

export interface ComponentUiListFieldButtonDeleteI18n {
    title?: string;
    message?: string;
    deleteButton?: string;
    cancelButton?: string;
}

export interface ComponentUiListI18n {
    dataFetchingRejected: string;
    noData: string;
}

export interface PageAuthUserFormI18n {
    title: string;
    form: {
        id: string;
        idPlaceholder: string;
        username: string;
        password: string;
        passwordPlaceholder: string;
        createdAt: string;
        createdAtPlaceholder: string;
        active: string;
    };
    button: {
        deletionConfirmation: Partial<CommonFormButtonDeleteModalI18n>;
    };
    notification: {
        error: Notification;
        createSuccess: Notification;
        updateSuccess: Notification;
    }
}

export interface PageAuthUserListI18n {
    title: string;
    listheads: {
        username: string;
        active: string;
    };
    deletionConfirmation: {
        title: string;
        message: string;
    }
}

export interface PagesLoginI18n {
    usernameValidationRequired: string;
    passwordValidationRequired: string;
    credentialsInvalid: string;
    username: string;
    password: string;
    submit: string;
    registerMessage: string;
    loginFailed: string;
    loginError: string;
}

export type StoreAuthLogoutI18n = Notification;

export interface StoreUserDeleteI18n {
    success: Notification;
    fail: Notification;
}
