export interface NotificationStoreState {
    notifications: StoreNotification[];
}

type NotificationTypes = "info" | "success" | "warning" | "error";

export interface Notification {
    heading: string;
    content?: string;
    duration?: number
    type?: NotificationTypes;
}

export interface StoreNotification extends Notification {
    id: string;
    duration: number;
    type: NotificationTypes;
}
