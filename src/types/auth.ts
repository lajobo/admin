export enum AuthStates {
    LoggedOut = "logged_out",
    Pending = "pending",
    LoggedIn = "logged_in"
}

export enum AuthStateDetails {
    Credentials = "credentials",
    ServerError = "server_error"
}

export enum AuthLoginStates {
    Success = "success",
    Failure = "failure",
    Error = "error"
}

export interface AuthLoginResponse {
    state: AuthLoginStates;
}

export interface AuthRefreshResponse {
    state: AuthLoginStates;
    accessToken: string;
    refreshToken: string;
}

export interface AuthStoreState {
    state: AuthStates;
    details?: AuthStateDetails;
}
