import { FormDataActions, FormDataState, FormDataValue } from "../types/reducers/formData";
import { ActionContainer } from "../types/reducer";

export type FormDataReducer = (
    state: FormDataState,
    action: ActionContainer<FormDataActions>
) => FormDataState;

export const formDataReducer: FormDataReducer = (
    state: FormDataState,
    action: ActionContainer<FormDataActions>
) => {
    switch (action.action) {
        case FormDataActions.SetValue:
            return {
                ...state,
                [action.payload.name]: action.payload.value
            };
        default:
            return { ...state };
    }
};

export const setValue: (
    name: string,
    value: FormDataValue
) => ActionContainer<FormDataActions> = (
    name: string,
    value: FormDataValue
) => ({
    action: FormDataActions.SetValue,
    payload: { name, value }
});
