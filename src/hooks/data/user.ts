import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { User } from "../../types/user";
import { DataFetchingState } from "../../types/state";
import { fetchUser } from "../../store/api/user";
import { setUserDataFetchingStateForUser } from "../../store/slices/user";
import { selectUser, selectUserDataFetchingStateForUser } from "../../store/selectors/user";
import { Dispatch } from "../../types/store";

export const useUserData: (userId: string) => [DataFetchingState, User] = (userId: string) => {
    const dataFetchingState: DataFetchingState = useSelector(selectUserDataFetchingStateForUser(userId));
    const data: User = useSelector(selectUser(userId));
    const dispatch: Dispatch = useDispatch();

    useEffect(() => {
        if (userId && [DataFetchingState.Idle, DataFetchingState.Rejected, undefined].includes(dataFetchingState)) {
            dispatch(fetchUser({ userId }));
        }

        return () => {
            if (userId) {
                dispatch(setUserDataFetchingStateForUser({ state: DataFetchingState.Idle, userId }));
            }
        };
    }, [userId]);

    return [dataFetchingState, data];
};
