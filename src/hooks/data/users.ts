import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Users } from "../../types/user";
import { DataFetchingState } from "../../types/state";
import { fetchUsers } from "../../store/api/user";
import { setUserDataFetchingState } from "../../store/slices/user";
import { selectUserDataFetchingState, selectUsers } from "../../store/selectors/user";
import { Dispatch } from "../../types/store";

export const useUsersData: () => [DataFetchingState, Users] = () => {
    const dataFetchingState: DataFetchingState = useSelector(selectUserDataFetchingState);
    const data: Users = useSelector(selectUsers);
    const dispatch: Dispatch = useDispatch();

    useEffect(() => {
        if ([DataFetchingState.Idle, DataFetchingState.Rejected].includes(dataFetchingState)) {
            dispatch(fetchUsers());
        }

        return () => {
            dispatch(setUserDataFetchingState(DataFetchingState.Idle));
        };
    }, []);

    return [dataFetchingState, data];
};
