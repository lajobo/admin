import React, { Dispatch, SetStateAction, useCallback, useRef, useState } from "react";

const isFunction: <S>(
    setStateAction: React.SetStateAction<S>
) => setStateAction is (prevState: S) => S = <S>(
    setStateAction: SetStateAction<S>
): setStateAction is (prevState: S) => S => typeof setStateAction === "function";

type StateValue <S> = S | (() => S);

// provides the same features as the native useState but returns an additional 3rd value which is a reference to the
// value to be used in stale callbacks
export const useStateRef: <S>(
    initialState?: StateValue<S>
) => [S, Dispatch<SetStateAction<S>>, React.MutableRefObject<S>] = <S>(
    initialState?: StateValue<S>
) => {
    const [state, setState] = useState<S>(initialState);
    const ref: React.MutableRefObject<S> = useRef(state);

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const dispatch: typeof setState = useCallback((setStateAction: any) => {
        ref.current = isFunction(setStateAction) ? setStateAction(ref.current) : setStateAction;

        setState(ref.current);
    }, []);

    return [state, dispatch, ref];
};
