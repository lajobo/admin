import React, { useEffect, useRef, useState } from "react";

interface TimerData {
    reset: () => void;
    toggle: (isActive?: boolean) => void;
    time: number;
    isActive?: boolean;
}

export const useTimer: (
    durationInS: number,
    stepsInS?: number,
    onFinish?: () => void,
    autostart?: boolean,
    inverted?: boolean
) => TimerData = (
    durationInS: number,
    stepsInS: number = 0.1,
    onFinish?: () => void,
    autostart?: boolean,
    inverted?: boolean
) => {
    const getResetTime: () => number = () => (inverted ? durationInS : 0);
    const [time, setTime] = useState<number>(getResetTime());
    const [isActive, setIsActive] = useState<boolean>(Boolean(autostart));
    const intervalRef: React.MutableRefObject<number> = useRef();
    const [decimalLength, setDecimalLength] = useState<number>(1);

    useEffect(() => {
        setTime(getResetTime());
    }, [durationInS]);

    const reset: () => void = () => {
        setTime(getResetTime());
    };

    const toggle: (toggleIsActive?: boolean) => void = (toggleIsActive?: boolean | undefined) => {
        // do not toggle the timer if it has finished and was not reset
        setIsActive((currentIsActive: boolean) => (
            (typeof toggleIsActive === "boolean" ? toggleIsActive : !currentIsActive) &&
            (
                (inverted && time > 0) ||
                (!inverted && time < durationInS)
            )
        ));
    };

    // timer interval
    useEffect(() => {
        clearInterval(intervalRef.current);

        if (isActive && ((inverted && time > 0) || (!inverted && time < durationInS))) {
            intervalRef.current = window.setInterval(() => {
                setTime((currentTime: number) => currentTime + (stepsInS * (inverted ? -1 : 1)));
            }, stepsInS * 1000);
        }
    }, [isActive, time]);

    // stop the time when the duration is reached
    useEffect(() => {
        if ((inverted && time <= 0) || (!inverted && time >= durationInS)) {
            setIsActive(false);
            clearInterval(intervalRef.current);

            if (onFinish) {
                onFinish();
            }
        }
    }, [time]);

    // determine the amount of decimal places to keep based on the stepsInS value
    useEffect(() => {
        const stepsParts: string[] = String(stepsInS).split(".");
        setDecimalLength(stepsParts?.[1]?.length || 1);
    }, [stepsInS]);

    return {
        reset,
        toggle,
        time: parseFloat((time).toFixed(decimalLength)),
        isActive
    };
};
