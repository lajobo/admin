import { useContext, useEffect } from "react";
import { FieldContextStructure } from "../types/contexts/field";
import { FieldContext } from "../contexts/field";

export const useFieldContext: (id: string) => void = (id: string) => {
    const fieldContext: FieldContextStructure = useContext<FieldContextStructure>(FieldContext);

    useEffect(() => {
        if (fieldContext?.active && id && !fieldContext?.fieldId) {
            fieldContext.setFieldId(id);
        }
    }, [id, fieldContext]);
};
