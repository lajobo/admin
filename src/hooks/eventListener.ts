/* eslint-disable @typescript-eslint/no-explicit-any */
import * as React from "react";
import { useEffect, useRef } from "react";

export const useEventListener: (eventName: string, handler: (event: any) => void, element?: HTMLElement) => void = (
    eventName: string,
    handler: (event: any) => void,
    element: HTMLElement
) => {
    const handlerRef: React.MutableRefObject<any> = useRef();

    useEffect(() => {
        handlerRef.current = handler;
    }, [handler]);

    useEffect(
        () => {
            const eventElement: Partial<HTMLElement> = element;

            if (!eventElement || !eventElement.addEventListener) {
                return;
            }

            const eventListener: (event: any) => void = (event: any) => handlerRef.current(event);

            eventElement.addEventListener(eventName, eventListener);

            // eslint-disable-next-line consistent-return
            return () => {
                eventElement.removeEventListener(eventName, eventListener);
            };
        },
        [eventName, element]
    );
};
