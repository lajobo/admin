import { useContext } from "react";
import { UserAgentContext } from "../contexts/userAgent";

export const useUserAgent: () => string = () => useContext<string>(UserAgentContext);
