import { useState } from "react";
import {
    FieldValidation,
    Validation,
    ValidationContextStructure,
    ValidationResult
} from "../types/contexts/validation";
import { FormDataValue } from "../types/reducers/formData";
import { useStateRef } from "./stateRef";

export interface ValidationContextProviderData {
    context: ValidationContextStructure;
    validate: FieldValidation;
    validationResult: ValidationResult;
    setFormErrors: (formErrors: string[]) => void;
}

export const useValidationContextProvider: () => ValidationContextProviderData = () => {
    const [validations, setValidations, validationsRef] = useStateRef<Validation[]>([]);
    const [validationResult, setValidationResult] = useState<ValidationResult>({ valid: true });
    const [formErrors, setFormErrors] = useState<string[]>([]);

    const addValidation: (validation: Validation) => () => void = (newValidation: Validation) => {
        setValidations([...validationsRef.current, newValidation]);

        return () => {
            setValidations(validations.filter((storedValidation: Validation) => storedValidation !== newValidation));
        };
    };

    const validate: FieldValidation = (value: FormDataValue) => {
        // eslint-disable-next-line @typescript-eslint/naming-convention,no-underscore-dangle
        const _validationResult: ValidationResult = validationsRef?.current
            .reduce<ValidationResult>((resultSum: ValidationResult, validation: Validation) => {
            const result: ValidationResult = validation(value);

            return {
                valid: resultSum.valid && result.valid,
                errors: [...(resultSum?.errors || []), ...(result?.errors || [])],
                warnings: [...(resultSum?.warnings || []), ...(result?.warnings || [])]
            };
        }, { valid: true });

        setValidationResult(_validationResult);

        return _validationResult;
    };

    return {
        context: {
            active: true,
            addValidation
        },
        validate,
        validationResult: {
            valid: validationResult.valid && formErrors.length === 0,
            errors: validationResult.errors || formErrors.length > 0
                ? [...validationResult.errors, ...formErrors]
                : undefined,
            warnings: validationResult.warnings
        },
        setFormErrors: (newFormErrors: string[]) => {
            if (JSON.stringify(formErrors) !== JSON.stringify(newFormErrors)) {
                setFormErrors(newFormErrors);
            }
        }
    };
};
