import React, { useContext, useEffect, useRef } from "react";
import { useSelector } from "react-redux";
import { FormContextStructure } from "../types/contexts/form";
import { FormContext } from "../contexts/form";
import { log } from "../utils/logger";
import { FormDataValue, FormError } from "../types/reducers/formData";
import { LogLevels } from "../types/logger";
import { useFieldContext } from "./fieldContext";
import { FieldValidation } from "../types/contexts/validation";
import { selectI18n } from "../store/selectors/i18n";
import { ComponentUiFormValidationErrorI18n } from "../types/i18n";

export interface FormContextProps {
    name: string;
    id?: string;
    onChange?: (event: React.FormEvent<HTMLElement>) => void;
}

export interface FormContextData {
    active: boolean;
    id: string;
    onChange: (event: React.FormEvent<HTMLElement>, value: FormDataValue) => void;
    value: FormDataValue | undefined;
    loading: boolean;
    placeholder?: string;
}

export const useFormContext: (
    name: string,
    id: string,
    value: FormDataValue,
    defaultValue: FormDataValue,
    onChange: (event: React.FormEvent<HTMLElement>) => void,
    validate: FieldValidation,
    onFormErrorsChange: (formErrors: string[]) => void,
    fallbackValue?: FormDataValue
) => FormContextData = (
    name: string,
    propsId: string,
    propsValue: FormDataValue,
    defaultValue: FormDataValue,
    propsOnChange: (event: React.FormEvent<HTMLElement>) => void,
    validate: FieldValidation,
    onFormErrorsChange: (formErrors: string[]) => void,
    fallbackValue: FormDataValue = ""
) => {
    const id: string = propsId || name;
    const formContext: FormContextStructure = useContext<FormContextStructure>(FormContext);
    const value: FormDataValue = formContext?.active
        ? (formContext.formData && formContext.formData[id] ? formContext.formData[id] : fallbackValue)
        : undefined;
    const valueRef: React.MutableRefObject<FormDataValue> = useRef(value);
    const i18n: ComponentUiFormValidationErrorI18n = useSelector(selectI18n).components.ui.form.validation.error;
    useFieldContext(id);

    const onChange: (
        event: React.FormEvent<HTMLElement>,
        value: FormDataValue
    ) => void = (
        event: React.FormEvent<HTMLElement>,
        newValue: FormDataValue
    ) => {
        // forward change event to formContext
        if (formContext?.active) {
            formContext.setValue(id, newValue);
        }

        // forward change event to props
        if (propsOnChange) {
            propsOnChange(event);
        }
    };

    useEffect(() => {
        valueRef.current = value;
    }, [value]);

    useEffect(() => {
        // validate input props on mount (controlled vs uncontrolled vs formContext)
        if ((propsValue || defaultValue) && formContext?.active) {
            log(
                LogLevels.Error,
                "A form/* component can either be used in a form context or in an un-/controlled state, but both at " +
                "a time is not possible. The props \"value\" and \"defaultValue\" are therefore forbidden. Consider " +
                "using the form components formData and onSubmit prop instead."
            );
        }

        let removeValidation: () => void;

        // register input in formContext
        if (formContext) {
            formContext.setValue(id, formContext.formData[id] || fallbackValue);

            // return the returned value of addValidation to remove the validation from the context on unmount.
            // value needs to be passed as a reference since the callback would not be able to access the updated
            // value otherwise and would calculate with a stale state
            removeValidation = formContext.addValidation(() => validate(valueRef?.current).valid);
        }

        return removeValidation;
    }, []);

    useEffect(() => {
        onFormErrorsChange(formContext.errors
            .filter((error: FormError) => error.field === id)
            .map((error: FormError) => (i18n?.[error.error] ? i18n?.[error.error] : error.error)));
    }, [formContext.errors]);

    return {
        active: formContext.active,
        id,
        onChange,
        value,
        loading: formContext.loading,
        placeholder: formContext.i18n?.[`${name}Placeholder`]
    };
};
