import { useCookies as useReactCookies } from "react-cookie";
import { Cookie, CookieSetOptions } from "universal-cookie";
import { Cookies } from "../types/cookies";
import { getCookieSetOptions } from "../utils/cookies";
import { useUserAgent } from "./userAgent";

export interface UseCookiesData {
    cookies: { [name: string]: string; };
    setCookie: (name: Cookies, value: Cookie, days?: number, options?: CookieSetOptions) => void;
    removeCookie: (name: string, options?: CookieSetOptions) => void;
}

export const useCookies: (dependencies?: string[]) => UseCookiesData = (dependencies?: string[]) => {
    const userAgent: string = useUserAgent();
    const [cookies, setReactCookie, removeCookie] = useReactCookies(dependencies);

    const setCookie: (name: Cookies, value: Cookie, days?: number, options?: CookieSetOptions) => void = (
        name: Cookies,
        value: Cookie,
        days?: number,
        options?: CookieSetOptions
    ) => {
        setReactCookie(
            name,
            value,
            getCookieSetOptions({
                days,
                userAgent,
                ...(options || {})
            })
        );
    };

    return {
        cookies,
        setCookie,
        removeCookie
    };
};
