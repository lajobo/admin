import { useContext, useEffect } from "react";
import { Validation, ValidationContextStructure } from "../types/contexts/validation";
import { ValidationContext } from "../contexts/validation";

export const useValidationContext: (validation: Validation) => void = (validation: Validation) => {
    const validationContext: ValidationContextStructure = useContext<ValidationContextStructure>(ValidationContext);

    // return the returned value of addValidation to remove the validation from the context on unmount
    useEffect(() => validationContext.addValidation(validation), []);
};
