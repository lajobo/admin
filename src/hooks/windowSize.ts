import { useEffect, useState } from "react";

export interface WindowSize {
    width: number;
    height: number;
}

export const useWindowSize: () => WindowSize = () => {
    const [windowSize, setWindowSize] = useState<WindowSize>({
        width: undefined,
        height: undefined
    });

    const onResize: () => void = () => {
        setWindowSize({
            width: window.innerWidth,
            height: window.innerHeight
        });
    };

    useEffect(() => {
        window.addEventListener("resize", onResize);
        onResize();
        return () => window.removeEventListener("resize", onResize);
    }, []);

    return windowSize;
};
