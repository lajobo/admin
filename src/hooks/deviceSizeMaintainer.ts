import { useContext, useEffect } from "react";
import { ThemeContext } from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { Theme } from "../types/ui";
import { useWindowSize, WindowSize } from "./windowSize";
import { DeviceSize } from "../types/device";
import { Dispatch } from "../types/store";
import { selectDeviceSize } from "../store/selectors/device";
import { setDeviceSize } from "../store/slices/device";

export const useDeviceSizeMaintainer: () => void = () => {
    const { breakpoints }: Theme = useContext(ThemeContext);
    const windowSize: WindowSize = useWindowSize();
    const dispatch: Dispatch = useDispatch();
    const currentDeviceSize: DeviceSize = useSelector(selectDeviceSize);

    useEffect(() => {
        const deviceSize: DeviceSize = breakpoints.reduce<DeviceSize>(
            (resultDeviceSize: DeviceSize, breakpoint: string, index: number) => (
                resultDeviceSize === DeviceSize.L && windowSize.width < Number(breakpoint.replace(/\D+/, ""))
                    ? (index === 0 ? DeviceSize.S : (index === 1 ? DeviceSize.M : DeviceSize.L))
                    : resultDeviceSize),
            DeviceSize.L
        );

        if (currentDeviceSize !== deviceSize) {
            dispatch(setDeviceSize(deviceSize));
        }
    }, [windowSize?.width]);
};
