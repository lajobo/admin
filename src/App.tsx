import React from "react";
import { useSelector } from "react-redux";
import { LoginPage } from "./pages/login/login";
import { AuthStates } from "./types/auth";
import { Flex } from "./components/ui/structure/flex";
import { MainMenu } from "./components/menu/main";
import { GlobalStyle } from "./components/ui/style/global";
import "normalize.css";
import { Router } from "./components/router/router";
import { selectAuthState } from "./store/selectors/auth";
import { NotificationManager } from "./components/notification/manager";
import { Maintainer } from "./components/maintainer/maintainer";

const App: React.FunctionComponent = () => {
    const authState: AuthStates = useSelector(selectAuthState);

    return (
        <>
            <Maintainer />
            <GlobalStyle />
            <NotificationManager />
            <Flex
                flexDirection={"column"}
                height={authState !== AuthStates.LoggedIn ? "100%" : undefined}
                minHeight={"100%"}
                backgroundColor={authState !== AuthStates.LoggedIn ? "white" : ["white", "grey.pale"]}
            >
                {authState !== AuthStates.LoggedIn && (
                    <LoginPage />
                )}
                {authState === AuthStates.LoggedIn && (
                    <>
                        <MainMenu />
                        <Flex pt={["68px", "98px"]}>
                            <Router />
                        </Flex>
                    </>
                )}
            </Flex>
        </>
    );
};

export default App;
