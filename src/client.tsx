import { BrowserRouter } from "react-router-dom";
import React from "react";
import { hydrate } from "react-dom";
import { Store } from "redux";
import { Provider } from "react-redux";
import { CookiesProvider } from "react-cookie";
import { ThemeProvider } from "styled-components";
import App from "./App";
import { configureStore } from "./store/configureStore";
import { UserAgentContext } from "./contexts/userAgent";
import { State } from "./types/store";
import { defaultTheme } from "./components/ui/theme";

declare const module: {
    hot: {
        accept: (path?: string, callback?: () => void) => void
    }
};

declare global {
    interface Window {
        __PRELOADED_STATE__?: State;
    }
}

// eslint-disable-next-line no-underscore-dangle
const store: Store<State> = configureStore(window.__PRELOADED_STATE__);

hydrate(
    (
        <UserAgentContext.Provider value={window.navigator.userAgent}>
            <CookiesProvider>
                <BrowserRouter>
                    <Provider store={store}>
                        <ThemeProvider theme={defaultTheme}>
                            <App />
                        </ThemeProvider>
                    </Provider>
                </BrowserRouter>
            </CookiesProvider>
        </UserAgentContext.Provider>
    ),
    document.getElementById("root")
);

if (module.hot) {
    module.hot.accept();
}
