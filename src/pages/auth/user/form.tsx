import React, { useState } from "react";
import { Location } from "history";
import { useLocation } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { NavigateFunction, useNavigate } from "react-router";
import { AsyncThunk } from "@reduxjs/toolkit";
import { Page, PageWidth } from "../../../components/ui/structure/page";
import { getFormId } from "../../../utils/form";
import { useUserData } from "../../../hooks/data/user";
import { Form } from "../../../components/ui/form/form";
import { Group } from "../../../components/ui/form/group";
import { Field } from "../../../components/ui/form/field";
import { Input } from "../../../components/ui/form/input";
import { ValidationRequired } from "../../../components/ui/form/validation/required";
import { Checkbox } from "../../../components/ui/form/checkbox";
import { PageAuthUserFormI18n } from "../../../types/i18n";
import { selectI18n } from "../../../store/selectors/i18n";
import { ButtonSubmit } from "../../../components/ui/form/button/submit";
import { ButtonCancel } from "../../../components/ui/form/button/cancel";
import { ButtonDelete } from "../../../components/ui/form/button/delete";
import { createUser, deleteUser, SaveUserProps, updateUser } from "../../../store/api/user";
import { FormDataState, FormError } from "../../../types/reducers/formData";
import { User } from "../../../types/user";
import { ValidationPassword } from "../../../components/ui/form/validation/password";
import { ValidationEmail } from "../../../components/ui/form/validation/email";
import { Dispatch } from "../../../types/store";
import { addNotification } from "../../../store/slices/notification";

export const AuthUserFormPage: React.FC = () => {
    const location: Location = useLocation();
    const navigate: NavigateFunction = useNavigate();
    const userId: string = getFormId(location.pathname);
    const data: User = useUserData(userId)?.[1];
    const i18n: PageAuthUserFormI18n = useSelector(selectI18n).pages.auth.user.form;
    const dispatch: Dispatch = useDispatch();
    const [formErrors, setFormErrors] = useState<FormError[]>([]);

    return (
        <Page width={PageWidth.L} title={i18n.title}>
            <Form
                defaultFormData={{
                    id: data?.id,
                    username: data?.username,
                    createdAt: data?.createdAt.substr(0, data?.createdAt.indexOf(".")),
                    active: data?.active
                }}
                i18n={i18n.form}
                onFormSubmit={(formData: FormDataState, valid: boolean) => {
                    if (valid) {
                        const saveThunk: AsyncThunk<void, SaveUserProps, {}> = data?.id ? updateUser : createUser;

                        dispatch(saveThunk({
                            formData,
                            onSuccess: () => {
                                dispatch(addNotification({
                                    type: "success",
                                    ...i18n.notification[data?.id ? "updateSuccess" : "createSuccess"]
                                }));
                                navigate(-1);
                            },
                            onFailure: (errors: FormError[]) => {
                                setFormErrors(errors);
                            },
                            onError: () => {
                                dispatch(addNotification({
                                    type: "error",
                                    ...i18n.notification.error
                                }));
                            }
                        }));
                    }
                }}
                errors={formErrors}
            >
                <Group>
                    <Field>
                        <Input name={"id"} disabled />
                    </Field>
                </Group>
                <Group>
                    <Field>
                        <Input
                            name={"username"}
                            autoComplete={"username"}
                        >
                            <ValidationRequired />
                            <ValidationEmail />
                        </Input>
                    </Field>
                </Group>
                <Group>
                    <Field>
                        <Input
                            name={"password"}
                            type={"password"}
                            autoComplete={"new-password"}
                            {...(!userId ? { placeholder: "" } : undefined)}
                        >
                            {!userId && (
                                <ValidationRequired />
                            )}
                            <ValidationPassword />
                        </Input>
                    </Field>
                </Group>
                <Group>
                    <Field>
                        <Input name={"createdAt"} type={"datetime-local"} disabled />
                    </Field>
                </Group>
                <Group>
                    <Field>
                        <Checkbox name={"active"} />
                    </Field>
                </Group>
                <ButtonSubmit initial={!userId} />
                {userId && (
                    <ButtonDelete
                        modalI18n={i18n.button.deletionConfirmation}
                        deleteFunction={deleteUser}
                        deleteFunctionPropModifier={(deleteData: FormDataState) => ({ userId: String(deleteData.id) })}
                    />
                )}
                <ButtonCancel />
            </Form>
        </Page>
    );
};
