import React from "react";
import { useSelector } from "react-redux";
import { Page, PageWidth } from "../../../components/ui/structure/page";
import { List } from "../../../components/ui/list/list";
import { ListColumn } from "../../../components/ui/list/column";
import { FormDataState } from "../../../types/reducers/formData";
import { ListTextField } from "../../../components/ui/list/field/text";
import { ListCheckboxField } from "../../../components/ui/list/field/checkbox";
import { PageAuthUserListI18n } from "../../../types/i18n";
import { selectI18n } from "../../../store/selectors/i18n";
import { ListEditButtonField } from "../../../components/ui/list/field/button/edit";
import { ListDeleteButtonField } from "../../../components/ui/list/field/button/delete";
import { deleteUser } from "../../../store/api/user";
import { useUsersData } from "../../../hooks/data/users";

export const AuthUserListPage: React.FC = () => {
    const i18n: PageAuthUserListI18n = useSelector(selectI18n).pages.auth.user.list;
    const [dataFetchingState, data] = useUsersData();

    return (
        <Page width={PageWidth.L} title={i18n.title}>
            <List
                i18n={i18n.listheads}
                dataFetchingState={dataFetchingState}
                data={data as never}
            >
                <ListColumn name={"edit"} shrinkToContent compact>
                    <ListEditButtonField />
                </ListColumn>
                <ListColumn name={"username"}>
                    <ListTextField name={"username"} />
                </ListColumn>
                <ListColumn name={"active"} shrinkToContent center>
                    <ListCheckboxField name={"active"} />
                </ListColumn>
                <ListColumn name={"delete"} shrinkToContent compact>
                    <ListDeleteButtonField
                        modalI18n={i18n.deletionConfirmation}
                        deleteFunction={deleteUser}
                        deleteFunctionPropModifier={(deleteData: FormDataState) => ({ userId: String(deleteData.id) })}
                    />
                </ListColumn>
            </List>
        </Page>
    );
};
