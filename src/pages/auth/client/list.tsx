import React from "react";
import { Page, PageWidth } from "../../../components/ui/structure/page";
import { Heading } from "../../../components/ui/typography/heading";
import { log } from "../../../utils/logger";
import { LogLevels } from "../../../types/logger";

export const AuthClientListPage: React.FC = () => {
    log(LogLevels.Debug, "AuthClientListPage");
    return (
        <Page width={PageWidth.L}>
            <Heading size={"xl"} content={"AuthClientListPage"} />
        </Page>
    );
};
