import React from "react";
import { Page, PageWidth } from "../../components/ui/structure/page";
import { Heading } from "../../components/ui/typography/heading";
import { log } from "../../utils/logger";
import { LogLevels } from "../../types/logger";

export const DashboardPage: React.FC = () => {
    log(LogLevels.Debug, "DashboardPage");
    return (
        <Page width={PageWidth.Fullwidth} title={"Dashboard"}>
            <Heading size={"xl"} content={"DashboardPage"} />
        </Page>
    );
};
