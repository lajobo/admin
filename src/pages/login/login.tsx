import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { AnyAction, ThunkDispatch } from "@reduxjs/toolkit";
import { Page, PageWidth } from "../../components/ui/structure/page";
import { Form } from "../../components/ui/form/form";
import { FormDataState, FormError } from "../../types/reducers/formData";
import { log } from "../../utils/logger";
import { LogLevels } from "../../types/logger";
import { Group } from "../../components/ui/form/group";
import { Field } from "../../components/ui/form/field";
import { Input } from "../../components/ui/form/input";
import { Button } from "../../components/ui/form/button";
import { Segment } from "../../components/ui/structure/segment";
import { Icon, Icons } from "../../components/ui/icon/icon";
import { Message } from "../../components/ui/message/message";
import { ValidationRequired } from "../../components/ui/form/validation/required";
import { PagesLoginI18n } from "../../types/i18n";
import { selectI18n } from "../../store/selectors/i18n";
import { ValidationEmail } from "../../components/ui/form/validation/email";
import { ValidationPassword } from "../../components/ui/form/validation/password";
import { login } from "../../store/api/auth/login";
import { AuthStateDetails, AuthStates } from "../../types/auth";
import { selectAuthState, selectAuthStateDetails } from "../../store/selectors/auth";
import { State } from "../../types/store";
import { LanguageSwitch } from "../../components/language/switch";

interface LoginFormData {
    username: string;
    password: string;
}

export const LoginPage: React.FC = () => {
    const dispatch: ThunkDispatch<State, {}, AnyAction> = useDispatch();
    const i18n: PagesLoginI18n = useSelector(selectI18n).pages.login;
    const authState: AuthStates = useSelector(selectAuthState);
    const authStateDetails: AuthStateDetails = useSelector(selectAuthStateDetails);
    const formErrors: FormError[] = authState !== AuthStates.LoggedOut || !authStateDetails
        ? undefined
        : [{ error: authStateDetails === AuthStateDetails.Credentials ? i18n.loginFailed : i18n.loginError }];

    return (
        <Page
            width={PageWidth.S}
            center
            backgroundColor={"grey.lighter"}
            innerBackgroundColor={"grey.lighter"}
            innerBorderColor={"transparent"}
        >
            <Icon icon={Icons.BrandLogo} height={"60px"} mb={4} />
            <Segment stacked mb={4} py={4}>
                <Form
                    onFormSubmit={async (formData: FormDataState<LoginFormData>, valid: boolean) => {
                        if (valid) {
                            log(LogLevels.Debug, "submit", formData);
                            dispatch(login({ username: formData.username, password: formData.password }));
                        }
                    }}
                    loading={authState === AuthStates.Pending}
                    errors={formErrors}
                >
                    <Group>
                        <Field>
                            <Input
                                name={"username"}
                                placeholder={i18n.username}
                                icon={Icons.User}
                                autoComplete={"username"}
                            >
                                <ValidationRequired i18n={{ required: i18n.usernameValidationRequired }} />
                                <ValidationEmail />
                            </Input>
                        </Field>
                    </Group>
                    <Group>
                        <Field>
                            <Input
                                type={"password"}
                                name={"password"}
                                placeholder={i18n.password}
                                icon={Icons.Lock}
                                autoComplete={"current-password"}
                            >
                                <ValidationRequired i18n={{ required: i18n.passwordValidationRequired }} />
                                <ValidationPassword />
                            </Input>
                        </Field>
                    </Group>
                    <Button type={"submit"} fullwidth content={i18n.submit} />
                    {/* {loginFailed && (<Message type={"error"} content={i18n.credentialsInvalid} mt={4} size={"s"} />)} */}
                </Form>
            </Segment>
            <Message type={"info"} center content={i18n.registerMessage} />
            <LanguageSwitch darkMode />
        </Page>
    );
};
