import * as core from "express";
import { Response as NodeResponse } from "node-fetch";
import { ApiResponseMiddleware } from "../../../../types/server/api";
import { RequestWithCookies } from "../../../../types/routes";
import { AnyObject } from "../../../../types/generic";
import { Cookies } from "../../../../types/cookies";
import { AuthLoginStates } from "../../../../types/auth";

export const ApiResponseLoginMiddleware: ApiResponseMiddleware = (
    req: RequestWithCookies,
    res: core.Response,
    requestResponse: NodeResponse,
    next: () => void
) => {
    if (req.url === "/api/auth/login") {
        if (requestResponse.status === 201) {
            requestResponse
                .json()
                .then((data: any) => { // eslint-disable-line @typescript-eslint/no-explicit-any
                    if (
                        typeof data === "object" &&
                        Object.prototype.hasOwnProperty.call(data, "accessToken") &&
                        Object.prototype.hasOwnProperty.call(data, "refreshToken")
                    ) {
                        const cookieOptions: AnyObject = {
                            // store the cookie for 100 days so that the user won't need to log in again everytime he doesn't
                            // visit the site for a while
                            maxAge: 100 * 24 * 60 * 60,
                            httpOnly: true,
                            sameSite: "strict",
                            secure: process.env.SECURE === "true"
                        };
                        res.cookie(Cookies.AccessToken, data.accessToken, cookieOptions);
                        res.cookie(Cookies.RefreshToken, data.refreshToken, cookieOptions);

                        res.status(201).send(JSON.stringify({ state: AuthLoginStates.Success }));
                    }
                })
                .catch(() => {
                    res.status(requestResponse.status).send(JSON.stringify({ state: AuthLoginStates.Error }));
                });
        } else if ([400, 401].includes(requestResponse.status)) {
            res.status(requestResponse.status).send(JSON.stringify({ state: AuthLoginStates.Failure }));
        } else {
            res.status(requestResponse.status).send(JSON.stringify({ state: AuthLoginStates.Error }));
        }
    } else {
        next();
    }
};
