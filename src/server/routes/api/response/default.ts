import * as core from "express";
import { Response as NodeResponse } from "node-fetch";
import { ApiResponseMiddleware } from "../../../../types/server/api";
import { RequestWithCookies } from "../../../../types/routes";

export const ApiResponseDefaultMiddleware: ApiResponseMiddleware = async (
    req: RequestWithCookies,
    res: core.Response,
    requestResponse: NodeResponse
) => {
    requestResponse
        .text()
        .then((data: any) => { // eslint-disable-line @typescript-eslint/no-explicit-any
            res
                .status(requestResponse.status)
                .setHeader("content-type", requestResponse.headers.get("content-type"))
                .send(data);
        })
        .catch(() => {
            res.status(500).send();
        });
};
