import * as core from "express";
import { Response as NodeResponse } from "node-fetch";
import { ApiResponseMiddleware } from "../../../../types/server/api";
import { RequestWithCookies } from "../../../../types/routes";
import { AuthLoginStates } from "../../../../types/auth";

export const ApiResponseRefreshMiddleware: ApiResponseMiddleware = (
    req: RequestWithCookies,
    res: core.Response,
    requestResponse: NodeResponse,
    next: () => void
) => {
    if (req.url === "/api/auth/refresh") {
        if ([200, 201].includes(requestResponse.status)) {
            requestResponse
                .json()
                .then((data: any) => { // eslint-disable-line @typescript-eslint/no-explicit-any
                    if (
                        typeof data === "object" &&
                        Object.prototype.hasOwnProperty.call(data, "accessToken") &&
                        Object.prototype.hasOwnProperty.call(data, "refreshToken")
                    ) {
                        res
                            .status(requestResponse.status)
                            .send(JSON.stringify({
                                state: AuthLoginStates.Success,
                                accessToken: data.accessToken,
                                refreshToken: data.refreshToken
                            }));
                    }
                })
                .catch(() => {
                    res.status(requestResponse.status).send(JSON.stringify({ state: AuthLoginStates.Error }));
                });
        } else if ([400, 401].includes(requestResponse.status)) {
            res.status(requestResponse.status).send(JSON.stringify({ state: AuthLoginStates.Failure }));
        } else {
            res.status(requestResponse.status).send(JSON.stringify({ state: AuthLoginStates.Error }));
        }
    } else {
        next();
    }
};
