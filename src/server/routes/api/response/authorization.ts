import * as core from "express";
import { Response as NodeResponse } from "node-fetch";
import { ApiResponseMiddleware } from "../../../../types/server/api";
import { RequestWithCookies } from "../../../../types/routes";
import { Cookies } from "../../../../types/cookies";
import { forwardApiRequest, isRequestWhitelisted, prepareForwardApiRequestConfig } from "../../../../utils/server/api";
import { AuthLoginStates, AuthRefreshResponse } from "../../../../types/auth";
import { requestLoginRefresh } from "../../../../utils/api/auth";
import { AnyObject } from "../../../../types/generic";
import { ApiRequestConfig } from "../../../../types/api";

export const ApiResponseAuthorizationMiddleware: ApiResponseMiddleware = async (
    req: RequestWithCookies,
    res: core.Response,
    requestResponse: NodeResponse,
    next: () => void
) => {
    const refreshToken: string = req.universalCookies.get(Cookies.RefreshToken);

    if (isRequestWhitelisted(req) && requestResponse.status === 401 && refreshToken) {
        const refreshResponse: AuthRefreshResponse = await requestLoginRefresh(refreshToken, req.header["User-Agent"]);

        if (refreshResponse.state !== AuthLoginStates.Success) {
            res.cookie(Cookies.AccessToken, "", { maxAge: 0 });
            res.cookie(Cookies.RefreshToken, "", { maxAge: 0 });
            res.status(401).send();
            return;
        }

        // update token cookies since old tokens are invalid in any way after the refresh
        const cookieOptions: AnyObject = {
            // store the cookie for 100 days so that the user won't need to log in again everytime he doesn't
            // visit the site for a while
            maxAge: 100 * 24 * 60 * 60,
            httpOnly: true,
            sameSite: "strict",
            secure: process.env.SECURE === "true"
        };
        res.cookie(Cookies.AccessToken, refreshResponse.accessToken, cookieOptions);
        res.cookie(Cookies.RefreshToken, refreshResponse.refreshToken, cookieOptions);

        const requestConfig: ApiRequestConfig = prepareForwardApiRequestConfig(req);
        requestConfig.headers.Authorization = `Bearer ${refreshResponse.accessToken}`;
        requestConfig.headers["Cache-Control"] = "no-cache";
        const response: NodeResponse = await forwardApiRequest(req, requestConfig);

        res
            .status(response.status)
            .setHeader("content-type", response.headers.get("content-type"))
            .send(await response.text());
    } else {
        next();
    }
};
