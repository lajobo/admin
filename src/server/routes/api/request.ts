import * as core from "express";
import { ApiRequestMiddleware } from "../../../types/server/api";
import { ApiRequestAuthorizationMiddleware } from "./request/authorization";
import { RequestWithCookies } from "../../../types/routes";
import { ApiRequestConfig } from "../../../types/api";
import { ApiRequestLogoutMiddleware } from "./request/logout";

const requestMiddlewares: ApiRequestMiddleware[] = [
    ApiRequestAuthorizationMiddleware,
    ApiRequestLogoutMiddleware
];

export const handleRequest: (
    req: RequestWithCookies,
    res: core.Response,
    requestConfig: ApiRequestConfig,
    index?: number
) => void = (
    req: RequestWithCookies,
    res: core.Response,
    requestConfig: ApiRequestConfig,
    index: number = 0
) => {
    if (requestMiddlewares[index]) {
        requestMiddlewares[index](
            req,
            res,
            requestConfig,
            () => {
                handleRequest(req, res, requestConfig, index + 1);
            }
        );
    }
};
