import * as core from "express";
import { Response as NodeResponse } from "node-fetch";
import { ApiResponseMiddleware } from "../../../types/server/api";
import { RequestWithCookies } from "../../../types/routes";
import { ApiResponseAuthorizationMiddleware } from "./response/authorization";
import { ApiResponseLoginMiddleware } from "./response/login";
import { ApiResponseDefaultMiddleware } from "./response/default";
import { ApiResponseRefreshMiddleware } from "./response/refresh";

const responseMiddlewares: ApiResponseMiddleware[] = [
    ApiResponseAuthorizationMiddleware,
    ApiResponseRefreshMiddleware,
    ApiResponseLoginMiddleware,
    ApiResponseDefaultMiddleware
];

export const handleResponse: (
    req: RequestWithCookies,
    res: core.Response,
    requestResponse: NodeResponse,
    index?: number
) => void = (
    req: RequestWithCookies,
    res: core.Response,
    requestResponse: NodeResponse,
    index: number = 0
) => {
    if (responseMiddlewares[index]) {
        responseMiddlewares[index](
            req,
            res,
            requestResponse,
            () => {
                handleResponse(req, res, requestResponse, index + 1);
            }
        );
    }
};
