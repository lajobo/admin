import * as core from "express";
import { ApiRequestMiddleware } from "../../../../types/server/api";
import { RequestWithCookies } from "../../../../types/routes";
import { ApiRequestConfig } from "../../../../types/api";
import { Cookies } from "../../../../types/cookies";

export const ApiRequestLogoutMiddleware: ApiRequestMiddleware = (
    req: RequestWithCookies,
    res: core.Response,
    requestConfig: ApiRequestConfig,
    next: () => void
) => {
    if (req.url === "/api/auth/invalidate") {
        res.cookie(Cookies.AccessToken, "", { maxAge: 0 });
        res.cookie(Cookies.RefreshToken, "", { maxAge: 0 });
    }

    next();
};
