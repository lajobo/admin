/* eslint-disable no-param-reassign */
import * as core from "express";
import { ApiRequestMiddleware } from "../../../../types/server/api";
import { RequestWithCookies } from "../../../../types/routes";
import { ApiRequestConfig } from "../../../../types/api";
import { Cookies } from "../../../../types/cookies";
import { isRequestWhitelisted } from "../../../../utils/server/api";

export const ApiRequestAuthorizationMiddleware: ApiRequestMiddleware = (
    req: RequestWithCookies,
    res: core.Response,
    requestConfig: ApiRequestConfig,
    next: () => void
) => {
    const serverAccessToken: string = req.headers?.["x-access-token"] as string;
    const clientAccessToken: string = req.universalCookies.get(Cookies.AccessToken);
    const accessToken: string = serverAccessToken || clientAccessToken;

    if (isRequestWhitelisted(req) && !!accessToken) {
        requestConfig.headers.Authorization = `Bearer ${accessToken}`;
    }

    next();
};
