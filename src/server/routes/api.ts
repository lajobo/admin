import * as core from "express";
import { Response as NodeResponse } from "node-fetch";
import { RequestWithCookies, Route } from "../../types/routes";
import { ApiRequestConfig } from "../../types/api";
import { handleRequest } from "./api/request";
import { handleResponse } from "./api/response";
import { forwardApiRequest, prepareForwardApiRequestConfig } from "../../utils/server/api";

export const apiRoute: Route = async (req: RequestWithCookies, res: core.Response) => {
    const requestConfig: ApiRequestConfig = prepareForwardApiRequestConfig(req);
    handleRequest(req, res, requestConfig);
    const response: NodeResponse = await forwardApiRequest(req, requestConfig);
    handleResponse(req, res, response);
};
