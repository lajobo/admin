import { Request, Response } from "express";
import { Route } from "../../types/routes";

export const healthRoute: Route = (req: Request, res: Response) => res.sendStatus(200);
