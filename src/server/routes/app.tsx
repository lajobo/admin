import * as core from "express";
import React from "react";
import { StaticRouter } from "react-router-dom/server";
import { renderToString } from "react-dom/server";
import serialize from "serialize-javascript";
import { Store } from "redux";
import { Provider } from "react-redux";
import { ServerStyleSheet, ThemeProvider } from "styled-components";
import { CookiesProvider } from "react-cookie";
import { renderDocumentTemplate } from "./app/documentTemplate";
import App from "../../App";
import { RequestWithCookies, Route } from "../../types/routes";
import { configureStore } from "../../store/configureStore";
import { runtimeConfig } from "../../utils/runtimeConfig";
import { UserAgentContext } from "../../contexts/userAgent";
import { upgradeStore } from "./app/upgradeStore";
import { State } from "../../types/store";
import { defaultTheme } from "../../components/ui/theme";

// eslint-disable-next-line @typescript-eslint/no-explicit-any,import/no-dynamic-require
const assets: any = require(process.env.RAZZLE_ASSETS_MANIFEST);

export const appRoute: Route = async (req: RequestWithCookies, res: core.Response) => {
    const context: any = {}; // eslint-disable-line @typescript-eslint/no-explicit-any
    const sheet: ServerStyleSheet = new ServerStyleSheet();
    const store: Store<State> = configureStore();
    await upgradeStore(store, req, res);

    const markup: string = renderToString(sheet.collectStyles(
        <UserAgentContext.Provider value={req.headers["user-agent"]}>
            <CookiesProvider cookies={req.universalCookies}>
                <StaticRouter location={req.url}>
                    <Provider store={store}>
                        <ThemeProvider theme={defaultTheme}>
                            <App />
                        </ThemeProvider>
                    </Provider>
                </StaticRouter>
            </CookiesProvider>
        </UserAgentContext.Provider>
    ));

    if (context.url) {
        res.redirect(context.url);
    } else {
        res
            .status(200)
            .send(
                renderDocumentTemplate(
                    assets,
                    markup,
                    sheet.getStyleTags(),
                    serialize(store.getState()),
                    serialize(runtimeConfig)
                )
            );
    }
};
