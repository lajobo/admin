import * as core from "express";
import { RequestWithCookies } from "../../../../types/routes";
import { Cookies } from "../../../../types/cookies";
import { verifyTokens } from "../../../../utils/auth";
import { AuthLoginStates, AuthRefreshResponse, AuthStates } from "../../../../types/auth";
import { requestLoginRefresh } from "../../../../utils/api/auth";
import { AnyObject } from "../../../../types/generic";

/**
 * for a cleaner implementation this logic could be moved into a redux slice as well but then there would be the problem
 * to get the new tokens from the reducer to the response instance to set it as cookies without exposing the tokens in
 * any way. its just easier and safer to do it that way even if it doesn't follow the flux flow exactly.
 * since this function requires a request and response argument including access- and refresh token cookies it is
 * impossible to call it from client side without a previous data breach so that it's not a risk to receive the
 * access- and refresh token
 */
export const getAuthState: (
    req: RequestWithCookies,
    res: core.Response
) => Promise<[AuthStates, string, string]> = async (
    req: RequestWithCookies,
    res: core.Response
) => {
    let accessToken: string = req.universalCookies.get(Cookies.AccessToken);
    let refreshToken: string = req.universalCookies.get(Cookies.RefreshToken);
    let authState: AuthStates = verifyTokens(accessToken, refreshToken) ? AuthStates.LoggedIn : AuthStates.LoggedOut;

    if (authState === AuthStates.LoggedOut) {
        if (accessToken && refreshToken) {
            const loginRefreshResponse: AuthRefreshResponse = await requestLoginRefresh(
                refreshToken,
                req.headers["user-agent"]
            );

            if (loginRefreshResponse?.state === AuthLoginStates.Success) {
                const cookieOptions: AnyObject = {
                    // store the cookie for 100 days so that the user won't need to log in again everytime he doesn't
                    // visit the site for a while
                    maxAge: 100 * 24 * 60 * 60,
                    httpOnly: true,
                    sameSite: "strict",
                    secure: process.env.SECURE === "true"
                };
                res.cookie(Cookies.AccessToken, loginRefreshResponse.accessToken, cookieOptions);
                res.cookie(Cookies.RefreshToken, loginRefreshResponse.refreshToken, cookieOptions);
                accessToken = loginRefreshResponse.accessToken;
                refreshToken = loginRefreshResponse.refreshToken;
                authState = AuthStates.LoggedIn;
            } else {
                res.cookie(Cookies.AccessToken, "", { maxAge: 0 });
                res.cookie(Cookies.RefreshToken, "", { maxAge: 0 });
                accessToken = undefined;
                refreshToken = undefined;
            }
        } else {
            res.cookie(Cookies.AccessToken, "", { maxAge: 0 });
            res.cookie(Cookies.RefreshToken, "", { maxAge: 0 });
            accessToken = undefined;
            refreshToken = undefined;
        }
    }

    return [authState, accessToken, refreshToken];
};
