export const renderDocumentTemplate: (
    assets: any, // eslint-disable-line @typescript-eslint/no-explicit-any
    markup: string,
    style: string,
    serializedState: string,
    serializedRuntimeConfig: string
) => string = (
    assets: any, // eslint-disable-line @typescript-eslint/no-explicit-any
    markup: string,
    style: string,
    serializedState: string,
    serializedRuntimeConfig: string
) => `<!doctype html>
    <html lang="">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta charset="utf-8" />
        <title>kiwano</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        ${style}
        ${assets?.client?.css ? `<link rel="stylesheet" href="${assets.client.css}">` : ""}
        <script src="${assets.client.js}" defer ${process.env.NODE_ENV === "production" ? "" : " crossorigin"}></script>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@400;700;800&family=Lato:wght@400;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="/global.css">
    </head>
    <body>
        <div id="root">${markup}</div>
        <script>
            window.__RUNTIME_CONFIG__ = ${serializedRuntimeConfig};
            window.__PRELOADED_STATE__ = ${serializedState};
        </script>
    </body>
</html>`;
