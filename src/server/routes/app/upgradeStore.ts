import { Store } from "redux";
import * as core from "express";
import { AnyAction } from "@reduxjs/toolkit";
import { RequestWithCookies } from "../../../types/routes";
import { setDeviceSize, setUserAgent } from "../../../store/slices/device";
import { setAuthState } from "../../../store/slices/auth";
import { getAuthState } from "./upgradeStore/auth";
import { State } from "../../../types/store";
import { fetchUser, fetchUsers } from "../../../store/api/user";
import { AnyObject } from "../../../types/generic";
import { RoutePaths } from "../../../types/router";
import { getDeviceSizeFromUserAgent } from "../../../utils/device";
import { Cookies } from "../../../types/cookies";
import { I18nLanguages } from "../../../types/i18n";
import { setLanguage } from "../../../store/slices/i18n";

export const upgradeStore: (store: Store<State>, req: RequestWithCookies, res: core.Response) => void = async (
    store: Store<State>,
    req: RequestWithCookies,
    res: core.Response
) => {
    const [authState, accessToken, refreshToken] = await getAuthState(req, res);
    const serverHeaders: AnyObject = { "X-Access-Token": accessToken, "X-Refresh-Token": refreshToken };
    const promises: unknown[] = [
        store.dispatch(setUserAgent(req.headers["user-agent"])),
        store.dispatch(setDeviceSize(getDeviceSizeFromUserAgent(req.headers["user-agent"]))),
        store.dispatch(setAuthState(authState))
    ];

    const language: I18nLanguages = req.universalCookies.get(Cookies.Language);
    if (language) {
        promises.push(store.dispatch(setLanguage(language)));
    }

    if (req.path.startsWith(RoutePaths.AuthUserList)) {
        if ((req.path.match(/\//g) || []).length > (RoutePaths.AuthUserList.match(/\//g) || []).length) {
            // path with additional params
            const userId: string = req.path.split("/").at(-1);
            if (userId && userId !== "create") {
                promises.push(store.dispatch(fetchUser({ userId, serverHeaders }) as never as AnyAction));
            }
        } else {
            // path without any params = list
            promises.push(store.dispatch(fetchUsers(serverHeaders) as never as AnyAction));
        }
    }

    await Promise.all(promises);
};
