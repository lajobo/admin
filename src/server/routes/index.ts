import { Router } from "express";
import { healthRoute } from "./health";
import { appRoute } from "./app";
import { apiRoute } from "./api";

export const router: Router = Router();

router.get("/health", healthRoute);
router.all("/api/*", apiRoute);
router.get("/*", appRoute);
