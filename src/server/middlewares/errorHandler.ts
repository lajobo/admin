/* eslint-disable no-console */
import * as core from "express";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const errorHandlerMiddleware: (err: any, req: core.Request, res: core.Response, next: () => void) => void = (
    err: any, // eslint-disable-line @typescript-eslint/no-explicit-any
    req: core.Request,
    res: core.Response,
    next: () => void
) => {
    console.error("--------------------------");
    console.error("Error", err);
    console.error("--------------------------");

    next();
};
