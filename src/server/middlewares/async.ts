import express, { NextFunction, Router } from "express";

// Make sure to `.catch()` any errors and pass them along to the `next()`
// middleware in the chain, in this case the error handler.
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const asyncMiddleware: (fn: Router) => any = (fn: Router) => (
    req: express.Request,
    res: express.Response,
    next: NextFunction
) => Promise.resolve(fn(req, res, next)).catch(next);
