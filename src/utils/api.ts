import nodeFetch, { Response as NodeResponse } from "node-fetch";
import { ApiRequestConfig, ApiRequestMethods, ApiResponse, FormBodyData } from "../types/api";
import { isClient } from "./env";

export const fetch: (input: string, init?: ApiRequestConfig) => Promise<Response | NodeResponse> = isClient
    ? window.fetch
    : nodeFetch;

export const encodeFormBody: (data: FormBodyData) => string = (data: FormBodyData) => Object
    .keys(data)
    .map((key: string) => `${encodeURIComponent(key)}=${encodeURIComponent(data[key])}`)
    .join("&");

export const sendRequest: <T = unknown>(
    url: string,
    userAgent: string,
    config?: ApiRequestConfig
) => Promise<ApiResponse<T>> = async <T = unknown>(
    url: string,
    userAgent: string,
    config?: ApiRequestConfig
) => {
    const requestConfig: ApiRequestConfig = {
        method: ApiRequestMethods.GET,
        ...config,
        headers: {
            "User-Agent": userAgent,
            ...config?.headers
        }
    };

    const response: ApiResponse<T> = await fetch(url, requestConfig)
        .then(async (resp: Response) => {
            let data: T;

            try {
                if (resp.headers.get("content-type").startsWith("application/json")) {
                    data = await resp.json();
                } else {
                    data = await resp.text() as never;
                }
            } catch (e: unknown) { /* ignore if not parsable */ }

            return {
                data,
                status: resp.status
            };
        })
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .catch((error: any) => ({
            data: error,
            status: 500
        }));

    return response;
};
