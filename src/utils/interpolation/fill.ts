import { InterpolationFunction } from "styled-components";
import { resolveObjectKey } from "../object";
import { ThemeProps } from "./theme";

export interface FillProps extends ThemeProps {
    fill?: string;
}
export const fill: InterpolationFunction<FillProps> =
    (props: FillProps) => (props.fill
        ? `* { fill: ${resolveObjectKey(props.theme.colors, props.fill) || props.fill} }`
        : undefined);
