import { InterpolationFunction } from "styled-components";

type BorderSpacings = string | number | "initial" | "inherit";

export interface BorderSpacingProps {
    borderSpacing?: BorderSpacings;
}
export const borderSpacing: InterpolationFunction<BorderSpacingProps> =
    (props: BorderSpacingProps) => (props.borderSpacing ? `border-spacing: ${props.borderSpacing};` : "");
