import { ThemeProps } from "./theme";

export interface HtmlForProps extends ThemeProps {
    htmlFor?: string;
}
