import { InterpolationFunction } from "styled-components";

export interface TransformProps {
    transform?: string;
}
export const transform: InterpolationFunction<TransformProps> =
    (props: TransformProps) => (props.transform ? `transform: ${props.transform};` : "");
