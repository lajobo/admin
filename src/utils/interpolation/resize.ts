import { InterpolationFunction } from "styled-components";

type Resizes = "none" |
"both" |
"horizontal" |
"vertical" |
"block" |
"inline" |
"inherit" |
"initial" |
"unset";

export interface ResizeProps {
    resize?: Resizes;
}
export const resize: InterpolationFunction<ResizeProps> =
    (props: ResizeProps) => (props.resize ? `resize: ${props.resize};` : "");
