import { InterpolationFunction } from "styled-components";
import { ThemeProps } from "./theme";

export interface AfterProps extends ThemeProps {
    after?: string;
}
export const after: InterpolationFunction<AfterProps> = (
    props: AfterProps
) => (props.after ? `&::after { ${props.after} }` : "");
