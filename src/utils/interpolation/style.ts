import { FlattenSimpleInterpolation, InterpolationFunction } from "styled-components";

export interface StyleProps {
    styleProps?: string | FlattenSimpleInterpolation;
}
export const style: InterpolationFunction<StyleProps> = (props: StyleProps) => props.styleProps;
