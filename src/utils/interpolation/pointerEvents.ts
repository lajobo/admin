import { InterpolationFunction } from "styled-components";

type PointerEvents = "auto" |
"none" |
"visiblePainted" |
"visibleFill" |
"visibleStroke" |
"visible" |
"painted" |
"fill" |
"stroke" |
"all" |
"inherit" |
"initial" |
"unset";

export interface PointerEventsProps {
    pointerEvents?: PointerEvents;
}
export const pointerEvents: InterpolationFunction<PointerEventsProps> =
    (props: PointerEventsProps) => (props.pointerEvents ? `pointer-events: ${props.pointerEvents};` : "");
