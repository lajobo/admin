// Several components seem to have type conflicts with the color props, so this is an easy fix for it
export interface StringColorProps {
    color?: string;
}
