import { InterpolationFunction } from "styled-components";
import { resolveObjectKey } from "../object";
import { ThemeProps } from "./theme";

export interface StrokeProps extends ThemeProps {
    stroke?: string;
}
export const stroke: InterpolationFunction<StrokeProps> =
    (props: StrokeProps) => (props.stroke
        ? `* { stroke: ${resolveObjectKey(props.theme.colors, props.stroke) || props.stroke} }`
        : undefined);
