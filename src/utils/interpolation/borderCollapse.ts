import { InterpolationFunction } from "styled-components";

type BorderCollapses = "separate" | "collapse" | "initial" | "inherit";

export interface BorderCollapseProps {
    borderCollapse?: BorderCollapses;
}
export const borderCollapse: InterpolationFunction<BorderCollapseProps> =
    (props: BorderCollapseProps) => (props.borderCollapse ? `border-collapse: ${props.borderCollapse};` : "");
