import { css, InterpolationFunction, Keyframes } from "styled-components";

type AnimationDirections = "normal" | "reverse" | "alternate" | "alternate-reverse";
type AnimationTimingFunctions = "ease" | "linear" | "ease-in" | "ease-out" | "ease-in-out";
type AnimationFillModes = "none" | "forwards" | "backwards" | "both";
type AnimationIterationCount = number | "infinite" | "initial" | "inherit";

export interface AnimationProps {
    animationName?: string | Keyframes;
    animationDuration?: string;
    animationDelay?: string;
    animationIterationCount?: AnimationIterationCount;
    animationDirection?: AnimationDirections;
    animationTimingFunction?: AnimationTimingFunctions;
    animationFillMode?: AnimationFillModes;
    animation?: string | Keyframes;
}

export const animation: InterpolationFunction<AnimationProps> =
    (props: AnimationProps) => css`
       ${props.animationName ? css`animation-name: ${props.animationName};` : ""}
       ${props.animationDuration ? `animation-duration: ${props.animationDuration};` : ""}
       ${props.animationDelay ? `animation-delay: ${props.animationDelay};` : ""}
       ${props.animationIterationCount ? `animation-iteration-count: ${props.animationIterationCount};` : ""}
       ${props.animationDirection ? `animation-direction: ${props.animationDirection};` : ""}
       ${props.animationTimingFunction ? `animation-timing-function: ${props.animationTimingFunction};` : ""}
       ${props.animationFillMode ? `animation-fill-mode: ${props.animationFillMode};` : ""}
       ${props.animation ? css`animation: ${props.animation};` : ""}
    `;
