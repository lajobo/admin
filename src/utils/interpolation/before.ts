import { InterpolationFunction } from "styled-components";
import { ThemeProps } from "./theme";

export interface BeforeProps extends ThemeProps {
    before?: string;
}
export const before: InterpolationFunction<BeforeProps> = (
    props: BeforeProps
) => (props.before ? `&::before { ${props.before} }` : "");
