import { InterpolationFunction } from "styled-components";

export interface TextDecorationProps {
    textDecoration?: string;
}
export const textDecoration: InterpolationFunction<TextDecorationProps> =
    (props: TextDecorationProps) => (props.textDecoration ? `text-decoration: ${props.textDecoration};` : "");
