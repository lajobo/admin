import { InterpolationFunction } from "styled-components";

export interface TransitionProps {
    transition?: string;
}
export const transition: InterpolationFunction<TransitionProps> =
    (props: TransitionProps) => (props.transition ? `transition: ${props.transition};` : "");
