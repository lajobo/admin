import { InterpolationFunction } from "styled-components";

type BoxSizes = "content-box" | "border-box" | "initial" | "inherit";

export interface BoxSizingProps {
    boxSizing?: BoxSizes;
}
export const boxSizing: InterpolationFunction<BoxSizingProps> =
    (props: BoxSizingProps) => (props.boxSizing ? `box-sizing: ${props.boxSizing};` : "");
