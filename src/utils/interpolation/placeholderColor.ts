import { InterpolationFunction } from "styled-components";
import { resolveColor } from "../component";
import { ThemeProps } from "./theme";

export interface PlaceholderColorProps extends ThemeProps {
    placeholderColor?: string;
}
export const placeholderColor: InterpolationFunction<PlaceholderColorProps> = (
    props: PlaceholderColorProps
) => (props.placeholderColor
    ? `::placeholder { color: ${resolveColor(props.theme.colors, props.placeholderColor)}; }`
    : ""
);
