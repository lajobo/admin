import { InterpolationFunction } from "styled-components";

export interface GapProps {
    gap?: string;
}
export const gap: InterpolationFunction<GapProps> =
    (props: GapProps) => (props.gap ? `gap: ${props.gap};` : "");
