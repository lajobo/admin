import { InterpolationFunction } from "styled-components";

export interface BackdropFilterProps {
    backdropFilter?: string;
}
export const backdropFilter: InterpolationFunction<BackdropFilterProps> =
    (props: BackdropFilterProps) => (props.backdropFilter ? `backdrop-filter: ${props.backdropFilter};` : "");
