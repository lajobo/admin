import React from "react";

export interface RefProps {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ref?: React.RefObject<any>;
}
