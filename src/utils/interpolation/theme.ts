import { Theme } from "../../types/ui";

export interface ThemeProps {
    theme?: Theme;
}
