import { InterpolationFunction } from "styled-components";

export interface IconTransformProps {
    // has to be written lowercase to avoid native react prop errors
    icontransform?: string;
}
export const iconTransform: InterpolationFunction<IconTransformProps> =
    (props: IconTransformProps) => (props.icontransform ? `transform: ${props.icontransform};` : "");
