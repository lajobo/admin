import { InterpolationFunction } from "styled-components";

export interface FilterProps {
    filter?: string;
}
export const filter: InterpolationFunction<FilterProps> =
    (props: FilterProps) => (props.filter ? `filter: ${props.filter};` : "");
