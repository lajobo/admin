import UAParser from "ua-parser-js";
import { DeviceSize } from "../types/device";

export const getDeviceSizeFromUserAgent: (userAgent: string) => DeviceSize = (userAgent: string) => {
    const parser: UAParser = new UAParser();

    switch (parser.setUA(userAgent).getDevice().type) {
        case "mobile":
            return DeviceSize.S;
        case "tablet":
            return DeviceSize.M;
    }

    return DeviceSize.L;
};
