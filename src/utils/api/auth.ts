import { ApiRequestMethods, ApiResponse } from "../../types/api";
import { AuthLoginResponse, AuthLoginStates, AuthRefreshResponse } from "../../types/auth";
import { encodeFormBody, sendRequest } from "../api";
import { runtimeConfig } from "../runtimeConfig";

export const requestLogin: (
    username: string,
    password: string,
    userAgent: string
) => Promise<AuthLoginResponse> = async (
    username: string,
    password: string,
    userAgent: string
) => sendRequest(
    `${runtimeConfig.API_URL}/auth/login`,
    userAgent,
    {
        method: ApiRequestMethods.POST,
        body: encodeFormBody({
            username,
            password
        }),
        headers: { "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8" }
    }
)
    .then((response: ApiResponse<string>) => {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        let message: any = response.data;

        try {
            if (typeof response.data === "string") {
                message = JSON.parse(response.data);
            }
        } catch (parsingError) {
            message = response.data;
        }

        return message;
    })
    .catch(() => ({ state: AuthLoginStates.Error }));

export const requestLogout: (
    userAgent: string
) => void = (
    userAgent: string
) => {
    sendRequest(
        `${runtimeConfig.API_URL}/auth/invalidate`,
        userAgent,
        {
            method: ApiRequestMethods.POST,
            headers: { "Content-Type": "application/json" }
        }
    )
        // ignore the response since the client can't fix it in any way if something fails and the user should not
        // be confused
        .then(() => {})
        .catch(() => {});
};

export const requestLoginRefresh: (
    refreshToken: string,
    userAgent: string
) => Promise<AuthRefreshResponse> = (
    refreshToken: string,
    userAgent: string
) => sendRequest(
    `${runtimeConfig.API_URL}/auth/refresh`,
    userAgent,
    {
        method: ApiRequestMethods.POST,
        body: JSON.stringify({ refreshToken }),
        headers: { "Content-Type": "application/json" }
    }
)
    .then((response: ApiResponse<string>) => {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        let message: any = response.data;

        try {
            if (typeof response.data === "string") {
                message = JSON.parse(response.data);
            }
        } catch (parsingError) {
            message = response.data;
        }

        return message;
    })
    .catch(() => ({ state: AuthLoginStates.Error }));
