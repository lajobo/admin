import { Interpolation } from "styled-components";
import { AnyObject } from "../types/generic";
import { resolveObjectKey } from "./object";
import { DeviceSize } from "../types/device";

export const resolveBreakpoint: <T>(
    values: T[] | T,
    deviceSize: DeviceSize
) => T = <T>(
    values: T[] | T,
    deviceSize: DeviceSize
) => {
    const breakpointIndex: number = deviceSize === DeviceSize.S ? 0 : (deviceSize === DeviceSize.M ? 1 : 2);
    return (Array.isArray(values)
        ? (values.length < (breakpointIndex + 1) ? values[values.length - 1] : values[breakpointIndex])
        : values);
};

export const resolveColor: (
    colors: AnyObject,
    key: string
) => string = (
    colors: AnyObject,
    key: string
) => resolveObjectKey(colors, key, key);

export const mapResolveColor: (
    colors: AnyObject,
    obj: AnyObject
) => Interpolation<unknown> = (
    colors: AnyObject,
    obj: AnyObject
) => Object.keys(obj).reduce<AnyObject>((result: AnyObject, key: string) => ({
    ...result,
    [key]: resolveColor(colors, obj[key])
}), {});
