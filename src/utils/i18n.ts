export const replaceVariables: (
    string: string,
    data: { [key: string]: string | number | boolean }
) => string = (
    string: string,
    data: { [key: string]: string | number | boolean }
) => Object
    .keys(data)
    .reduce((result: string, dataKey: string) => result.replace(`%${dataKey}%`, String(data[dataKey])), string);
