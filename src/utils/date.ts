export const getDateDayDiff: (date1: Date, date2: Date) => number = (date1: Date, date2: Date) => {
    const date1Utc: number = Date.UTC(
        date1.getFullYear(),
        date1.getMonth(),
        date1.getDate(),
        date1.getHours(),
        date1.getMinutes(),
        date1.getSeconds(),
        date1.getMilliseconds()
    );
    const date2Utc: number = Date.UTC(
        date2.getFullYear(),
        date2.getMonth(),
        date2.getDate(),
        date2.getHours(),
        date2.getMinutes(),
        date2.getSeconds(),
        date2.getMilliseconds()
    );

    return (date1Utc - date2Utc) / 86400000;
};
