import http from "http";
import fetch, { Response as NodeResponse } from "node-fetch";
import { ApiRequestConfig, ApiRequestMethods, FormBodyData } from "../../types/api";
import { RequestWithCookies } from "../../types/routes";
import { TokenRequestWhitelistEntry } from "../../types/server/api";
import { encodeFormBody } from "../api";

const tokenRequestWhitelist: TokenRequestWhitelistEntry[] = [
    { pattern: /^\/api\/users$/, method: ApiRequestMethods.GET },
    { pattern: /^\/api\/users\/[^/]*$/, method: ApiRequestMethods.GET },
    { pattern: /^\/api\/users\/[^/]*$/, method: ApiRequestMethods.DELETE },
    { pattern: /^\/api\/users$/, method: ApiRequestMethods.POST },
    { pattern: /^\/api\/users$/, method: ApiRequestMethods.PUT }
];

export const isRequestWhitelisted: (
    req: http.IncomingMessage & RequestWithCookies
) => boolean = (
    req: http.IncomingMessage & RequestWithCookies
) => tokenRequestWhitelist.reduce<boolean>((result: boolean, whitelistEntry: TokenRequestWhitelistEntry) => (
    result ||
    (
        Boolean(req.url.match(whitelistEntry.pattern)) &&
        whitelistEntry.method === req.method
    )
), false);

export const prepareForwardApiRequestConfig: (
    req: RequestWithCookies
) => ApiRequestConfig = (
    req: RequestWithCookies
) => {
    // eslint-disable-next-line prefer-destructuring
    const body: object | string = req.body;
    const contentType: string = req.headers["content-type"];
    const isJson: boolean = contentType && contentType.includes("application/json");
    const isForm: boolean = contentType && contentType.includes("application/x-www-form-urlencoded");

    return {
        method: req.method as ApiRequestMethods,
        headers: req.headers,
        body: [ApiRequestMethods.POST, ApiRequestMethods.PUT].includes(req.method as ApiRequestMethods)
            ? (isJson ? JSON.stringify(body) : (isForm ? encodeFormBody(body as FormBodyData) : body)) as never
            : undefined
    };
};

export const forwardApiRequest: (
    req: RequestWithCookies,
    requestConfig: ApiRequestConfig
) => Promise<NodeResponse> = async (
    req: RequestWithCookies,
    requestConfig: ApiRequestConfig
) => {
    const url: string = process.env.DATA_URL + req.url.substr(4);
    const response: NodeResponse = await fetch(url, requestConfig);
    return response;
};
