import { isClient, isProduction } from "./env";
import { runtimeConfig } from "./runtimeConfig";
import { LogLevels } from "../types/logger";

const i18n: string[] = [
    "OFF",
    "FATAL",
    "ERROR",
    "WARN",
    "INFO",
    "ACCESS",
    "DEBUG",
    "TRACE"
];

export const log: (level: LogLevels, ...messages: unknown[]) => void = (level: LogLevels, ...messages: unknown[]) => {
    if (level <= (Number(runtimeConfig.LOG_LEVEL) || 0)) {
        // TODO log to logging api

        if (!isProduction || !isClient) {
            if (level <= LogLevels.Error) {
                // eslint-disable-next-line no-console
                console.error(`[${i18n[level]}]`, ...messages);
            } else {
                // eslint-disable-next-line no-console
                console.log(`[${i18n[level]}]`, ...messages);
            }
        }
    }
};
