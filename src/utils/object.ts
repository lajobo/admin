import { AnyObject } from "../types/generic";

export const deepClone: <T = Object>(obj: T) => T = <T>(obj: T) => JSON.parse(JSON.stringify(obj));

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const resolveObjectKey: <T = any>(
    object: AnyObject,
    keys: string,
    fallback?: any // eslint-disable-line @typescript-eslint/no-explicit-any
) => T | typeof fallback = <T = any>( // eslint-disable-line @typescript-eslint/no-explicit-any
    object: AnyObject,
    keys: string,
    fallback?: any // eslint-disable-line @typescript-eslint/no-explicit-any
) => keys.split(".").reduce((subObject: AnyObject, key: string) => subObject[key], object) as T || fallback;
