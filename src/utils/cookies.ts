import { CookieSetOptions } from "universal-cookie";
import { isProduction } from "./env";
import { getDateDayDiff } from "./date";

export const supportsSameSiteCookies: (userAgent: string) => boolean = (userAgent: string) => {
    let supports: boolean = true;

    if (userAgent.includes("Safari")) {
        const match: RegExpMatchArray = userAgent.match(/Version\/(\d*.\d*)/);

        if (match) {
            supports = match[1] > `${12}`;
        }
    }

    if (userAgent.includes("Chrome") || userAgent.includes("Chromium")) {
        // eslint-disable-next-line no-useless-escape
        const pattern: RegExp = /Chrom[^ \/]+\/(\d+)[\.\d]*/;
        const matches: RegExpExecArray = pattern.exec(userAgent);

        if (matches.length > 1) {
            const majorVersion: number = parseInt(matches[1], 10) || 80;

            if (majorVersion >= 51 && majorVersion <= 66) {
                supports = false;
            }
        }
    }

    return supports;
};

export interface CookieSetOptionsProps {
    days?: number;
    userAgent?: string;
    options?: CookieSetOptions;
}

export const getCookieSetOptions: (props: CookieSetOptionsProps) => CookieSetOptions = (
    props: CookieSetOptionsProps
) => {
    const currentDate: Date = new Date();
    const endDate: Date = new Date();

    endDate.setDate(endDate.getDate() + (props.days || 7));

    return {
        maxAge: getDateDayDiff(endDate, currentDate) * 24 * 60 * 60,
        path: "/",
        sameSite: props.userAgent && supportsSameSiteCookies(props.userAgent) ? "lax" : "none",
        secure: isProduction,
        ...(props.options || {})
    };
};
