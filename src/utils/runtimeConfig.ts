/* eslint-disable object-curly-newline,no-underscore-dangle */
import { RuntimeConfig } from "../types/runtimeConfig";
import { Envs } from "../types/env";
import { LogLevels } from "../types/logger";

// Decided to use __ in the property name just as Redux __PRELOADED_STATE__ is named.
// These are states/configs that are loaded on page load for syncing server and client.
declare let window: { __RUNTIME_CONFIG__: RuntimeConfig };

const getClientRuntimeConfig: () => RuntimeConfig = () => Object
    .keys(window.__RUNTIME_CONFIG__)
    .reduce<Partial<RuntimeConfig>>((config: Partial<RuntimeConfig>, key: string) => ({
    ...config,
    [key]: window.__RUNTIME_CONFIG__[key]
}), {}) as RuntimeConfig;

const getServerRuntimeConfig: () => RuntimeConfig = () => ({
    NODE_ENV: process.env.NODE_ENV as Envs,
    API_URL: process.env.API_URL,
    AUTH_PUBLIC_KEY: process.env.AUTH_PUBLIC_KEY,
    LOG_LEVEL: process.env.LOG_LEVEL as unknown as LogLevels
});

export const runtimeConfig: RuntimeConfig = typeof window !== "undefined" ? getClientRuntimeConfig() : getServerRuntimeConfig();
