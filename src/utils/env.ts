import { runtimeConfig } from "./runtimeConfig";
import { Envs } from "../types/env";

export const isProduction: boolean = runtimeConfig.NODE_ENV === Envs.Production;

export const isClient: boolean = typeof window !== "undefined";
