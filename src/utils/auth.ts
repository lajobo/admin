import { verify } from "jsonwebtoken";
import { runtimeConfig } from "./runtimeConfig";

export const verifyToken: (token: string) => boolean = (token: string) => {
    let isValid: boolean = false;

    if (token) {
        try {
            verify(token, runtimeConfig.AUTH_PUBLIC_KEY);
            isValid = true;
        } catch (error: unknown) { /* dont do anything */ }
    }

    return isValid;
};

export const verifyTokens: (accessToken: string, refreshToken: string) => boolean = (
    accessToken: string,
    refreshToken: string
) => accessToken &&
    refreshToken &&
    verifyToken(accessToken) &&
    verifyToken(refreshToken);

export const isTokenExpired: (token: string) => boolean | undefined = (token: string) => {
    if (!token) {
        return undefined;
    }

    let expired: boolean;

    try {
        verify(token, runtimeConfig.AUTH_PUBLIC_KEY);
        expired = false;
    } catch (error: any) { // eslint-disable-line @typescript-eslint/no-explicit-any
        expired = error?.name === "TokenExpiredError";
    }

    return expired;
};
