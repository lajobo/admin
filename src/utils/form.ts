import { ValidationResult } from "../types/contexts/validation";

export const getFormElementStates: (
    disabled: boolean,
    loading: boolean,
    validationResult: ValidationResult
) => [boolean, boolean, boolean, boolean] = (
    disabled: boolean,
    loading: boolean,
    validationResult: ValidationResult
) => {
    const isDisabled: boolean = disabled;
    const isLoading: boolean = !isDisabled && loading;
    const hasErrors: boolean =
        !isDisabled &&
        !isLoading &&
        validationResult.valid === false &&
        validationResult?.errors?.length > 0;
    const hasWarnings: boolean =
        !isDisabled &&
        !isLoading &&
        !hasErrors &&
        validationResult?.warnings?.length > 0;

    return [isDisabled, isLoading, hasErrors, hasWarnings];
};

export const getFormId: (pathname: string) => string | undefined = (pathname: string) => {
    const directoryParams: string[] = pathname.split("/").filter(Boolean);
    return directoryParams.at(-1) === "create" ? undefined : directoryParams.at(-1);
};
