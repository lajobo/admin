/* eslint-disable @typescript-eslint/no-explicit-any */
export const cartesianProduct: (...args: any[][]) => any[][] = (...args: any[][]) => args
    .reduce((product: any[], options: any[]) => product
        .flatMap((productSet: any) => options
            .map((option: any) => [productSet, option].flat())));

export const enrichCartesianProductWithNames: (
    cartesianProduct: any[][],
    names: string[]
) => { [key: string]: any }[] = (
    product: any[][],
    names: string[]
) => product.map((set: any[]) => (
    names.reduce((result: { [key: string]: any }, name: string, index: number) => ({
        ...result,
        [name]: set[index]
    }), {})
));
/* eslint-enable @typescript-eslint/no-explicit-any */
