import { Context, createContext } from "react";
import { FieldContextStructure } from "../types/contexts/field";

export const FieldContext: Context<FieldContextStructure> = createContext<FieldContextStructure>({
    active: false,
    fieldId: undefined,
    setFieldId: () => {},
    setFieldRequired: () => {}
});
