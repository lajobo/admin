import { Context, createContext } from "react";
import { ListRowContextStructure, ListRowTypes } from "../types/contexts/list";

export const ListRowContext: Context<ListRowContextStructure> = createContext<ListRowContextStructure>({
    type: ListRowTypes.Data,
    data: {}
});
