import { Context , createContext } from "react";

export const UserAgentContext: Context<string> = createContext<string>(undefined);
