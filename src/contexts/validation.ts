import { Context, createContext } from "react";
import { ValidationContextStructure } from "../types/contexts/validation";

export const ValidationContext: Context<ValidationContextStructure> = createContext<ValidationContextStructure>({
    active: false,
    addValidation: () => () => {}
});
