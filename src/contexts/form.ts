import { Context, createContext } from "react";
import { FormContextStructure } from "../types/contexts/form";

export const FormContext: Context<FormContextStructure> = createContext<FormContextStructure>({
    active: false,
    formData: {},
    setValue: () => { },
    loading: false,
    addValidation: () => () => { },
    i18n: {},
    errors: []
});
