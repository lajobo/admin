import { ReducersMapObject } from "redux";
import { i18nReducer as i18n } from "./slices/i18n";
import { deviceReducer as device } from "./slices/device";
import { authReducer as auth } from "./slices/auth";
import { userReducer as user } from "./slices/user";
import { State } from "../types/store";
import { notificationReducer as notification } from "./slices/notification";

export const reducer: ReducersMapObject<State> = {
    auth,
    device,
    i18n,
    notification,
    user
};
