/* eslint-disable no-param-reassign */
import { CaseReducer, createSlice, Draft, PayloadAction, Reducer, Slice } from "@reduxjs/toolkit";
import { I18nLanguages, I18nStoreState } from "../../types/i18n";
// @ts-ignore
import I18nEn from "../../i18n/en.json";
// @ts-ignore
import I18nDe from "../../i18n/de.json";
import { Cookies } from "../../types/cookies";
import { isClient } from "../../utils/env";

type I18nSlice = Slice<I18nStoreState, {
    setLanguage: CaseReducer<I18nStoreState, PayloadAction<I18nLanguages>>
}, "i18n">;

const i18nSlice: I18nSlice = createSlice({
    name: "i18n",
    initialState: {
        language: I18nLanguages.EN,
        data: I18nEn
    },
    reducers: {
        setLanguage(state: Draft<I18nStoreState>, action: PayloadAction<I18nLanguages>) {
            state.language = action.payload;
            state.data = action.payload === I18nLanguages.DE ? I18nDe : I18nEn;

            if (isClient) {
                document.cookie = `${Cookies.Language}=${action.payload}; path=/; max-age=${365 * 24 * 60 * 60}`;
            }
        }
    }
});

export const { setLanguage } = i18nSlice.actions;
export const i18nReducer: Reducer = i18nSlice.reducer;
