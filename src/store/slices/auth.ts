/* eslint-disable no-param-reassign */
import {
    ActionReducerMapBuilder,
    CaseReducer,
    createSlice,
    Draft,
    PayloadAction,
    Reducer,
    Slice
} from "@reduxjs/toolkit";
import { NoInfer } from "@reduxjs/toolkit/dist/tsHelpers";
import { AuthLoginResponse, AuthLoginStates, AuthStateDetails, AuthStates, AuthStoreState } from "../../types/auth";
import { AnyAction } from "../../types/state";
import { login } from "../api/auth/login";

type AuthSlice = Slice<AuthStoreState, {
    setAuthState: CaseReducer<AuthStoreState, PayloadAction<AuthStates>>
}, "auth">;

const authSlice: AuthSlice = createSlice({
    name: "auth",
    initialState: { state: AuthStates.Pending },
    reducers: {
        setAuthState(state: Draft<AuthStoreState>, action: PayloadAction<AuthStates>) {
            state.state = action.payload;
        }
    },
    extraReducers: (builder: ActionReducerMapBuilder<NoInfer<AuthStoreState>>) => {
        builder.addCase(login.pending, (state: Draft<AuthStoreState>) => {
            if (state.state === AuthStates.LoggedOut) {
                state.state = AuthStates.Pending;
            }
        });
        builder.addCase(login.fulfilled, (state: Draft<AuthStoreState>, action: AnyAction<AuthLoginResponse>) => {
            if (state.state === AuthStates.Pending) {
                state.state = action.payload?.state === AuthLoginStates.Success
                    ? AuthStates.LoggedIn
                    : AuthStates.LoggedOut;
                state.details = action.payload?.state === AuthLoginStates.Failure
                    ? AuthStateDetails.Credentials
                    : (
                        action.payload?.state === AuthLoginStates.Error
                            ? AuthStateDetails.ServerError
                            : undefined
                    );
            }
        });
        builder.addCase(login.rejected, (state: Draft<AuthStoreState>) => {
            if (state.state === AuthStates.Pending) {
                state.state = AuthStates.LoggedOut;
                state.details = AuthStateDetails.ServerError;
            }
        });
    }
});

export const { setAuthState } = authSlice.actions;
export const authReducer: Reducer = authSlice.reducer;
