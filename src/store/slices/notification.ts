/* eslint-disable no-param-reassign */
import { CaseReducer, createSlice, Draft, PayloadAction, Reducer, Slice } from "@reduxjs/toolkit";
import { v4 as uuidv4 } from "uuid";
import { Notification, NotificationStoreState, StoreNotification } from "../../types/notification";

type NotificationSlice = Slice<NotificationStoreState, {
    addNotification: CaseReducer<NotificationStoreState, PayloadAction<Notification>>,
    deleteNotification: CaseReducer<NotificationStoreState, PayloadAction<string>>,
}, "notification">;

const notificationSlice: NotificationSlice = createSlice({
    name: "notification",
    initialState: { notifications: [] },
    reducers: {
        addNotification(state: Draft<NotificationStoreState>, action: PayloadAction<Notification>) {
            state.notifications = [
                ...state.notifications,
                {
                    ...action.payload,
                    id: uuidv4(),
                    duration: action.payload.duration ||
                        Math.max(
                            2,
                            Math.min(
                                10,
                                ((action.payload.heading.length + (action.payload.content?.length || 0)) * 0.05)
                            )
                        ),
                    type: action.payload.type || "info"
                }
            ];
        },
        deleteNotification(state: Draft<NotificationStoreState>, action: PayloadAction<string>) {
            state.notifications =
                state.notifications.filter((notification: StoreNotification) => notification.id !== action.payload);
        }
    }
});

export const { addNotification, deleteNotification } = notificationSlice.actions;
export const notificationReducer: Reducer = notificationSlice.reducer;
