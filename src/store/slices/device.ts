/* eslint-disable no-param-reassign */
import { CaseReducer, createSlice, Draft, PayloadAction, Reducer, Slice } from "@reduxjs/toolkit";
import { DeviceSize, DeviceStoreState } from "../../types/device";

type DeviceSlice = Slice<DeviceStoreState, {
    setUserAgent: CaseReducer<DeviceStoreState, PayloadAction<string>>,
    setDeviceSize: CaseReducer<DeviceStoreState, PayloadAction<DeviceSize>>
}, "device">;

const deviceSlice: DeviceSlice = createSlice({
    name: "device",
    initialState: {
        userAgent: "",
        size: DeviceSize.L
    },
    reducers: {
        setUserAgent(state: Draft<DeviceStoreState>, action: PayloadAction<string>) {
            state.userAgent = action.payload;
        },
        setDeviceSize(state: Draft<DeviceStoreState>, action: PayloadAction<DeviceSize>) {
            state.size = action.payload;
        }
    }
});

export const { setUserAgent, setDeviceSize } = deviceSlice.actions;
export const deviceReducer: Reducer = deviceSlice.reducer;
