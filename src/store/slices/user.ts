/* eslint-disable no-param-reassign,object-curly-newline */
import { CaseReducer, createSlice, Draft, PayloadAction, Reducer, Slice } from "@reduxjs/toolkit";
import { User, Users, UserStoreState } from "../../types/user";
import { fetchUser, FetchUserProps, fetchUsers } from "../api/user";
import { AnyAction, DataFetchingState, RejectedAction } from "../../types/state";

type UserSlice = Slice<UserStoreState, {
    setUserDataFetchingState: CaseReducer<UserStoreState, PayloadAction<DataFetchingState>>
    setUserDataFetchingStateForUser: CaseReducer<UserStoreState, PayloadAction<{ state: DataFetchingState, userId: string }>>
}, "user">;

const userSlice: UserSlice = createSlice({
    name: "user",
    initialState: {
        state: DataFetchingState.Idle,
        users: []
    },
    reducers: {
        setUserDataFetchingState(state: Draft<UserStoreState>, action: PayloadAction<DataFetchingState>) {
            state.state = action.payload;
        },
        setUserDataFetchingStateForUser(state: Draft<UserStoreState>, action: PayloadAction<{ state: DataFetchingState, userId: string }>) {
            state.users = [
                ...state.users.filter((user: User) => user.id !== action.payload.userId),
                {
                    ...state.users.find((user: User) => user.id === action.payload.userId),
                    state: action.payload.state
                }
            ];
        }
    },
    extraReducers: (builder) => {
        builder.addCase(fetchUsers.pending, (state: Draft<UserStoreState>) => {
            state.state = DataFetchingState.Pending;
        });
        builder.addCase(fetchUsers.fulfilled, (state: Draft<UserStoreState>, action: AnyAction<Users>) => {
            state.state = DataFetchingState.Fulfilled;
            state.users = action.payload;
        });
        builder.addCase(fetchUsers.rejected, (state: Draft<UserStoreState>) => {
            state.state = DataFetchingState.Rejected;
        });
        builder.addCase(fetchUser.pending, (state: Draft<UserStoreState>, action: AnyAction<Users, FetchUserProps>) => {
            state.users = [
                ...state.users.filter((user: User) => user.id !== action.meta.arg.userId),
                {
                    id: action.meta.arg.userId,
                    ...state.users.find((user: User) => user.id === action.meta.arg.userId),
                    state: DataFetchingState.Pending
                }
            ];
        });
        builder.addCase(fetchUser.fulfilled, (state: Draft<UserStoreState>, action: AnyAction<Users, FetchUserProps>) => {
            state.users = [
                ...state.users.filter((user: User) => user.id !== action.meta.arg.userId),
                {
                    id: action.meta.arg.userId,
                    ...state.users.find((user: User) => user.id === action.meta.arg.userId),
                    ...action.payload,
                    state: DataFetchingState.Fulfilled
                }
            ];
        });
        builder.addCase(fetchUser.rejected, (state: Draft<UserStoreState>, action: RejectedAction<FetchUserProps>) => {
            state.users = [
                ...state.users.filter((user: User) => user.id !== action.meta.arg.userId),
                {
                    id: action.meta.arg.userId,
                    ...state.users.find((user: User) => user.id === action.meta.arg.userId),
                    state: DataFetchingState.Rejected
                }
            ];
        });
    }
});

export const { setUserDataFetchingState, setUserDataFetchingStateForUser } = userSlice.actions;
export const userReducer: Reducer = userSlice.reducer;
