import { combineReducers, Store } from "redux";
import { configureStore as configureStoreRTK } from "@reduxjs/toolkit";
import { EnhancedStore } from "@reduxjs/toolkit/src/configureStore";
import { setupListeners } from "@reduxjs/toolkit/query";
import { reducer } from "./reducer";
import { State } from "../types/store";

declare const module: {
    hot: {
        accept: (path?: string, callback?: () => void) => void
    }
};

export const configureStore: (preloadedState?: State) => Store<State> = (
    preloadedState: State
) => {
    const store: EnhancedStore<State> = configureStoreRTK({
        reducer,
        preloadedState
    });

    if (module.hot) {
        module.hot.accept("./reducer", () => {
            store.replaceReducer(combineReducers(reducer));
        });
    }

    setupListeners(store.dispatch);

    return store;
};
