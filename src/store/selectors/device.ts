import { DeviceSize } from "../../types/device";
import { State } from "../../types/store";

export const selectUserAgent: (state: State) => string = (state: State) => state.device.userAgent;

export const selectDeviceSize: (state: State) => DeviceSize = (state: State) => state.device.size;
