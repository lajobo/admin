import { AuthStateDetails, AuthStates } from "../../types/auth";
import { State } from "../../types/store";

export const selectAuthState: (state: State) => AuthStates = (state: State) => state.auth.state;

export const selectAuthStateDetails: (state: State) => AuthStateDetails = (state: State) => state.auth.details;
