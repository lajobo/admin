import { I18n, I18nLanguages } from "../../types/i18n";
import { State } from "../../types/store";

export const selectLanguage: (state: State) => I18nLanguages = (state: State) => state.i18n.language;

export const selectI18n: (state: State) => I18n = (state: State) => state.i18n.data;
