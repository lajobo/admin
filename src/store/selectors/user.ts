import { State } from "../../types/store";
import { User, Users } from "../../types/user";
import { DataFetchingState } from "../../types/state";

export const selectUsers: (state: State) => Users = (state: State) => state.user.users;

export const selectUser: (userId: string) => (state: State) => User =
    (userId: string) => (state: State) => state.user.users.find((user: User) => user.id === userId);

export const selectUserDataFetchingState: (state: State) => DataFetchingState = (state: State) => state.user.state;

export const selectUserDataFetchingStateForUser: (userId: string) => (state: State) => DataFetchingState =
    (userId: string) => (state: State) => state.user.users.find((user: User) => user.id === userId)?.state;
