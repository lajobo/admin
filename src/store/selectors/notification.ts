import { State } from "../../types/store";
import { StoreNotification } from "../../types/notification";

export const selectNotifications: (state: State) => StoreNotification[] = (state: State) => state.notification.notifications;
