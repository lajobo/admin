import { AsyncThunk, createAsyncThunk } from "@reduxjs/toolkit";
import { AuthLoginResponse } from "../../../types/auth";
import { requestLogin } from "../../../utils/api/auth";
import { selectUserAgent } from "../../selectors/device";
import { GetThunkAPI } from "../../../types/store";

interface LoginProps {
    username: string;
    password: string;
}

export const login: AsyncThunk<AuthLoginResponse, LoginProps, {}> = createAsyncThunk(
    "auth/login",
    // eslint-disable-next-line @typescript-eslint/return-await
    async (props: LoginProps, thunkAPI: GetThunkAPI) => await requestLogin(
        props.username,
        props.password,
        selectUserAgent(thunkAPI.getState())
    )
);
