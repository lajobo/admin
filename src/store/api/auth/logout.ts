import { AsyncThunk, createAsyncThunk } from "@reduxjs/toolkit";
import { requestLogout } from "../../../utils/api/auth";
import { setAuthState } from "../../slices/auth";
import { AuthStates } from "../../../types/auth";
import { GetThunkAPI } from "../../../types/store";
import { addNotification } from "../../slices/notification";
import { selectI18n } from "../../selectors/i18n";

export const logout: AsyncThunk<void, void, {}> = createAsyncThunk(
    "auth/login",
    async (props: void, thunkAPI: GetThunkAPI) => {
        await requestLogout(thunkAPI.getState().device.userAgent);
        thunkAPI.dispatch(addNotification({
            type: "success",
            ...selectI18n(thunkAPI.getState()).store.auth.logout
        }));
        thunkAPI.dispatch(setAuthState(AuthStates.LoggedOut));
    }
);
