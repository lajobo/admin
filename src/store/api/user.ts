import { AsyncThunk, createAsyncThunk } from "@reduxjs/toolkit";
import { encodeFormBody, sendRequest } from "../../utils/api";
import { runtimeConfig } from "../../utils/runtimeConfig";
import { ApiRequestMethods, ApiResponse } from "../../types/api";
import { selectUserAgent } from "../selectors/device";
import { Users } from "../../types/user";
import { GetThunkAPI } from "../../types/store";
import { AnyObject } from "../../types/generic";
import { logout } from "./auth/logout";
import { addNotification } from "../slices/notification";
import { selectI18n } from "../selectors/i18n";
import { StoreUserDeleteI18n } from "../../types/i18n";
import { FormDataState, FormError } from "../../types/reducers/formData";

export interface FetchUserProps {
    userId: string;
    serverHeaders?: AnyObject;
}

export const fetchUser: AsyncThunk<Users, FetchUserProps, {}> = createAsyncThunk(
    "authData/users/fetchUser",
    async (props: FetchUserProps, thunkAPI: GetThunkAPI) => {
        const userResponse: ApiResponse<Users> = await sendRequest<Users>(
            `${runtimeConfig.API_URL}/users/${props.userId}`,
            selectUserAgent(thunkAPI.getState()),
            { method: ApiRequestMethods.GET, headers: props?.serverHeaders }
        );

        if (userResponse.status === 401) {
            thunkAPI.dispatch(logout());
        }

        return new Promise<Users>((resolve, reject) => {
            if (userResponse.status === 200) {
                resolve(userResponse.data);
            } else {
                reject();
            }
        });
    }
);

export const fetchUsers: AsyncThunk<Users, AnyObject | void, {}> = createAsyncThunk(
    "authData/users/fetchUsers",
    async (serverHeaders: AnyObject, thunkAPI: GetThunkAPI) => {
        const userResponse: ApiResponse<Users> = await sendRequest<Users>(
            `${runtimeConfig.API_URL}/users`,
            selectUserAgent(thunkAPI.getState()),
            { method: ApiRequestMethods.GET, headers: serverHeaders }
        );

        if (userResponse.status === 401) {
            thunkAPI.dispatch(logout());
        }

        return new Promise<Users>((resolve, reject) => {
            if (userResponse.status === 200) {
                resolve(userResponse.data);
            } else {
                reject();
            }
        });
    }
);

export interface DeleteUserProps {
    userId: string;
    serverHeaders?: AnyObject;
}

export const deleteUser: AsyncThunk<boolean, DeleteUserProps, {}> =
    createAsyncThunk(
        "authData/users/deleteUser",
        async (props: DeleteUserProps, thunkAPI: GetThunkAPI) => {
            const userResponse: ApiResponse<Users> = await sendRequest<Users>(
                `${runtimeConfig.API_URL}/users/${props.userId}`,
                selectUserAgent(thunkAPI.getState()),
                { method: ApiRequestMethods.DELETE, headers: props.serverHeaders }
            );

            if (userResponse.status === 401) {
                thunkAPI.dispatch(logout());
            }

            const i18n: StoreUserDeleteI18n = selectI18n(thunkAPI.getState()).store.user.delete;
            thunkAPI.dispatch(addNotification({
                type: userResponse.status === 204 ? "success" : "error",
                ...i18n[userResponse.status === 204 ? "success" : "fail"]
            }));

            // update user list no matter if succeeded or failed to get the correct users
            thunkAPI.dispatch(fetchUsers());

            return new Promise<boolean>((resolve, reject) => {
                if (userResponse.status === 204) {
                    resolve(true);
                } else {
                    reject();
                }
            });
        }
    );

export interface SaveUserProps {
    formData: FormDataState;
    onSuccess?: () => void;
    onFailure?: (errors: FormError[]) => void;
    onError?: () => void;
    serverHeaders?: AnyObject;
}

export const createUser: AsyncThunk<void, SaveUserProps, {}> =
    createAsyncThunk(
        "authData/users/createUser",
        async (props: SaveUserProps, thunkAPI: GetThunkAPI) => {
            const userResponse: ApiResponse<FormError[]> = await sendRequest<FormError[]>(
                `${runtimeConfig.API_URL}/users`,
                selectUserAgent(thunkAPI.getState()),
                {
                    method: ApiRequestMethods.POST,
                    body: encodeFormBody({
                        username: String(props.formData?.username),
                        password: String(props.formData?.password),
                        createdAt: new Date().toISOString(),
                        active: String(props.formData?.active)
                    }),
                    headers: {
                        ...props.serverHeaders,
                        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
                    }
                }
            );

            return new Promise<void>((resolve, reject) => {
                if (userResponse.status === 204 && !userResponse.data) {
                    props.onSuccess();
                    resolve();
                } else if (userResponse.status === 409 && userResponse.data) {
                    props.onFailure(userResponse.data);
                    reject();
                } else {
                    props.onError();
                    reject();
                }
            });
        }
    );

export const updateUser: AsyncThunk<void, SaveUserProps, {}> =
    createAsyncThunk(
        "authData/users/updateUser",
        async (props: SaveUserProps, thunkAPI: GetThunkAPI) => {
            const userResponse: ApiResponse<FormError[]> = await sendRequest<FormError[]>(
                `${runtimeConfig.API_URL}/users`,
                selectUserAgent(thunkAPI.getState()),
                {
                    method: ApiRequestMethods.PUT,
                    body: encodeFormBody({
                        id: String(props.formData?.id),
                        username: String(props.formData?.username),
                        password: String(props.formData?.password),
                        active: String(props.formData?.active)
                    }),
                    headers: {
                        ...props.serverHeaders,
                        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
                    }
                }
            );

            return new Promise<void>((resolve, reject) => {
                if (userResponse.status === 204 && !userResponse.data) {
                    props.onSuccess();
                    resolve();
                } else if (userResponse.status === 409 && userResponse.data) {
                    props.onFailure(userResponse.data);
                    reject();
                } else {
                    props.onError();
                    reject();
                }
            });
        }
    );
