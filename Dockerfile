FROM node:10-alpine
WORKDIR /opt
ENTRYPOINT ["npm", "run", "start"]
