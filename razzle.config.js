module.exports = {
    plugins: ["scss"],
    devtool: "source-map",
    modifyWebpackConfig: (config) => ({
        ...config.webpackConfig,
        resolve: {
            ...config.webpackConfig.resolve,
            // https://webpack.js.org/configuration/resolve/#resolvefallback
            fallback: {
                crypto: require.resolve('crypto-browserify'),
                stream: require.resolve('stream-browserify'),
                util: require.resolve('util')
            },
        }
    })
};
