# OAuth2/Admin

The Admin service is meant as a frontend for the OAuth2 project to access the data provided and maintain users.

## Quick start
Rename `/example.env` to `/.env` and fill in the placeholder values. You will need the OAuth2/auth public key for token encryption.
```bash
npm install
```
followed by
```bash
npm run start
```
or
```bash
docker compose up
```
## Usage
This frontend allows users from the **OAuth2/auth** service to log into it.
Once logged in there are placeholders for a **Dashboard** and a **Client** management.
The **User** management pages are the only pages that are fully implemented.
In there you can see a list of all users where existing datasets can either be edited or deleted or new datasets can be created.
Once a single dataset gets opened, it will show a form with all available fields and actions to update, delete or cancel.

## Extensibility
Since this whole project was mainly based on learning as you can see in the **Oauth2/auth** README.md section **Motivation**,
this project came a bit on hold after the main part, the authorization, was successfully implemented and working.
The project may or may not be extended at some point, for the moment it is on hold and the **User list and form** are the only available pages.
It could be extended to get a complete user and client management running and even implement a whole product around it.

## Highlights
All authentication related cookies are stored as `HttpOnly` so that they can not be read from any external scripts or addons running on the website or client.
Therefore, all requests to the **OAuth2/data** service will be piped through the webserver where a proxy will add the necessary tokens from the cookies to the request and forward it to the **OAuth2/data** service.
This concept allows to keep the cookies as `HttpOnly` while still handling the request logic completely in the same service and the cookies on the same domain.

The List component (`/src/components/ui/list`), that was used for the **User list**, was built for simplicity and reusability.
It should be as simple as possible to just put in data and define the columns that are shown.

Same goes for the Form component (`/src/components/ui/form`) that was used in the **User form**.
It allows creating new forms quickly without having to deal with data handling or validation yourself, everything is handled in a smooth way by the provided components and their contexts.



## Known issues
This service contains mocked data, that was used during the development, and is not really complete in some places.
* The user data in the menu sidebar is hardcoded and doesn't use the real OAuth2/auth userdata
* The **Sign up** link in the login page does not link anywhere
* The Dashboard is completely empty
* There is no permission handling in place

#### Author

* [Lars Bomnüter](mailto:larsbomnueter@web.de)

---

MIT License
